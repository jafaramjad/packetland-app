module.exports.ORIGINS = [
  'https://canvass.pro',
  /\.canvass\.pro$/,
  'https://packet-land.firebaseapp.com',
  /\.packet\.land$/,
  'http://localhost:3000'
]
