const cors = require('cors')
const express = require('express')
const twitterAPI = require('node-twitter-api')

/**
 * API - TWITTER =====================================================================
 *  */
const twitter = new twitterAPI({
  consumerKey: process.env.TwitterConsumerKey,
  consumerSecret: process.env.TwitterConsumerSecret
})

const appTwitter = express()
appTwitter.use(cors({ origin: true }))
appTwitter.get('/timeline', (req, res) => {
  const username = req.query.q

  twitter.getTimeline(
    'user',
    {
      count: 100,
      include_rts: true,
      exclude_replies: false,
      screen_name: username
    },
    process.env.TwitterAccessToken,
    process.env.TwitterAccessTokenSecret,
    function(error, data, response) {
      if (error) {
        console.error(error)
      } else {
        res.send(data)
      }
    }
  )
})

module.exports = appTwitter
