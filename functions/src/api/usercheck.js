// const axios = require('axios')
const cors = require('cors')
const express = require('express')
const validate = require('../utils/validate')
const url = require('../utils/constants')

/**
 * API - USER CHECK =====================================================================
 */
const appUserCheck = express()

appUserCheck.use(cors({ origin: url.ORIGINS }))
appUserCheck.use(validate.FirebaseIdToken)

appUserCheck.get('/userverify', (req, res) => {
  // res.header("Access-Control-Allow-Origin", "*")
  // res.header("Access-Control-Allow-Headers", "X-Requested-With")
  // res.header("Access-Control-Allow-Credentials", "true")

  let checkemail = req.query.q

  if (checkemail === undefined) {
    res.send('')
  } else {
    const DASHBOARD = process.env.DASHBOARD
    const ACCESS = process.env.ACCESS

    const accessDash = DASHBOARD.split('|').includes(checkemail)
    const accessSite = true // ACCESS.split('|').includes(checkemail)

    res.send({
      dashboard: accessDash,
      access: accessSite
    })
  }
})

module.exports = appUserCheck
