const cors = require('cors')
const express = require('express')
const admin = require('firebase-admin')
const validate = require('../utils/validate')
const url = require('../utils/constants')

/**
 * SETUP CORS AND VALIDATE REQUEST
 */
const appUsers = express()
appUsers.use(cors({ origin: url.ORIGINS }))
appUsers.use(validate.FirebaseIdToken)


/**
 * API - USERS LIST BY ORG & CAMP =====================================================================
 */
appUsers.post('/users/people/new', (req, res) => {
  let email = req.query.email
  let role = req.query.role


  admin
    .auth()
    .createUser({
      email: email,
      emailVerified: false,
      disabled: false
    })
    .then(function (userRecord) {
      // See the UserRecord reference doc for the contents of userRecord.
      console.log('Successfully created new user:', userRecord.uid);

      admin.auth().setCustomUserClaims(userRecord.uid, {
        'org': ['NC09'],
        'camp': ['MCCREADY'],
        'role': [role]
      })


      res.send({ userRecord })

    })
    .catch(function (error) {
      console.log('Error creating new user:', error);
    });



})



/**
 * API - USERS LIST BY ORG & CAMP =====================================================================
 */
appUsers.get('/users/listByClaims', (req, res) => {
  // let checkemail = req.query.q
  let org = req.query.org
  let camp = req.query.camp
  let role = req.query.role

  let usersList = []
  // function listAllUsers(nextPageToken) {
  // List batch of users, 1000 at a time.
  // let nextPageToken = 1

  admin
    .auth()
    .listUsers(1000)
    .then(function (listUsersResult) {
      listUsersResult.users.forEach(function (userRecord) {
        console.log(userRecord)
        // console.log(userRecord.customClaims.camp)
        // console.log(userRecord.customClaims.org)


        if (userRecord.customClaims && userRecord.customClaims.org[0] === org && userRecord.customClaims.camp[0] === camp) {

          usersList.push(userRecord.toJSON())
        }


      })

      res.send({
        usersList
      })
      // if (listUsersResult.pageToken) {
      //     // List next batch of users.
      //     listAllUsers(listUsersResult.pageToken)
      // }
    })
    .catch(function (error) {
      console.log('Error listing users:', error)
    })

  // }
  // Start listing users from the beginning, 1000 at a time.
  // listAllUsers();
  // END
})


/**
 * API - USERS LIST =====================================================================
 */
appUsers.get('/users/list', (req, res) => {
  // let checkemail = req.query.q

  let usersList = []
  // function listAllUsers(nextPageToken) {
  // List batch of users, 1000 at a time.
  // let nextPageToken = 1

  admin
    .auth()
    .listUsers(1000)
    .then(function (listUsersResult) {
      listUsersResult.users.forEach(function (userRecord) {
        console.log(userRecord.customClaims)

        usersList.push(userRecord.toJSON())
      })

      res.send({
        usersList
      })
      // if (listUsersResult.pageToken) {
      //     // List next batch of users.
      //     listAllUsers(listUsersResult.pageToken)
      // }
    })
    .catch(function (error) {
      console.log('Error listing users:', error)
    })

  // }
  // Start listing users from the beginning, 1000 at a time.
  // listAllUsers();
  // END
})

/**
 * SET CUSTOM CLAIM - ROLES
 */
appUsers.post('/users/roles', (req, res) => {
  let email = req.query.email
  let role = req.query.role

  admin
    .auth()
    .getUserByEmail(email)
    .then(user => {
      // Confirm user is verified.
      // if (user.emailVerified) {
      // Add custom claims for additional privileges.
      // This will be picked up by the user on token refresh or next sign in on new device.
      if (role !== 'remove') {
        admin.auth().setCustomUserClaims(user.uid, {
          // [role]: true
          role: [role]
        })
      } else {
        admin.auth().setCustomUserClaims(user.uid, {
          role: null
        })
      }

      res.send({ user })

      // } else {
      // 	res.send({ 'verified': false })
      // }
    })
    .catch(error => {
      console.log(error)
    })
})


/**
 * SET CUSTOM CLAIMS
 */
appUsers.post('/users/claims', (req, res) => {
  const email = req.query.email
  const claim = req.query.claim
  const val = req.query.val

  let claimName

  switch (claim) {
    case 'org':
      claimName = 'org'
      break
    case 'camp':
      claimName = 'camp'
      break
    case 'role':
      claimName = 'role'
      break
    default:
      claimName = null
      break
  }

  if (claimName === null) {
    res.send('error with request...')
    return
  }

  admin
    .auth()
    .getUserByEmail(email)
    .then(user => {

      console.log(user.customClaims)

      if (val !== 'remove') {
        admin.auth().setCustomUserClaims(user.uid, {
          ...user.customClaims,
          [claimName]: [val]
        })
      } else {
        let tmpObj = { ...user.customClaims }
        delete tmpObj[claimName]

        admin.auth().setCustomUserClaims(user.uid, {
          ...tmpObj
          // ...user.customClaims,
          // [claimName]: []
        })
      }

      res.send({ user })

    })
    .catch(error => {
      console.log(error)
    })
})

/**
 * SET INITIAL ADMIN
 */
/* appUsers.post('/users/initadmin', (req, res) => {

	// res.send({ msg: 'yo wassup' })

	// let checkemail = req.query.q

	admin.auth().getUserByEmail('jafar104@gmail.com').then((user) => {
		// Confirm user is verified.
		//if (user.emailVerified) {
		// Add custom claims for additional privileges.
		// This will be picked up by the user on token refresh or next sign in on new device.
		admin.auth().setCustomUserClaims(user.uid, {
			admin: true
		})

		res.send(user)

	}).catch((error) => {
		console.log(error)
	})

}) */

module.exports = appUsers
