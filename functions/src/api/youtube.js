const axios = require('axios')
const cors = require('cors')
const express = require('express')

/**
 * YOUTUBE =====================================================================
 */

const appYoutube = express()
appYoutube.use(cors({ origin: true }))

appYoutube.get('/search', (req, res) => {
  let query = req.query.q

  axios({
    method: 'get',
    url: `https://www.googleapis.com/youtube/v3/search`,
    params: {
      key: process.env.YouTubeApiKey,
      maxResults: 50,
      order: 'date',
      part: 'snippet',
      q: query,
      type: 'video'
    }
  }).then(response => {
    res.send(response.data.items)
  })
})

appYoutube.get('/channel', (req, res) => {
  let query = req.query.q

  axios({
    method: 'get',
    url: `https://www.googleapis.com/youtube/v3/search`,
    params: {
      key: process.env.YouTubeApiKey,
      maxResults: 50,
      order: 'date',
      part: 'snippet',
      channelId: query,
      type: 'video'
    }
  }).then(response => {
    res.send(response.data.items)
  })
})

appYoutube.get('/channelAndQuery', (req, res) => {
  let query = req.query.q
  let channel = req.query.channelId

  axios({
    method: 'get',
    url: `https://www.googleapis.com/youtube/v3/search`,
    params: {
      key: process.env.YouTubeApiKey,
      maxResults: 50,
      order: 'date',
      part: 'snippet',
      channelId: channel,
      q: query,
      type: 'video'
    }
  }).then(response => {
    res.send(response.data.items)
  })
})

appYoutube.get('/user', (req, res) => {
  let query = req.query.q

  axios({
    method: 'get',
    url: `https://www.googleapis.com/youtube/v3/channels`,
    params: {
      key: process.env.YouTubeApiKey,
      maxResults: 50,
      order: 'date',
      part: 'contentDetails',
      forUsername: query,
      type: 'video'
    }
  }).then(response => {
    res.send(response.data.items)
  })
})

module.exports = appYoutube
