const axios = require('axios')
const cors = require('cors')
const express = require('express')

/**
 * API - NEWS =====================================================================
 */
const appNews = express()
appNews.use(cors({ origin: true }))
appNews.get('/search', (req, res) => {
  let query = req.query.q

  axios({
    method: 'get',
    url: `https://newsapi.org/v2/everything?q=${query}&pageSize=100&sortBy=publishedAt`,
    headers: {
      'x-api-key': process.env.NewsApiKey
    }
  }).then(response => {
    res.send(response.data.articles)
  })
})

module.exports = appNews
