const axios = require('axios')
const cors = require('cors')
const express = require('express')
const validate = require('../utils/validate')
const url = require('../utils/constants')

const apiKey = process.env.testKey // process.env.prodKey //
const dbMode = '1'

const username = 'demo.jamjad.api'
// const username = 'dnc.jafaramjad.api'
const password = apiKey + '|' + dbMode

/**
 * API - NGP VAN =====================================================================
 */
const appVan = express()
appVan.use(cors({ origin: url.ORIGINS }))
appVan.use(validate.FirebaseIdToken)

appVan.get('/events', (req, res) => {
  const after = req.query.after
  const before = req.query.before
  const method = 'get'
  let url = `https://api.securevan.com/v4/events?startingAfter=${after}&startingBefore=${before}`

  axios({
    method: method,
    url: url,
    headers: {
      'Content-type': 'application/json'
    },
    auth: {
      username: username,
      password: password
    }
  })
    .then(resp => {
      // console.log(resp)
      res.send(resp.data)
    })
    .catch(error => {
      // console.log(error)
      res.send(`error with request ${error}`)
    })
})

/**
 * SIGNUPS
 */
appVan.get('/signups', (req, res) => {
  const query = req.query.q
  const method = 'get'
  const url = `https://api.securevan.com/v4/signups?eventId=${query}`

  axios({
    method: method,
    url: url,
    headers: {
      'Content-type': 'application/json'
    },
    auth: {
      username: username,
      password: password
    }
  })
    .then(resp => {
      // console.log(resp)
      res.send(resp.data)
    })
    .catch(error => {
      // console.log(error)
      res.send(`error with request ${error}`)
    })
})

/**
 * PEOPLE
 */
appVan.get('/people', (req, res) => {
  const query = req.query.q
  const method = 'get'
  const url = `https://api.securevan.com/v4/people/${query}?$expand=phones`

  axios({
    method: method,
    url: url,
    headers: {
      'Content-type': 'application/json'
    },
    auth: {
      username: username,
      password: password
    }
  })
    .then(resp => {
      // console.log(resp)
      res.send(resp.data)
    })
    .catch(error => {
      // console.log(error)
      res.send(`error with request ${error}`)
    })
})

module.exports = appVan
