const axios = require('axios')
const cors = require('cors')
const express = require('express')

/**
 * API - FACEBOOK =====================================================================
 */

const appFacebook = express()
appFacebook.use(cors({ origin: true }))
appFacebook.get('/posts', (req, res) => {
  let query = req.query.q

  const db = admin.firestore()
  let dbRef

  if (query === 'ClaudiaForCongress') {
    dbRef = db.collection('facebookTenney').doc('ClaudiaForCongress')
  }

  if (query === 'RepClaudiaTenney') {
    dbRef = db.collection('facebookTenney').doc('RepClaudiaTenney')
  }

  if (query === 'BrindisiForCongress') {
    dbRef = db.collection('facebookBrindisi').doc('BrindisiForCongress')
  }

  var getDoc = dbRef
    .get()
    .then(doc => {
      if (!doc.exists) {
        console.log('No such document!')
      } else {
        console.log('Document data:', doc.data())
        res.send(doc.data().data)
      }
    })
    .catch(err => {
      console.log('Error getting document', err)
    })

  // ClaudiaForCongress
  // RepClaudiaTenney
  // BrindisiForCongress
})

module.exports = appFacebook

/**
 * API - FACEBOOK =====================================================================


const appFacebook = express()
appFacebook.use(cors({ origin: true }))
appFacebook.get('/posts', (req, res) => {
	let query = req.query.q

	puppeteer.launch({
		args: [
			'--no-sandbox',
			'--disable-setuid-sandbox',
			'--no-first-run',
			'--no-sandbox',
			'--no-zygote',
			'--single-process'
		]
	}).then(async browser => {
		const scrapeMobileURL = `https://mobile.facebook.com/${query}/posts`
		const page = await browser.newPage()
		await page.goto(scrapeMobileURL)

		await page.waitForSelector('div.story_body_container')

		const scrapedTime = await page.evaluate(() => {
			const times = Array.from(document.querySelectorAll('a > abbr'))
			return times.map(time => time.innerHTML)
		})

		const scrapedLink = await page.evaluate(() => {
			const links = Array.from(
				document.querySelectorAll('div.story_body_container > div > a')
			)
			return links.map(link =>
				link.getAttribute('href').replace('&__tn__=%2As%2As-R', '')
			)
		})

		const scrapedPost = await page.evaluate(() => {
			const post = Array.from(
				document.querySelectorAll('div.story_body_container > div > span')
			)
			return post.map((el, idx) =>
				el.innerHTML
					.replace('…', '')
					.replace(/<a\b[^>]*>More<\/a>/i, '')
					.replace(/<\/?span[^>]*>/g, '')
			)
		})

		// console.log(scrapedTime);
		// console.log(scrapedLink);
		// console.log(scrapedPost);

		let itemJSON = {}

		itemJSON = scrapedPost.map((item, idx) => {
			return { item, date: scrapedTime[idx], link: scrapedLink[idx] }
		})

		await browser.close()
		res.json(itemJSON)
	})
})
const apiFacebook = functions.https.onRequest(appFacebook)
 **/
