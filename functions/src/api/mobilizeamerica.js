// const { Router } = require('express')
// const router = Router()
const axios = require('axios')
const cors = require('cors')
const express = require('express')

const API_KEY = process.env.mobilizeAmericaKey

/**
 * API - MOBILIZE AMERICA =====================================================================
 */

const appMobilizeAmerica = express()
appMobilizeAmerica.use(cors({ origin: true }))
appMobilizeAmerica.get('/eventsGOTV', function(req, res) {
  let urlEventsGOTV = `https://events.mobilizeamerica.io/api/v1/events?organization_id=235&timeslot_end=lte_1541548800&timeslot_start=gte_1541235600`

  axios({
    method: 'get',
    url: urlEventsGOTV,
    headers: {
      Authorization: 'Bearer' + ' ' + API_KEY
    }
  }).then(function(response) {
    // var json = JSON.stringify(response, null, 2);
    // res.json(response.data)
    res.send(response.data)
  })
})

module.exports = appMobilizeAmerica
