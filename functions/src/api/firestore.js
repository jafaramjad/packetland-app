const cors = require('cors')
const express = require('express')
const admin = require('firebase-admin')
const validate = require('../utils/validate')
const url = require('../utils/constants')

/**
 * SETUP CORS AND VALIDATE REQUEST
 */
const appFire = express()
appFire.use(cors({ origin: url.ORIGINS }))
appFire.use(validate.FirebaseIdToken)

/**
 * API - LIST =====================================================================
 */
appFire.get('/daily/list', (req, res) => {

  let location = req.query.location
  let territory = req.query.territory

  let query = location && territory ? `/${location}/${territory}` : ''
  let list = []

  const db = admin.firestore()

  const dbRef = db.collection(`NC09/MCCREADY/DAILY${query}`)

  dbRef
    .get()
    .then(snapshot => {
      snapshot.forEach(doc => {
        console.log(doc.id, '=>', doc.data())
        list.push(doc.data())
      })

      res.send({
        list
      })

    })
    .catch(err => {
      console.log('Error getting documents', err)
    })

  // END
})

/**
 * API - SUB COLLECTIONS =====================================================================
 */
appFire.get('/daily/sub', (req, res) => {

  let path = req.query.path
  let doc = req.query.doc

  let list = []
  let data = []

  const db = admin.firestore()

  let dbRef = db.collection(`NC09/MCCREADY/${path}`).doc(`${doc}`)

  dbRef
    .getCollections()
    .then(collections => {
      collections.forEach(doc => {
        console.log(doc)
        list.push(doc.id)
        data.push(doc.data())
      })

      res.send({
        list,
        data
      })

    })
    .catch(err => {
      console.log('Error getting documents', err)
    })
})


module.exports = appFire
