const axios = require('axios')
const cors = require('cors')
const express = require('express')

/**
 * API - PROPUBLICA =====================================================================
 */
const appProPub = express()
appProPub.use(cors({ origin: true }))

appProPub.get('/vote', (req, res) => {
  let id = req.query.q

  axios({
    method: 'get',
    url: `https://api.propublica.org/congress/v1/members/${id}/votes.json`,
    headers: {
      'x-api-key': process.env.PropublicaApiKey
    }
  }).then(response => {
    res.send(response.data.results[0].votes)
  })
})

module.exports = appProPub
