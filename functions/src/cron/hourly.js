const functions = require('firebase-functions')
const puppeteer = require('puppeteer')
/**
 * APPENGINE - CRON - HOURLY
 */
const jobHourly = functions.pubsub.topic('hourly-tick').onPublish(event => {
  console.log('This job is run every hour!')

  async function scrape(collName, docName) {
    // LAUNCH
    const browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    })

    // then(async (browser) => {

    // VARS
    const scrapeMobileURL = `https://mobile.facebook.com/${docName}/posts`
    // const scrapeMobileURL = 'https://mobile.facebook.com/RepClaudiaTenney/posts';
    const page = await browser.newPage()

    // AWAIT
    await page.setViewport({ width: 2780, height: 10150 })
    await page.goto(scrapeMobileURL)
    await page.evaluate('window.scrollTo(0, 10000)')
    await page.waitFor(15000)
    await page.waitForSelector('div.story_body_container')

    // TIME
    const scrapedTime = await page.evaluate(() => {
      const times = Array.from(document.querySelectorAll('a > abbr'))
      return times.map(time => time.innerHTML)
    })

    // LINK
    const scrapedLink = await page.evaluate(() => {
      const links = Array.from(
        document.querySelectorAll('div.story_body_container > div > a')
      )
      return links.map(link =>
        link.getAttribute('href').replace('&__tn__=%2As%2As-R', '')
      )
    })

    // POST
    const scrapedPost = await page.evaluate(() => {
      const post = Array.from(
        document.querySelectorAll('div.story_body_container > div > span')
      )
      return post.map((el, idx) =>
        el.innerHTML
          .replace('…', '')
          .replace(/<a\b[^>]*>\More\<\/a>/i, '')
          .replace(/<\/?span[^>]*>/g, '')
      )
    })

    // console.log(scrapedTime);
    // console.log(scrapedLink);
    // console.log(scrapedPost);

    // MAKE JSON
    let itemJSON = {}
    itemJSON = scrapedPost.map((item, idx) => {
      return { item, date: scrapedTime[idx], link: scrapedLink[idx] }
    })

    // CLOSE
    await browser.close()

    // RENDER
    // const json = JSON.stringify(itemJSON, null, 2);
    // const fbData = JSON.stringify(itemJSON);

    // res.send(json)
    // res.json(itemJSON);
    // console.log(fbData)
    console.log(itemJSON)

    // FIRESTORE
    // https://firebase.google.com/docs/firestore/quickstart
    // admin.initializeApp(functions.config().firebase);
    const db = admin.firestore()
    const docRef = db.collection(collName).doc(docName)
    const fbData = docRef.set({ data: itemJSON })
    return fbData
  }

  // return run('facebookTenney', 'RepClaudiaTenney')
  return Promise.all([
    scrape('facebookTenney', 'RepClaudiaTenney')
    // scrape('facebookTenney', 'ClaudiaForCongress'),
    // scrape('facebookBrindisi', 'BrindisiForCongress')
  ])
})

module.exports = jobHourly
