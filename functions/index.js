const glob = require('glob')
const path = require('path')
const admin = require('firebase-admin')
const functions = require('firebase-functions')

// const axios = require('axios')
// const cors = require('cors')
// const express = require('express')
// const puppeteer = require('puppeteer')
// const twitterAPI = require('node-twitter-api')
// const validateFirebaseIdToken = require('./src/utils/validate')

const appFire = require('./src/api/firestore')
// const appVan = require('./src/api/van')
const appUsers = require('./src/api/users')
const appUserCheck = require('./src/api/usercheck')
// const appMobilizeAmerica = require('./src/api/mobilizeamerica')

// const hourly_job = require('./src/cron/hourly')
// const appYoutube = require('./src/api/youtube')
// const appTwitter = require('./src/api/twitter')
// const appFacebook = require('./src/api/facebook')
// const appNews = require('./src/api/news')
// const appProPub = require('./src/api/propub')

// TOKEN REQUIRED
const apiFire = functions.https.onRequest(appFire)
// const apiVan = functions.https.onRequest(appVan)
const apiUserCheck = functions.https.onRequest(appUserCheck)
const apiUsers = functions.https.onRequest(appUsers)

// NOT TOKENIZED YET
// const apiMobilizeAmerica = functions.https.onRequest(appMobilizeAmerica)
// const apiYoutube = functions.https.onRequest(appYoutube)
// const apiTwitter = functions.https.onRequest(appTwitter)
// const apiFacebook = functions.https.onRequest(appFacebook)
// const apiNews = functions.https.onRequest(appNews)
// const apiProPub = functions.https.onRequest(appProPub)

/**
 * =====================================================================
 */
// Initialize Firebase so it is available within functions
try {
  admin.initializeApp(functions.config().firebase)
} catch (e) {
  /* istanbul ignore next: not called in tests */
  console.error(
    'Caught error initializing app with functions.config():',
    e.message || e
  )
}

// Set Firestore timestamp settings
// NOTE: Skipped when running tests tests so it does not have to be mocked
if (process.env.NODE_ENV !== 'test') {
  // admin.firestore().settings({ timestampsInSnapshots: true })
}

const codeFolder = process.env.NODE_ENV === 'test' ? './src' : './dist'

// Load all folders within dist directory (mirrors layout of src)
const files = glob.sync(codeFolder + '/**/index.js', {
  cwd: __dirname,
  ignore: [
    './node_modules/**',
    codeFolder + '/utils/**',
    codeFolder + '/constants'
  ]
})

// Loop over all folders found within dist loading only the relevant function
files.forEach(functionFile => {
  // Get folder name from file name (removing any dashes)
  const folderName = path
    .basename(path.dirname(functionFile))
    .replace(/[-]/g, '')

  // Load single function from default
  !process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === folderName // eslint-disable-line no-unused-expressions
    ? (exports[folderName] = require(functionFile).default) // eslint-disable-line global-require
    : () => { }
})

module.exports = {
  // apiTwitter,
  // apiFacebook,
  // apiYoutube,
  // apiNews,
  // apiProPub,
  // apiMobilizeAmerica,
  // hourly_job,
  apiFire,
  // apiVan,
  apiUsers,
  apiUserCheck
}
