export const counties = [
  'Anson',
  'Bladen',
  'Cumberland',
  'Mecklenburg',
  'Richmond',
  'Robeson',
  'Scotland',
  'Union'
]

export const locations = [
  'Anson',
  'Bladen',
  'Cumberland',
  'Mecklenburg',
  'Richmond',
  'Robeson',
  'Scotland',
  'Union'
]

export const stations = {
  Anson: ['Town1', 'Town2', 'Town3', 'Town4', 'Town5', 'Town6'],
  Bladen: ['Town1', 'Town2'],
  Cumberland: ['Town1', 'Town2'],
  Mecklenburg: ['Charlotte', 'Mint Hill', 'Huntersville', 'Gastonia'],
  Richmond: ['Town1', 'Town2'],
  Robeson: ['Town1'],
  Scotland: ['Town1', 'Town2', 'Town3', 'Town4'],
  Union: ['Town1']
}

export const roles = {
  Oneida: ['utica', 'new_hartford', 'whitestown', 'rome', 'trenton', 'camden'],
  Oswego: ['mexico', 'central_square'],
  Herkimer: ['ilion', 'little_falls'],
  Madison: ['cazenovia', 'oneida', 'hamilton_phones', 'hamilton_doors'],
  Chenango: ['norwich_doors', 'norwich_phones'],
  Cortland: ['cortland'],
  Broome: ['binghamton', 'endicott', 'whitney_point', 'windsor'],
  Tioga: ['owego']
}

export const STATION_MAP = {
  ONEIDA: {
    UTICA: 'Desales Center',
    NEW_HARTFORD: 'New Hartford Local Headquarters',
    WHITESTOWN: 'UFCW Local 1',
    ROME: 'ROI Office Interiors',
    TRENTON: 'Samson Residence',
    CAMDEN: 'Farnsworth Residence'
  },
  OSWEGO: {
    MEXICO: 'Dineen Residence',
    CENTRAL_SQUARE: 'Ryan Residence'
  },
  HERKIMER: {
    ILION: 'Moose River Coffee House',
    LITTLE_FALLS: 'Feldmeier Law Office'
  },
  MADISON: {
    CAZENOVIA: 'Brindisi for Congress Cazenovia Office',
    ONEIDA: 'Cohen Residence',
    HAMILTON_PHONES: 'McVaugh Residence',
    HAMILTON_DOORS: 'Norman Von Wettberg Office'
  },
  CHENANGO: {
    NORWICH_DOORS: 'Brindisi for Congress Norwich Office',
    NORWICH_PHONES: 'Giltner Residence'
  },
  CORTLAND: {
    CORTLAND: 'Cortland County Democratic Committee Headquarters'
  },
  BROOME: {
    BINGHAMTON: 'Binghamton Local Headquarters',
    ENDICOTT: 'Schimmerling Law Office',
    WHITNEY_POINT: 'Ford Hill Country Club',
    WINDSOR: 'Windsor Staging Location'
  },
  TIOGA: {
    OWEGO: 'Cloud Croft Studios'
  }
}
