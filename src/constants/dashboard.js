export const CURRENT_DAY = 'day4'

export const shifts = ['9AM', '12PM', '3PM', '6PM', '9PM']
export const bgBlue = 'rgba(56, 166, 234, 0.43)'
export const bgGreen = 'rgba(63, 191, 63, 0.43)'
export const bgOrange = 'rgba(245, 108, 34, 0.43)'
export const bgRed = 'rgba(253, 11, 11, 0.43)'
export const borderBlue = 'rgb(56, 166, 234, 1)'
export const borderGreen = 'rgb(63, 191, 63,1)'
export const borderOrange = 'rgb(245, 108, 34,1)'
export const borderRed = 'rgb(253, 11, 11, 1)'

export const chartsDaily = {
  maintainAspectRatio: false,
  legend: {
    position: 'bottom'
  },
  title: {
    display: true,
    text: ''
  }
}

export const graphDaily = {
  maintainAspectRatio: false,
  legend: {
    position: 'none'
  },
  title: {
    display: true,
    text: ''
  },
  elements: {
    line: {
      fill: false
    }
  },
  scales: {
    yAxes: [
      {
        type: 'linear',
        display: true,
        position: 'left',
        id: 'y-axis-1',
        gridLines: {
          display: true
        },
        labels: {
          show: true
        },
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 25000,
          stepSize: 5000
        }
      }
      /* 	{
					ticks: {
						beginAtZero: true,
						min: 0,
						max: 150,
						stepSize: 10
					}
				} */
    ]
  }
}

export const graphOptions = {
  maintainAspectRatio: false,
  legend: {
    position: 'bottom'
  },
  title: {
    display: true,
    text: ''
  },
  elements: {
    line: {
      fill: false
    }
  },
  scales: {
    yAxes: [
      {
        type: 'linear',
        display: true,
        position: 'left',
        id: 'y-axis-1',
        gridLines: {
          display: false
        },
        labels: {
          show: true
        },
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 50,
          stepSize: 5
        }
      },
      {
        type: 'linear',
        display: true,
        position: 'right',
        id: 'y-axis-2',
        gridLines: {
          display: false
        },
        labels: {
          show: true
        },
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 300,
          stepSize: 25
        }
      }
      /* 	{
					ticks: {
						beginAtZero: true,
						min: 0,
						max: 150,
						stepSize: 10
					}
				} */
    ]
  }
}

export const graphOptionsWalk = {
  maintainAspectRatio: false,
  legend: {
    position: 'bottom'
  },
  title: {
    display: true,
    text: ''
  },
  elements: {
    line: {
      fill: false
    }
  },
  scales: {
    yAxes: [
      {
        type: 'linear',
        display: true,
        position: 'left',
        id: 'y-axis-1',
        gridLines: {
          display: false
        },
        labels: {
          show: true
        },
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 1000,
          stepSize: 100
        }
      }
      /*  {
          type: 'linear',
          display: true,
          position: 'right',
          id: 'y-axis-2',
          gridLines: {
              display: false
          },
          labels: {
              show: true
          },
          ticks: {
              beginAtZero: true,
              min: 0,
              max: 300,
              stepSize: 25
          }
      }
      {
              ticks: {
                  beginAtZero: true,
                  min: 0,
                  max: 150,
                  stepSize: 10
              }
          } */
    ]
  }
}

export const graphOptionsVols = {
  maintainAspectRatio: false,
  legend: {
    position: 'bottom'
  },
  title: {
    display: true,
    text: ''
  },
  elements: {
    line: {
      fill: false
    }
  },
  scales: {
    yAxes: [
      {
        type: 'linear',
        display: true,
        position: 'left',
        id: 'y-axis-1',
        gridLines: {
          display: false
        },
        labels: {
          show: true
        },
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 100,
          stepSize: 10
        }
      }
      /*  {
          type: 'linear',
          display: true,
          position: 'right',
          id: 'y-axis-2',
          gridLines: {
              display: false
          },
          labels: {
              show: true
          },
          ticks: {
              beginAtZero: true,
              min: 0,
              max: 300,
              stepSize: 25
          }
      }
      {
              ticks: {
                  beginAtZero: true,
                  min: 0,
                  max: 150,
                  stepSize: 10
              }
          } */
    ]
  }
}
