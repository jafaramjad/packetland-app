export const TOTAL_PACKETS = {
  oneida: {
    utica: 138,
    new_hartford: 94,
    whitestown: 51,
    rome: 63,
    trenton: 33,
    camden: 23
  },
  oswego: {
    mexico: 24,
    central_square: 25
  },
  herkimer: {
    ilion: 78,
    little_falls: 27
  },
  madison: {
    cazenovia: 45,
    oneida: 69,
    'hamilton_(phones)': 0,
    'hamilton_(doors)': 56
  },
  chenango: {
    'norwich_(phones)': 0,
    'norwich_(doors)': 60
  },
  cortland: { cortland: 90 },
  broome: {
    binghamton: 202,
    endicott: 130,
    whitney_point: 22,
    windsor: 24
  },
  tioga: { owego: 36 }
}

/*
export const TOTAL_PACKETS_MAP = {
    ONEIDA: {
        UTICA: [0, 20, 30, 30],
        NEW_HARTFORD: [0, 20, 40, 40],
        WHITESTOWN: [0, 17, 15, 15],
        ROME: [0, 0, 15, 0],
        TRENTON: [0, 7, 0, 0],
        CAMDEN: [0, 7, 0, 0]
    },
    OSWEGO: {
        MEXICO: [0, 0, 0, 0],
        CENTRAL_SQUARE: [0, 0, 10, 0]
    },
    HERKIMER: {
        ILION: [0, 15, 13, 13],
        LITTLE_FALLS: [0, 0, 0, 0]
    },
    MADISON: {
        CAZENOVIA: [0, 15, 11, 11],
        ONEIDA: [0, 0, 10, 0],
        HAMILTON_PHONES: [0, 0, 0, 0],
        HAMILTON_DOORS: [0, 0, 10, 0]
    },
    CHENANGO: {
        NORWICH_PHONES: [0, 10, 11, 11],
        NORWICH_DOORS: [0, 10, 11, 11]
    },
    CORTLAND: {
        CORTLAND: [0, 10, 30, 10]
    },
    BROOME: {
        BINGHAMTON: [0, 27, 60, 45],
        ENDICOTT: [0, 0, 38, 0],
        WHITNEY_POINT: [0, 0, 0, 0],
        WINDSOR: [0, 0, 10, 10]
    },
    TIOGA: {
        OWEGO: [0, 16, 25, 0]
    }
}
*/

/*

Binghamton - 204
Endicott - 130
Whitney Point - 22
Windsor - 24

Owego - 36

Norwich - 53

Cortland - 94

Cazenovia - 44
Hamilton - 56
Oneida - 69

Ilion - 76
Little Falls - 26

Utica - 138
New Hartford - 94
Whitestown - 51
Trenton - 33
Rome - 61
Camden - 23

Mexico - 24
Central Square - 25

*/
