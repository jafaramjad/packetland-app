import { compose } from 'redux'
import { connect } from 'react-redux'
import { LIST_PATH } from 'constants'
import { withHandlers, withProps, withStateHandlers, pure } from 'recompose'
import { firestoreConnect, isEmpty, isLoaded } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'

export default compose(
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  // Map auth uid from state to props
  connect((store /* { firebase: { auth: { uid }, profile } } */) => {
    return {
      accessDash: store.accessDash,
      uid: store.firebase.auth.uid,
      profile: store.firebase.profile
    }
  }),
  // Wait for uid to exist before going further
  spinnerWhileLoading(['uid']),
  // Create listeners based on current users UID
  firestoreConnect(({ params, uid }) => [
    // Listener for projects the current user created
    /* 	{
				collection: 'projects',
				where: ['createdBy', '==', uid]
			} */
  ]),
  // Map projects from state to props
  /* 	connect(({ firestore: { ordered } }) => ({
			projects: ordered.projects
		})), */
  // Show loading spinner while projects and collabProjects are loading
  // spinnerWhileLoading(['Dashboard']),
  // Add props.router
  withRouter,
  // Add props.showError and props.showSuccess
  withNotifications,
  // Add state and state handlers as props
  withStateHandlers(
    // Setup initial state
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    // Add state handlers as props
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),

  // Add handlers as props
  withHandlers({
    getShiftData: props => data => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props

      return (
        firestore
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` }) // .get({ collection: `gotv/oneida/utica`, doc: `day1` })
          .get({ collection: `gotv/`, doc: `${data.county}` })
          .then(doc => {
            // console.log('query data has been retrieved')
            // console.log(doc)
            if (doc.exists) {
              const dbObj = doc.data()
              // console.log(doc)
              // console.log(dbObj[data.station][data.day])
              if (dbObj[data.station] && dbObj[data.station][data.day]) {
                return {
                  0: dbObj[data.station][data.day],
                  1: dbObj[data.station]
                }
              } else {
                showError('No data exists for this selection')

                return undefined
              }
              // console.log("Document data:", doc.data());
              // return doc.data()
            } else {
              // doc.data() will be undefined in this case
              showError('No data exists for this selection')
              // console.log("No such document!");
              return undefined
            }
          })
          .catch(err => {
            console.error('Error:', err) // eslint-disable-line no-console
            showError(err.message || 'Could not get shift')
            return Promise.reject(err)
          })
      )
    },
    autoUpdateData: props => (cb, data) => {
      // console.log(data)
      // console.log('auto getting shift data')
      const { firestore } = props
      return firestore
        .collection(`gotv/`) // `gotv/oneida/utica`
        .doc(`${data.county}`) // `day1`
        .onSnapshot(
          doc => {
            if (doc.exists) {
              // console.log("Document data:", doc.data());
              // console.log(doc)
              const dbObj = doc.data()
              // console.log(doc)
              // console.log(dbObj[data.station][data.day])
              if (dbObj[data.station] && dbObj[data.station][data.day]) {
                const docData = dbObj[data.station][data.day]
                // return dbObj[data.station][data.day]
                // cb(docData.packetsOut, docData.packetsIn, dbObj[data.station])
                cb({ 0: dbObj[data.station][data.day], 1: dbObj[data.station] })
              } else {
                // return undefined
              }

              // cb(doc.data().packetsOut, doc.data().packetsIn)
              // return doc.data()
            } else {
              // doc.data() will be undefined in this case
              // console.log("No such document!");
            }
          },
          err => {
            console.error('Error:', err) // eslint-disable-line no-console
          }
        )
    },
    getAllShiftData: props => data => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props

      // return firestore
      return firestore // .doc(`gotv/oneida`)
        .collection(`gotv/`)
        .get() // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` })
        .then(querySnapshot => {
          // console.log('query data has been retrieved')
          // console.log(querySnapshot)
          let dbObj = {}
          querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            // console.log(doc.id, " => ", doc.data());
            dbObj = { ...dbObj, [doc.id]: doc.data() }
          })
          // console.log(dbObj)
          if (!querySnapshot.empty) {
            // console.log("Document data:", doc.data());
            return dbObj
          } else {
            // doc.data() will be undefined in this case

            // console.log("No such document!");
            return undefined
          }
        })
        .catch(err => {
          console.error('Error:', err) // eslint-disable-line no-console
          showError(err.message || 'Could not add shift')
          return Promise.reject(err)
        })
    },
    autoUpdateAllData: props => (cb, data) => {
      // console.log(data)
      // console.log('auto getting shift data')
      const { firestore } = props
      return firestore
        .collection(`gotv/`) // `gotv/oneida/utica`
        .onSnapshot(
          querySnapshot => {
            // console.log(querySnapshot)

            let dbObj = {}
            querySnapshot.forEach(function(doc) {
              // doc.data() is never undefined for query doc snapshots
              // console.log(doc.id, " => ", doc.data());
              dbObj = { ...dbObj, [doc.id]: doc.data() }
            })
            // console.log(dbObj)
            if (!querySnapshot.empty) {
              // console.log("Document data:", doc.data());
              // return dbObj
              cb(dbObj)
            } else {
              // doc.data() will be undefined in this case

              // console.log("No such document!");
              return undefined
            }
          },
          err => {
            console.error('Error:', err) // eslint-disable-line no-console
          }
        )
    }
  }),
  withProps(({ auth, profile }) => ({
    authExists: isLoaded(auth) && !isEmpty(auth),
    profileEmail: profile.email
  })),
  pure // shallow equals comparison on props (prevent unessesary re-renders)
)
