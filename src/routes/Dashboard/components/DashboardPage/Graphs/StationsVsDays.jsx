import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classes from './Graphs.scss'
import SelectCountyDay from '../Select/SelectCountyDay'

import { Bar } from 'react-chartjs-2'
import Paper from '@material-ui/core/Paper'

import {
	shifts,
	graphOptions,
	bgBlue,
	bgGreen,
	bgOrange,
	bgRed,
	borderBlue,
	borderGreen,
	borderOrange,
	borderRed
} from '../../../../../constants/dashboard'

graphOptions.title.text = 'PACKETS vs TOTAL'

/* const plugins = [{
	afterDraw: (chartInstance, easing) => {
		const ctx = chartInstance.chart.ctx;
		ctx.fillText("This text drawn by a plugin", 100, 100);
	}
}]; */

class StationsVsDays extends Component {
	constructor(props) {
		super(props)

		this.state = {
			data1: '',
			packetsOut: '',
			packetsIn: '',
			linePlot: '',
			county: 'oneida',
			station: 'utica',
			day: 'day1',
			cancelCB: ''
		}
	}

	componentWillMount() {
		this.getData()
	}

	componentDidMount() {
		this.autoData()
	}

	componentWillUnmount() {
		this.unsubAutoData(this.state)
	}

	unsubAutoData = curState => {
		const { cancelCB } = { ...curState }
		// console.log('unsubscribing from callback...')
		// console.log(cancelCB)
		cancelCB()
	}

	autoData = () => {
		const getQuery = {
			county: this.state.county,
			station: this.state.station,
			day: this.state.day
		}

		let cancelCB = this.props.autoUpdateData(this.recCallBack, getQuery)

		this.setState({
			cancelCB: cancelCB
		})
	}

	recCallBack = (res) => {
		// console.log(packOut, packIn)
		if (res !== undefined) {
			const linePlot = this.calcTotalPerc(res)

			this.setState(
				{
					linePlot: linePlot,
					packetsOut: res[0].packetsOut,
					packetsIn: res[0].packetsIn
				},
				() => {
					this.setupData()
				}
			)
		}
	}

	setupData = () => {
		const dataPackOut = Object.values(this.state.packetsOut).map(item => {
			return item
		})

		const dataPackIn = Object.values(this.state.packetsIn).map(item => {
			return item
		})

		// console.log(dataPackIn)
		// console.log(dataPackOut)

		const data1 = {
			labels: shifts,
			datasets: [
				{
					label: 'Total %',
					type: 'line',
					data: this.state.linePlot,
					fill: false,
					borderColor: '#EC932F',
					backgroundColor: '#EC932F',
					pointBorderColor: '#EC932F',
					pointBackgroundColor: '#EC932F',
					pointHoverBackgroundColor: '#EC932F',
					pointHoverBorderColor: '#EC932F',
					yAxisID: 'y-axis-2'
				},
				{
					label: 'OUT',
					backgroundColor: bgBlue,
					borderColor: borderBlue,
					borderWidth: 1,
					hoverBackgroundColor: bgOrange,
					hoverBorderColor: borderOrange,
					yAxisID: 'y-axis-1',
					data: dataPackOut
				},
				{
					label: 'IN',
					backgroundColor: bgGreen,
					borderColor: borderGreen,
					borderWidth: 1,
					hoverBackgroundColor: bgRed,
					hoverBorderColor: borderRed,
					yAxisID: 'y-axis-1',
					data: dataPackIn
				}
			]
		}

		this.setState({
			data1: data1
		})
	}

	calcTotalPerc = allResp => {

		const curDay = this.state.day
		const allDays = allResp[1]
		const totalPacks = allResp[0].info.totalPackets

		// console.log(allResp)
		// console.log(curDay)
		// console.log(allDays)
		// console.log(totalPacks)

		let vals1 = allDays['day1'] ? Object.values(allDays['day1'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]
		let vals2 = allDays['day2'] ? Object.values(allDays['day2'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]
		let vals3 = allDays['day3'] ? Object.values(allDays['day3'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]
		let vals4 = allDays['day4'] ? Object.values(allDays['day4'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]

		// console.log(vals1)
		// console.log(vals2)
		// console.log(vals3)
		// console.log(vals4)

		const totalValsArr = vals1.concat(vals2, vals3, vals4)
		const totalCompArr = []

		const compound = (accval, curval, idx) => {
			// console.log("acc: " + accval)
			// console.log("cur: " + curval)
			// console.log("idx: " + idx)
			const comp = accval + curval

			if (idx === 1) {
				totalCompArr.push(accval)
				totalCompArr.push(comp)
			} else {
				totalCompArr.push(comp)
			}

			return comp
		}

		const TOTAL_ADDED = totalValsArr.reduce(compound)

		// console.log(totalValsArr)
		// console.log(totalCompArr)
		// console.log(TOTAL_ADDED)

		const ALL_PERC = totalCompArr.map(val => {
			return Number.parseFloat((val / totalPacks) * 100).toFixed(2)
		})

		// console.log(ALL_PERC)
		// console.log(ALL_PERC.slice(0, 5))
		// console.log(ALL_PERC.slice(5, 10))
		// console.log(ALL_PERC.slice(10, 15))
		// console.log(ALL_PERC.slice(15, 21))

		let linePlotVal = []

		switch (curDay) {
			case 'day1':
				linePlotVal = ALL_PERC.slice(0, 5)
				break
			case 'day2':
				linePlotVal = ALL_PERC.slice(5, 10)
				break
			case 'day3':
				linePlotVal = ALL_PERC.slice(10, 15)
				break
			case 'day4':
				linePlotVal = ALL_PERC.slice(15, 21)
				break
			default:
				linePlotVal = [0, 0, 0, 0, 0, 0]
		}

		return linePlotVal
		// return [10, 25, 40, 49, 60, 97, 140] //TEST
	}

	getData = () => {
		const getQuery = {
			county: this.state.county,
			station: this.state.station,
			day: this.state.day
		}

		// console.log(getQuery)

		const queryData = this.props.getShiftData(getQuery)

		queryData.then(res => {

			// console.log(res)

			if (res !== undefined) {

				const linePlot = this.calcTotalPerc(res)

				this.setState(
					{
						linePlot: linePlot,
						packetsOut: res[0].packetsOut,
						packetsIn: res[0].packetsIn
					},
					() => {
						this.setupData()
					}
				)
			} else {
				this.setState(
					{
						linePlot: [0, 0, 0, 0, 0],
						packetsOut: [0, 0, 0, 0, 0],
						packetsIn: [0, 0, 0, 0, 0]
					},
					() => {
						this.setupData()
					}
				)
			}
		})
	}

	handleSelect = options => {
		// console.log('handle new select options...')
		// console.log(options)
		// console.log('old state...')
		// console.log(this.state)

		this.unsubAutoData(this.state)

		this.setState(
			{
				county: options.county,
				station: options.station,
				day: options.day
			},
			() => {
				this.getData()
				this.autoData()
			}
		)
	}

	render() {
		return (
			<div className={classes.container}>
				<br />
				<SelectCountyDay handleSelect={this.handleSelect} />
				<br />
				<Paper elevation={1} className={classes.barGraphPaper}>
					{this.state.data1 ? (
						<Bar
							data={this.state.data1}
							// width={100}
							// height={50}
							options={graphOptions}
						// plugins={plugins}
						/>
					) : null}

				</Paper>
			</div>
		)
	}
}
StationsVsDays.propTypes = {
	getShiftData: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
	autoUpdateData: PropTypes.func.isRequired // from enhancer (withHandlers - firebase)
}

export default StationsVsDays
