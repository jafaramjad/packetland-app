import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classes from './Graphs.scss'
import SelectCountyDay from '../Select/SelectCountyDay'

import { Bar } from 'react-chartjs-2'
import Paper from '@material-ui/core/Paper'
import { STATION_MAP } from '../../../../../constants/places'

import {
    calcVolShifts,
    calcDayNum,
    calcVolDays,
    calcShiftName,
    calcShiftNameEDay,
    calcTotalPerc,
    calcTotals,
    calcDaily,
    calcTotalPercByDay,
    calcTotalPercByDayStation,
    calcTotalPacks,
    compoundArray
} from '../Utils/CalcFunc'
import {
    shifts,
    graphOptionsVols,
    bgBlue,
    bgGreen,
    bgOrange,
    bgRed,
    borderBlue,
    borderGreen,
    borderOrange,
    borderRed
} from '../../../../../constants/dashboard'

graphOptionsVols.title.text = ['VOLS', 'Begun vs Scheduled']

/* const plugins = [{
	afterDraw: (chartInstance, easing) => {
		const ctx = chartInstance.chart.ctx;
		ctx.fillText("This text drawn by a plugin", 100, 100);
	}
}]; */

class GraphVols extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data1: '',
            dayEvents: '',
            volsBegun: '',
            volsScheduled: [0, 0, 0, 0, 0],
            linePlot: '',
            county: 'oneida',
            station: 'utica',
            day: 'day1',
            cancelCB: ''
        }

        // console.log(props)
    }


    componentDidUpdate(prevProps) {
        if (this.props.dbData !== prevProps.dbData) {
            // console.log(this.props)
            const county = this.state.county
            const station = this.state.station
            const day = this.state.day
            const DB = this.props.dbData

            // const linePlot = calcTotalPerc(DB[county][station], county, station, day)
            const volsBegun = DB[county] && DB[county][station] && DB[county][station][day] ? DB[county][station][day].volsBegun : 0
            // const walkContacts = DB[county][station][day].walkContacts

            this.setState({
                volsBegun: volsBegun,
                // walkContacts: walkContacts,
                // linePlot: linePlot,
                dbData: this.props.dbData
            }, () => {
                this.setupData()
                // this.getData()
                // this.autoData()
            })




        }

        if (this.props.dayEvents !== prevProps.dayEvents) {


            const signups = this.props.dayEvents
            const county = this.state.county
            const station = this.state.station

            const dayNum = calcDayNum(this.state.day)

            const volShifts = calcVolShifts(signups[dayNum], county, station, STATION_MAP)

            // console.log(signups)
            // console.log(volShifts)

            // if (volShifts[shiftName] && volShifts[shiftName].length > 0) {
            // 					//	console.log(volShifts[shiftName])
            // 					volsScheduled = volShifts[shiftName].length
            // 				}
            this.setState({
                dayEvents: this.props.dayEvents,
                volsScheduled: volShifts
            }, () => {
                this.setupData()
            })
        }


    }

    setupData = () => {
        const dataPackOut = Object.values(this.state.volsBegun).map(item => {
            return item
        })


        let volShifts = [0, 0, 0, 0, 0]
        const signups = this.state.dayEvents

        if (signups.length > 0) {


            const county = this.state.county
            const station = this.state.station

            const dayNum = calcDayNum(this.state.day)

            volShifts = calcVolShifts(signups[dayNum], county, station, STATION_MAP)

        }

        const volsScheduled = volShifts


        //  console.log(this.state.dayEvents)
        // console.log(this.state.volsScheduled)

        const volShift = Object.values(volsScheduled).map(item => {
            return item.length || 0
        })

        // const volShift = Object.values(this.state.volsScheduled).map(item => {
        //     return item.length
        // })

        // console.log(volShift)
        //  = this.state.volsScheduled


        // let volsCount

        // if (this.state.day === 'day4') {
        //     volsCount = [
        //         volShift['7AM'].length,
        //         volShift['9AM'].length,
        //         volShift['12PM'].length,
        //         volShift['3PM'].length,
        //         volShift['6PM'].length
        //     ]
        // } else {
        //     volsCount = [
        //         volShift['9AM'].length,
        //         volShift['12PM'].length,
        //         volShift['3PM'].length,
        //         volShift['6PM'].length,
        //         volShift['9PM'].length
        //     ]
        // }




        // Object.values(this.state.volsScheduled).map(item => {
        //     return item
        // })

        // console.log(dataPackIn)
        // console.log(dataPackOut)

        const data1 = {
            labels: shifts,
            datasets: [
                // {
                //     label: 'Total %',
                //     type: 'line',
                //     data: this.state.linePlot,
                //     fill: false,
                //     borderColor: '#EC932F',
                //     backgroundColor: '#EC932F',
                //     pointBorderColor: '#EC932F',
                //     pointBackgroundColor: '#EC932F',
                //     pointHoverBackgroundColor: '#EC932F',
                //     pointHoverBorderColor: '#EC932F',
                //     yAxisID: 'y-axis-2'
                // },
                {
                    label: 'BEGUN',
                    backgroundColor: bgBlue,
                    borderColor: borderBlue,
                    borderWidth: 1,
                    hoverBackgroundColor: bgOrange,
                    hoverBorderColor: borderOrange,
                    yAxisID: 'y-axis-1',
                    data: dataPackOut
                },
                {
                    label: 'SCHEDULED',
                    backgroundColor: bgGreen,
                    borderColor: borderGreen,
                    borderWidth: 1,
                    hoverBackgroundColor: bgRed,
                    hoverBorderColor: borderRed,
                    yAxisID: 'y-axis-1',
                    data: volShift
                }
            ]
        }

        this.setState({
            data1: data1
        })
    }



    getData = () => {

        // const getQuery = {
        // 	county: this.state.county,
        // 	station: this.state.station,
        // 	day: this.state.day
        // }

        const county = this.state.county
        const station = this.state.station
        const day = this.state.day
        const DB = this.state.dbData


        if (DB[county] && DB[county][station]) {

            // console.log(DB[county][station])


            // const linePlot = calcTotalPerc(DB[county][station], county, station, day)
            const volsBegun = DB[county][station][day] && DB[county][station][day].volsBegun || 0
            // const volsScheduled = DB[county][station][day] && DB[county][station][day].volsScheduled || 0

            // console.log('raising the bar...')
            // console.log(linePlot)

            this.setState(
                {
                    //  linePlot,
                    volsBegun
                    // walkContacts
                },
                () => {
                    this.setupData()
                }
            )
        } else {
            // console.log('settin to zero...')
            this.setState(
                {
                    // linePlot: [0, 0, 0, 0, 0],
                    volsBegun: [0, 0, 0, 0, 0],
                    volsScheduled: [0, 0, 0, 0, 0]
                },
                () => {
                    this.setupData()
                }
            )
        }
        // })
    }


    handleSelect = options => {
        // console.log('handle new select options...')
        // console.log(options)
        // console.log('old state...')
        // console.log(this.state)

        // this.unsubAutoData(this.state)

        this.setState(
            {
                county: options.county,
                station: options.station,
                day: options.day
            },
            () => {
                this.getData()
                // this.autoData()
            }
        )
    }

    render() {
        return (
            <div className={classes.container}>
                <br />
                <SelectCountyDay handleSelect={this.handleSelect} />
                <br />
                <Paper elevation={1} className={classes.barGraphPaper}>
                    {this.state.data1 ? (
                        <Bar
                            data={this.state.data1}
                            // width={100}
                            // height={50}
                            options={graphOptionsVols}
                        // plugins={plugins}
                        />
                    ) : null}

                </Paper>
            </div>
        )
    }
}
/* GraphPackets.propTypes = {
	getShiftData: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
	autoUpdateData: PropTypes.func.isRequired // from enhancer (withHandlers - firebase)
}
 */
export default GraphVols
