import React, { Component } from 'react'
import axios from 'axios'
import moment from 'moment'
import classes from './Tables.scss'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import NativeSelect from '@material-ui/core/NativeSelect'
import ToggleButton from '@material-ui/lab/ToggleButton'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'
import Paper from '@material-ui/core/Paper'
import { stations, locations, STATION_MAP } from '../../../../../constants/places'

import {
	calcVolShifts,
	calcShiftName,
	calcVolDays,
	calcShiftNameEDay,
	calcTotalPerc,
	calcTotals,
	calcTotalPercByDay,
	findCurrentDay,
	findCurrentShift,
	calcGrandTotalPerc,
	calcAllPacks,
	compoundArray
} from '../Utils/CalcFunc'

import { numToPerc, secToDate } from '../Utils/FormatFunc'


class TableTotals extends Component {

	constructor(props) {
		super(props)
		this.state = {
			selectDay: 'day1',
			// shiftBtn: '0',
			//  cancelCB: '',
			allEvents: '',
			dbData: '',
			grandTotalPacketsIn: [0],
			grandTotalCallers: [0],
			grandTotalPackets: [0],
			grandTotalPacketsOut: [0],
			grandTotalVolsBegun: [0],
			grandTotalVolsScheduled: [0],
			grandTotalCallers: [0],
			grandTotalWalkAttempts: [0],
			grandTotalWalkContacts: [0]

			// Oneida: '',
			// Herkimer: '',
			// Oswego: '',
			// Madison: '',
			// Chenango: '',
			// Cortland: '',
			// Broome: '',
			// Tioga: ''
		}
	}


	componentDidUpdate(prevProps) {
		if (this.props.dbData !== prevProps.dbData) {
			// console.log(this.props)

			const DB = this.props.dbData

			this.setState({
				// events: this.props.events
				dbData: this.props.dbData,
				Oneida: DB.oneida ? DB.oneida : '',
				Herkimer: DB.herkimer ? DB.herkimer : '',
				Oswego: DB.oswego ? DB.oswego : '',
				Madison: DB.madison ? DB.madison : '',
				Chenango: DB.chenango ? DB.chenango : '',
				Cortland: DB.cortland ? DB.cortland : '',
				Broome: DB.broome ? DB.broome : '',
				Tioga: DB.tioga ? DB.tioga : ''
			}, () => {
				this.setupData()
				// this.getData()
				// this.autoData()
			})

		}

		if (this.props.allEvents !== prevProps.allEvents) {
			// let allEvents = this.props.dayEvents[0]
			// allEvents.concat(this.props.dayEvents[1], this.props.dayEvents[2], this.props.dayEvents[3])
			this.setState({
				allEvents: this.props.allEvents
			}, () => {
				this.setupData()
			})
		}
	}

	setupData() {

		let grandTotalPacketsIn = []
		let grandTotalPacketsOut = []
		let grandTotalPackets = []
		let grandTotalVolsBegun = []
		let grandTotalVolsScheduled = []
		let grandTotalCallers = []
		let grandTotalWalkAttempts = []
		let grandTotalWalkContacts = []

		// console.log('setting up data...')
		locations.map(county => {
			// const day1Events = []
			const allEvents = this.state.allEvents

			const signups = allEvents
			const district = this.state[county]
			const dataLives = district ? true : false

			if (dataLives) {
				// console.log(`Data in county: ${county}`)
				stations[county].map(stationName => {
					// console.log(stationName)
					const station = district[stationName.replace(' ', '_').toLowerCase()]
					// console.log(stationName.toLowerCase())
					// const stationDataLives = station ? true : false
					const stationDataLives = station ? true : false

					// console.log(station)
					if (stationDataLives) {
						/* 	console.log(`Data in station: ${stationName}`)
							console.log(station) */

						/*
	
						let mostRecentDay = this.state.selectDay
						// console.log(station[mostRecentDay])
	
						let mostRecentShift = Number(this.state.shiftBtn)
	
						*/
						let mostRecentDay = findCurrentDay(station)
						// console.log(mostRecentDay)
						// console.log(station[mostRecentDay])
						let mostRecentShift = findCurrentShift(station[mostRecentDay])
						// console.log(mostRecentShift)


						let shiftName
						if (mostRecentDay === 'day4') {
							shiftName = calcShiftNameEDay(mostRecentShift)
						} else {
							shiftName = calcShiftName(mostRecentShift)
						}



						// console.log(mostRecentShift)
						// console.log(station)
						// console.log(county)
						// console.log(stationName)

						// VARS
						let stationStatus
						let lastUpdate
						let stationPerc
						let packetsIn
						let packetsOut
						let totalPackets
						let callersHubdialer
						let callersPaper
						let totalCallers
						let volsBegun
						let volsScheduled
						let walkAttempts
						let walkContacts

						// CHECK IF DAY EXISTS
						if (station && station[mostRecentDay]) {

							// SET STATION STATUS
							stationStatus = station[mostRecentDay]['info']['stationOpen']
							// console.log(stationStatus)

							// SET STATION STATUS
							lastUpdate = station[mostRecentDay]['createdAt']

							// SET TOTAL PACKETS
							totalPackets = calcAllPacks(county, stationName) // Number(station[mostRecentDay]['info']['totalPackets'])

							// FIND TOTAL% vs PACKETS IN
							stationPerc = calcGrandTotalPerc(station, mostRecentDay, county, stationName).slice(-1)[0]


							// stationPerc = calcGrandTotalPerc(station).slice(-1)[0]
							// stationPerc = calcTotalPerc(station).slice(-1)[0]
							// console.log(stationPerc)



							// PACKETS IN
							if (station[mostRecentDay]['packetsIn']) {
								packetsIn = calcTotals(station, 'packetsIn')
							}

							// PACKETS OUT
							if (station[mostRecentDay]['packetsOut']) {
								packetsOut = calcTotals(station, 'packetsOut')
							}

							// TALLY CALLERS
							if (station[mostRecentDay]['volsHubdialer'] || station[mostRecentDay]['volsPaper']) {
								callersHubdialer = calcTotals(station, 'volsHubdialer')
								callersPaper = calcTotals(station, 'volsPaper')
								totalCallers = callersHubdialer + callersPaper

							}

							// SET VOLS BEGUN
							if (station[mostRecentDay]['volsBegun']) {
								volsBegun = calcTotals(station, 'volsBegun')
							}

							// SET WALK ATTEMPTS
							if (station[mostRecentDay]['walkAttempts']) {
								walkAttempts = calcTotals(station, 'walkAttempts')
							}

							// SET WALK CONTACTS
							if (station[mostRecentDay]['walkContacts']) {
								walkContacts = calcTotals(station, 'walkContacts')
							}


							// const volShifts = calcVolShifts(signups, county, stationName, STATION_MAP)
							// console.log(volShifts)
							// console.log(shiftName)
						}
						// console.log(signups)

						if (signups.length > 0) {

							const volDays = calcVolDays(signups, county, stationName, STATION_MAP)
							volsScheduled = volDays.length
							// console.log(volsScheduled)
						}



						grandTotalPacketsIn.push(packetsIn ? Number(packetsIn) : 0)
						grandTotalPacketsOut.push(packetsOut ? Number(packetsOut) : 0)
						grandTotalPackets.push(totalPackets ? Number(totalPackets) : 0)
						grandTotalVolsBegun.push(volsBegun ? Number(volsBegun) : 0)
						grandTotalVolsScheduled.push(volsScheduled ? Number(volsScheduled) : 0)
						grandTotalCallers.push(totalCallers ? Number(totalCallers) : 0)
						grandTotalWalkAttempts.push(walkAttempts ? Number(walkAttempts) : 0)
						grandTotalWalkContacts.push(walkContacts ? Number(walkContacts) : 0)

						// console.log()
						this.setState({
							grandTotalPacketsIn,
							grandTotalPacketsOut,
							grandTotalPackets,
							grandTotalVolsBegun,
							grandTotalVolsScheduled,
							grandTotalCallers,
							grandTotalWalkAttempts,
							grandTotalWalkContacts,
							[stationName.toLowerCase()]: {
								callers: totalCallers,
								totalPackets: totalPackets,
								lastUpdate: lastUpdate,
								packetsIn: packetsIn,
								packetsOut: packetsOut,
								totalPerc: stationPerc,
								walkAttempts: walkAttempts,
								walkContacts: walkContacts,
								volsScheduled: volsScheduled,
								volsBegun: volsBegun,
								stationStatus: stationStatus
							}
						})

					} else {
						// console.log(`No data in ${stationName} station`)
					}
				})

				// NO DATA...YET
			} else {
				// console.log(`No data in ${county} district`)
			}
		})

		// END SETUP
	}



	render() {

		return (
			<div className={classes.container}>

				<Typography variant="headline" component="h3" style={{ textAlign: 'center' }}>
					Totals
			</Typography>

				<br />
				<Paper elevation={1} className={classes.totalPaper} style={{ overflow: 'auto' }}>
					<table>
						<tbody>
							{locations.map((item, idx) => {
								const grandTotalPacketsIn = compoundArray(this.state.grandTotalPacketsIn)
								const grandTotalPacketsOut = compoundArray(this.state.grandTotalPacketsOut)
								const grandTotalPackets = compoundArray(this.state.grandTotalPackets)
								const grandTotalVolsBegun = compoundArray(this.state.grandTotalVolsBegun)
								const grandTotalVolsScheduled = compoundArray(this.state.grandTotalVolsScheduled)
								const grandTotalCallers = compoundArray(this.state.grandTotalCallers)
								const grandTotalWalkAttempts = compoundArray(this.state.grandTotalWalkAttempts)
								const grandTotalWalkContacts = compoundArray(this.state.grandTotalWalkContacts)

								let grandTotalPackPerc = (grandTotalPacketsIn / grandTotalPackets) * 100
								grandTotalPackPerc = numToPerc(grandTotalPackPerc, grandTotalPacketsIn)

								let grandTotalWalkHitsPerc = (grandTotalWalkContacts / grandTotalWalkAttempts) * 100
								grandTotalWalkHitsPerc = numToPerc(grandTotalWalkHitsPerc, grandTotalWalkContacts)


								let grandTotalVolHits = ((grandTotalVolsScheduled - grandTotalVolsBegun) / grandTotalVolsScheduled) * 100
								grandTotalVolHits = numToPerc(grandTotalVolHits, grandTotalVolsScheduled - grandTotalVolsBegun)
								grandTotalVolHits = parseInt(grandTotalVolHits) //* -1

								let inRed
								let inGreen
								if (grandTotalVolHits > 0) {
									// grandTotalVolHits = '+' + grandTotalVolHits
									inRed = true
								} else if (grandTotalVolHits < 0) {
									inGreen = true

								}
								return (
									<tr key={idx}>
										<td className={classes.tableTD} />
										<td>
											<table cellSpacing="0">
												<tbody>
													{idx === 0 ? (
														<tr>
															<th className={classes.totalsTH}>TOTALS</th>
															<th>{grandTotalPackPerc}</th>
															<th>{grandTotalPacketsIn}</th>
															<th>{grandTotalPacketsOut}</th>
															<th>{grandTotalPackets}</th>
															<th>{grandTotalVolsBegun}</th>
															<th>{grandTotalVolsScheduled}</th>
															<th>{grandTotalCallers}</th>
															<th>{grandTotalWalkAttempts}</th>
															<th>{grandTotalWalkContacts}</th>
															<th>{grandTotalWalkHitsPerc}</th>
															<th>{''}</th>
														</tr>
													) : null}
													{idx === 0 || idx === 4 ? (
														<tr className={classes.shownText}>
															<th />
															<th>% Daily Packets</th>
															<th>Packets In</th>
															<th>Packets Out</th>
															<th>#Packets</th>
															<th>#Vols Begun</th>
															<th>#Vols Scheduled</th>
															<th>#People Calling</th>
															<th>Walk Attempts</th>
															<th>Walk Contacts</th>
															<th>Walk Hits%</th>
															<th>Last Updated</th>
														</tr>
													) : null}
													<tr>
														<th className={classes.tableCountyName}>{item}</th>
													</tr>
													{stations[item].map((stationNameCap, idx2) => {
														const stationName = stationNameCap.toLowerCase()


														let lastUpdate = this.state[stationName] && this.state[stationName].lastUpdate ? this.state[stationName].lastUpdate : '-'

														const callers = this.state[stationName] && this.state[stationName].callers ? this.state[stationName].callers : 0
														let packetsIn = this.state[stationName] && this.state[stationName].packetsIn ? this.state[stationName].packetsIn : 0
														const packetsOut = this.state[stationName] && this.state[stationName].packetsOut ? this.state[stationName].packetsOut : 0
														const totalPackets = this.state[stationName] && this.state[stationName].totalPackets ? this.state[stationName].totalPackets : 0

														const totalPerc = this.state[stationName] && this.state[stationName].totalPerc ? this.state[stationName].totalPerc : 0
														const stationStatus = this.state[stationName] && this.state[stationName].stationStatus ? this.state[stationName].stationStatus : false
														const volsScheduled = this.state[stationName] && this.state[stationName].volsScheduled ? this.state[stationName].volsScheduled : 0
														const volsBegun = this.state[stationName] && this.state[stationName].volsBegun ? this.state[stationName].volsBegun : 0
														let walkAttempts = this.state[stationName] && this.state[stationName].walkAttempts ? this.state[stationName].walkAttempts : 0
														let walkContacts = this.state[stationName] && this.state[stationName].walkContacts ? this.state[stationName].walkContacts : 0
														let walkHits = walkContacts / walkAttempts * 100
														walkHits = numToPerc(walkHits, walkContacts)

														// FLAKE RATE
														let flakeRate = (Number(volsScheduled) - Number(volsBegun)) / Number(volsScheduled) * 100
														flakeRate = numToPerc(flakeRate, (Number(volsScheduled) - Number(volsBegun)))
														flakeRate = parseInt(flakeRate)// * -1

														let inRed
														let inGreen
														// console.log(flakeRate)

														/* isNaN(flakeRate) ? flakeRate = 0 : null
														flakeRate === Infinity || flakeRate === -Infinity ? flakeRate = volsBegun / -1 * 100 : null
														// flakeRate = parseInt(Math.abs(flakeRate))
														flakeRate = parseInt(flakeRate) * -1 */

														if (flakeRate > 0) {
															// flakeRate = '+' + flakeRate
															inRed = true
														} else if (flakeRate < 0) {

															inGreen = true
														}

														// LAST UPDATE FORMAT
														if (lastUpdate !== '-') {
															lastUpdate = secToDate(lastUpdate.seconds)
														}


														return (
															<tr key={`data-${idx2}`} className={classes.tableRowData}>
																<td className={classes.subTableStationName}>
																	{stationName}<div className={stationStatus ? classes.stationStatusOpen : classes.stationStatusClosed}></div>
																</td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{totalPerc}%</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{packetsIn}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{packetsOut}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{totalPackets}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{volsBegun}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{volsScheduled}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{callers}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{walkAttempts}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{walkContacts}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{walkHits}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCellDate}>{lastUpdate}</div></td>


															</tr>
														)
													})}
												</tbody>
											</table>
										</td>
									</tr>
								)
							})}
						</tbody>
					</table>
					<br />
				</Paper>
				<br />
				<br />
			</div>
		)
	}
}

export default TableTotals

/* 						
	<th>{idx === 0 || idx === 4 ? <span className={classes.shownText}>#Vols Scheduled</span> : <span className={classes.blankText}>#Vols Scheduled</span>}</th>
	<th>{idx === 0 || idx === 4 ? <span className={classes.shownText}>% Flake Rate</span> : <span className={classes.blankText}>% Flake Rate</span>}</th>


	<td className={classes.subTableData}><div className={classes.dataCell}>{volsScheduled}</div></td>
	<td className={classes.subTableData}>
		<div className={classes.dataCell}>
			<div className={inRed ? classes.inRed : inGreen ? classes.inGreen : classes.isZero}>{flakeRate}%</div>
		</div>
	</td>
*/
