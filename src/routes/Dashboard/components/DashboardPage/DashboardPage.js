import React, { Component } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'

import classes from './DashboardPage.scss'

import GraphPackets from './Graphs/GraphPackets'
import GraphWalk from './Graphs/GraphWalk'
import GraphVols from './Graphs/GraphVols'

// import StationsVsDays from './Graphs/StationsVsDays'
// import TotalsVsDays from './Graphs/TotalsVsDays'
// import DailyTotals from './Graphs/DailyTotals'

import TableTotals from './Tables/TableTotals'
import TableDaily from './Tables/TableDaily'
import TableShifts from './Tables/TableShifts'

import SwipeableViews from 'react-swipeable-views'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

import { vanEvents } from '../../../../constants/events'

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir}>
      {children}
    </Typography>
  )
}

class DashboardPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: 0,
      viewDashboard: props.accessDash,
      vanEvents: '',
      // token: '',
      dbData: '',
      cancelCB: ''
    }
    // console.log(props)
    // this.queryAPI()
  }

  componentDidUpdate(prevProps) {
    if (this.props.accessDash !== prevProps.accessDash) {
      // console.log('dashboard will update...')

      this.setState(
        {
          viewDashboard: this.props.accessDash
        },
        () => {
          // this.queryVanAPI()
          // this.props.accessDash ? this.state.dbData === '' ? this.getData() : null : null
          this.getData()
        }
      )
    }
  }

  componentWillMount() {
    // console.log('dashboard will mount...')
    // this.props.accessDash ? this.queryVanAPI() : null
    this.props.accessDash ? this.getData() : null
  }

  componentWillUnmount = () => {
    // this.unsubAutoData(this.state)
    // console.log('unsubscribing from callback...')

    const cancelAutoData = this.state.cancelCB

    // console.log(cancelAutoData)

    if (typeof cancelAutoData === 'function') {
      cancelAutoData()
    }
  }

  /* 	unsubAutoData = curState => {
			// const { cancelCB } = { ...curState }
			// console.log('unsubscribing from callback...')
			// console.log(cancelCB)
			const cancelAutoData = this.state.cancelCB
			if (typeof cancelAutoData === "function") {
				cancelAutoData()
			}
		} */

  /* 	autoData = () => {
			// console.log('setting up autoData retrieval...')
			let cancelCB = this.props.autoUpdateAllData(this.recCallBack)
			this.setState({
				cancelCB: cancelCB
			})
		} */

  recCallBack = res => {
    //	console.log(res)
    if (res !== undefined) {
      // const DB = res
      // console.log(res)

      this.setState(
        {
          dbData: res
          // Oneida: DB.oneida ? DB.oneida : '',
          // Herkimer: DB.herkimer ? DB.herkimer : '',
          // Oswego: DB.oswego ? DB.oswego : '',
          // Madison: DB.madison ? DB.madison : '',
          // Chenango: DB.chenango ? DB.chenango : '',
          // Cortland: DB.cortland ? DB.cortland : '',
          // Broome: DB.broome ? DB.broome : '',
          // Tioga: DB.tioga ? DB.tioga : ''
        },
        () => {
          // this.setupData()
          // this.autoData()
        }
      )
    }
  }

  getData = () => {
    const allShiftData = this.props.getAllShiftData()
    allShiftData.then(res => {
      if (res !== undefined) {
        // const DB = res
        // console.log(res)

        const cancelCB = this.props.autoUpdateAllData(this.recCallBack)
        // this.setState({
        // 	cancelCB: cancelCB
        // })

        this.setState(
          {
            //	signups: resp.data.items,
            dbData: res,
            cancelCB: cancelCB
            // Oneida: DB.oneida ? DB.oneida : '',
            // Herkimer: DB.herkimer ? DB.herkimer : '',
            // Oswego: DB.oswego ? DB.oswego : '',
            // Madison: DB.madison ? DB.madison : '',
            // Chenango: DB.chenango ? DB.chenango : '',
            // Cortland: DB.cortland ? DB.cortland : '',
            // Broome: DB.broome ? DB.broome : '',
            // Tioga: DB.tioga ? DB.tioga : ''
          },
          () => {
            // this.autoData()
            // this.getSignups()

            /*
						OCT 20 - 148489
						OCT 21 - 148490
						OCT 27 - 148558
						OCT 28 - 148560
					*/

            const getEvent = (eventId, tok) => {
              return axios({
                method: 'get',
                headers: {
                  'X-Requested-With': 'XMLHttpRequest',
                  Authorization: 'Bearer' + ' ' + tok
                },
                url: `https://us-central1-packet-land.cloudfunctions.net/apiVan/signups?q=${eventId}`
              })
            }

            const token = this.props.firebase.auth().currentUser.getIdToken()

            token.then(tok => {
              axios
                .all([
                  getEvent(vanEvents[0], tok), // DAY1
                  getEvent(vanEvents[1], tok), // DAY2
                  getEvent(vanEvents[2], tok), // DAY3
                  getEvent(vanEvents[3], tok) // DAY4
                ])
                .then(
                  axios.spread((evt1, evt2, evt3, evt4) => {
                    const day1Events = evt1.data.items
                    const day2Events = evt2.data.items
                    const day3Events = evt3.data.items
                    const day4Events = evt4.data.items

                    const allEvents = day1Events.concat(
                      day2Events,
                      day3Events,
                      day4Events
                    )

                    this.setState({
                      allEvents: allEvents,
                      dayEvents: [
                        day1Events,
                        day2Events,
                        day3Events,
                        day4Events
                      ]
                    })
                  })
                )

              // END THEN TOKEN
            })

            // END SET STATE
          }
        )
      }
    })
  }

  /* // QUERY
	queryVanAPI = () => {

		const token = this.props.firebase.auth().currentUser.getIdToken()

		token.then(tok => {

			// console.log(tok)
			// FORMAT yyyy-MM-dd
			axios({
				method: 'get',
				headers: {
					'X-Requested-With': 'XMLHttpRequest',
					'Authorization': 'Bearer' + ' ' + tok
				},
				url: `https://us-central1-packet-land.cloudfunctions.net/apiVan/events?before=2018-11-07&after=2018-11-02`
			}).then(resp => {
				// console.info(resp)

				const eventIds = resp.data.items.map(item => {
					let dayName = item.name.replace("GOTV", "").replace(" ", "").replace(" ", "").toLowerCase()
					dayName === 'e-day' ? dayName = 'day4' : null
					return { [dayName]: item.eventId }
				})

				// console.log(eventIds)

				this.setState({
					vanEvents: eventIds
					// token: tok
				}, () => {
					// console.log(this.state)
				})

			})
		}).catch(err => {
			console.log(err)
		})
	} */

  handleChange = (event, value) => {
    this.setState({ value })
  }

  handleChangeIndex = index => {
    this.setState({ value: index })
  }

  render() {
    return this.state.viewDashboard ? (
      <div>
        <AppBar position="static" color="default">
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            fullWidth>
            <Tab label="Daily" />
            <Tab label="Shifts" />
            <Tab label="Totals" />
            <Tab label="Graphs" />
          </Tabs>
        </AppBar>
        <SwipeableViews
          disabled
          className={classes.swipeViews}
          axis={'x'}
          index={this.state.value}
          onChangeIndex={this.handleChangeIndex}>
          <TabContainer dir={'ltr'}>
            <TableDaily
              dbData={this.state.dbData}
              dayEvents={this.state.dayEvents}
            />
          </TabContainer>

          <TabContainer dir={'ltr'}>
            <TableShifts
              dbData={this.state.dbData}
              dayEvents={this.state.dayEvents}
            />
          </TabContainer>

          <TabContainer dir={'ltr'}>
            <TableTotals
              dbData={this.state.dbData}
              allEvents={this.state.allEvents}
            />
          </TabContainer>

          <TabContainer dir={'ltr'}>
            <GraphPackets dbData={this.state.dbData} />
            <GraphWalk dbData={this.state.dbData} />
            <GraphVols
              dbData={this.state.dbData}
              dayEvents={this.state.dayEvents}
            />
            <br />
            <br />
            <br />
          </TabContainer>
        </SwipeableViews>
      </div>
    ) : (
      <div>Checking Credentials...</div>
    )
  }
}

DashboardPage.propTypes = {
  autoUpdateData: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  getShiftData: PropTypes.func.isRequired // from enhancer (withHandlers - firebase)
}

export default DashboardPage

/*
<StationsVsDays
	getShiftData={this.props.getShiftData}
	autoUpdateData={this.props.autoUpdateData}
/>

		<StationsVsDays
							getShiftData={this.props.getShiftData}
							autoUpdateData={this.props.autoUpdateData}
						/>

*/
