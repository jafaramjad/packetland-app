import moment from 'moment'

/**
 * NUMBER TO PERC
 * @param {Int} num the number being crunched
 * @param {Int} divisor the fallback number used to find a percantage
 */
export const numToPerc = (num, divisor) => {
  num = isNaN(num) ? 0 : num
  num = num === Infinity || num === -Infinity ? (divisor / 1) * 100 : num
  num = String(parseInt(num) + '%')

  return num
}

/**
 * SECONDS TO DATE
 * @param {Int} sec unix timestamp in seconds
 */
export const secToDate = sec => {
  let m = moment(sec * 1000)
    .utc()
    .local()
  let s = m.format('MMM Do h:mma')
  return s
}
