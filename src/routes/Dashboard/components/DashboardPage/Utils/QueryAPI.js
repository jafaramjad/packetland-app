import axios from 'axios'

// QUERY
export const queryVanSignupsAPI = (eventId, firebase) => {
  // const token = this.props.firebase.auth().currentUser.getIdToken()
  // const token = this.props.token

  const token = firebase.auth().currentUser.getIdToken()

  let query

  token.then(tok => {
    query = axios({
      method: 'get',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        Authorization: 'Bearer' + ' ' + tok
      },
      // url: `http://localhost:3000/van/events?q=`
      url: `https://us-central1-packet-land.cloudfunctions.net/apiVan/signups?q=${eventId}`
    })

    // return query

    /* .then(resp => {
				console.info(resp.data.items)



				// this.setState({
				// 	signupsAll: resp.data.items,
				// 	signups: signups,
				// 	volShiftSlots: volShiftSlots
				// })
			})
		}).catch(err => {
			console.log(err)
		}) */
  })
}
