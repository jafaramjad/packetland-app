// import { TOTAL_PACKETS_MAP } from '../../../../../constants/packets'
import { TOTAL_PACKETS } from '../../../../../constants/packets'

export const calcDayNum = dayName => {
  let dayNum
  switch (dayName) {
    case 'day1':
      dayNum = 0
      break
    case 'day2':
      dayNum = 1
      break
    case 'day3':
      dayNum = 2
      break
    case 'day4':
      dayNum = 3
      break

    default:
      break
  }

  return dayNum
}

export const calcShiftNameEDay = shiftNum => {
  let shiftName
  switch (Number(shiftNum)) {
    case 0:
      shiftName = '7AM'
      break
    case 1:
      shiftName = '9AM'
      break
    case 2:
      shiftName = '12PM'
      break
    case 3:
      shiftName = '3PM'
      break
    case 4:
      shiftName = '6PM'
      break
    case 5:
      shiftName = '9PM'
      break
    default:
      break
  }

  return shiftName
}

export const calcShiftName = shiftNum => {
  let shiftName
  switch (Number(shiftNum)) {
    case 0:
      shiftName = '9AM'
      break
    case 1:
      shiftName = '12PM'
      break
    case 2:
      shiftName = '3PM'
      break
    case 3:
      shiftName = '6PM'
      break
    case 4:
      shiftName = '9PM'
      break
    default:
      break
  }

  return shiftName
}

export const calcVolDays = (listItems, countyName, stationName, stationMap) => {
  const list = listItems // resp.data.items
  const county = countyName.toUpperCase() // this.state.county.toUpperCase()
  const station = stationName
    .replace(' ', '_')
    .replace('(', '')
    .replace(')', '')
    .toUpperCase() // this.state.station.replace(" ", "_").replace("(", "").replace(")", "").toUpperCase()
  const stagingLocationName = stationMap[county][station]

  /* if (dayNum === 2) {
		console.log(station)
	} */

  let signups = []
  list.map(item => {
    // console.log(item.status.name)
    if (
      item.status.name === 'Scheduled' ||
      item.status.name === 'Confirmed' ||
      item.status.name === 'Completed'
    ) {
      // EXCEPTION FOR DRY RUN
      // if (station !== 'HAMILTON_PHONES') {
      if (item.role.name === 'Canvasser' || item.role.name === 'Phonebanker') {
        item.location.name === stagingLocationName ? signups.push(item) : null
      }
      // }
    }

    // ? item.status.name === 'Scheduled' || item.status.name === 'Confirmed'
  })

  return signups
}

export const calcVolShifts = (
  listItems,
  countyName,
  stationName,
  stationMap
) => {
  // console.log(listItems)
  // console.log(countyName)
  // console.log(stationName)

  // console.log(STATION_MAP)
  const list = listItems // resp.data.items
  const county = countyName.toUpperCase() // this.state.county.toUpperCase()
  const station = stationName
    .replace(' ', '_')
    .replace('(', '')
    .replace(')', '')
    .toUpperCase() // this.state.station.replace(" ", "_").replace("(", "").replace(")", "").toUpperCase()
  const stagingLocationName = stationMap[county][station]

  // console.log(stagingLocationName)

  let signups = []
  list.map(item => {
    if (
      item.status.name === 'Scheduled' ||
      item.status.name === 'Confirmed' ||
      item.status.name === 'Completed'
    ) {
      // EXCEPTION FOR DRY RUN
      // if (station !== 'HAMILTON_PHONES') {
      // console.log(item)
      if (item.role.name === 'Canvasser' || item.role.name === 'Phonebanker') {
        if (item.location.name === stagingLocationName) {
          signups.push(item)
        }
      }

      // }
    }
  })
  // console.log(signups)

  // SORT VOL SHIFTS
  let volShiftSlots = {
    '7AM': [],
    '9AM': [],
    '12PM': [],
    '3PM': [],
    '6PM': [],
    '9PM': []
  }

  signups.map(item => {
    const timeStart = item.startTimeOverride
    // console.log(timeStart)
    // yyyy-MM-ddTHH:mm:ssZ
    let timeFormat = new Date(timeStart)
    const formatOptions = {
      timeZone: 'America/New_York',
      hour: '2-digit',
      minute: '2-digit',
      hour12: true
    }
    // const timeFormat = moment(timeStart, "yyyy-MM-dd[T]HH:mm:ssZ").format('HH:mm a')
    // console.log(timeFormat)
    let finalTime = timeFormat
      .toLocaleTimeString('en-US', formatOptions)
      .replace(':00 ', '')

    // console.log(finalTime)
    switch (finalTime) {
      case '6AM':
        volShiftSlots['7AM'].push(item)
        break
      case '9AM':
        volShiftSlots['9AM'].push(item)
        break
      case '12PM':
        volShiftSlots['12PM'].push(item)
        break
      case '3PM':
        volShiftSlots['3PM'].push(item)
        break
      case '6PM':
        volShiftSlots['6PM'].push(item)
        break
      default:
        break
    }
    // console.log(volShiftSlots)
  })

  return volShiftSlots

  // END FUNC
}

/**
 * CALC TOTAL PERC BY DAY
 * @param {Object} allResp
 *
 */
export const calcTotalPercByDay = (allResp, dayName) => {
  // const curDay = this.state.day

  const allDays = allResp
  const totalPacks =
    (allResp['day4'] &&
      allResp['day4'].info &&
      allResp['day4'].info.totalPackets) ||
    (allResp['day3'] &&
      allResp['day3'].info &&
      allResp['day3'].info.totalPackets) ||
    (allResp['day2'] &&
      allResp['day2'].info &&
      allResp['day2'].info.totalPackets) ||
    (allResp['day1'] &&
      allResp['day1'].info &&
      allResp['day1'].info.totalPackets)

  // console.log(allResp)
  // console.log(curDay)
  // console.log(allDays)
  // console.log(totalPacks)

  let vals1 = allDays['day1']
    ? Object.values(allDays['day1'].packetsIn).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]
  let vals2 = allDays['day2']
    ? Object.values(allDays['day2'].packetsIn).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]
  let vals3 = allDays['day3']
    ? Object.values(allDays['day3'].packetsIn).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]
  let vals4 = allDays['day4']
    ? Object.values(allDays['day4'].packetsIn).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]

  // let vals1 = allDays['day1'] ? Object.values(allDays['day1'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]
  // let vals2 = allDays['day2'] ? Object.values(allDays['day2'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]
  // let vals3 = allDays['day3'] ? Object.values(allDays['day3'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]
  // let vals4 = allDays['day4'] ? Object.values(allDays['day4'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]

  // console.log(vals1)
  // console.log(vals2)
  // console.log(vals3)
  // console.log(vals4)

  const totalValsArr = vals1.concat(vals2, vals3, vals4)
  const totalCompArr = []

  const compound = (accval, curval, idx) => {
    // console.log("acc: " + accval)
    // console.log("cur: " + curval)
    // console.log("idx: " + idx)
    const comp = accval + curval

    if (idx === 1) {
      totalCompArr.push(accval)
      totalCompArr.push(comp)
    } else {
      totalCompArr.push(comp)
    }

    return comp
  }

  const TOTAL_ADDED = totalValsArr.reduce(compound)

  // console.log(totalValsArr)
  // console.log(totalCompArr)
  // console.log(TOTAL_ADDED)

  const ALL_PERC = totalCompArr.map(val => {
    return Number.parseInt((val / totalPacks) * 100)
  })

  // console.log(ALL_PERC)
  // console.log(ALL_PERC.slice(0, 5))
  // console.log(ALL_PERC.slice(5, 10))
  // console.log(ALL_PERC.slice(10, 15))
  // console.log(ALL_PERC.slice(15, 21))

  return ALL_PERC
}

/**
 *  CALC TOTAL PERC
 * @param {Object} allResp
 */
export const calcTotalPerc = (allResp, county, station, day) => {
  const curDay = day // this.state.day

  const allDays = allResp
  // const totalPacks = allResp['day1'] && allResp['day1'].info && allResp['day1'].info.totalPackets
  const totalPacks = TOTAL_PACKETS[county][station]
  // console.log(allResp)
  // console.log(curDay)
  // console.log(allDays)
  // console.log(totalPacks)

  let vals1 =
    allDays && allDays['day1']
      ? Object.values(allDays['day1'].packetsIn).map(v =>
          v ? parseFloat(v, 10) : 0
        )
      : [0, 0, 0, 0, 0]
  let vals2 =
    allDays && allDays['day2']
      ? Object.values(allDays['day2'].packetsIn).map(v =>
          v ? parseFloat(v, 10) : 0
        )
      : [0, 0, 0, 0, 0]
  let vals3 =
    allDays && allDays['day3']
      ? Object.values(allDays['day3'].packetsIn).map(v =>
          v ? parseFloat(v, 10) : 0
        )
      : [0, 0, 0, 0, 0]
  let vals4 =
    allDays && allDays['day4']
      ? Object.values(allDays['day4'].packetsIn).map(v =>
          v ? parseFloat(v, 10) : 0
        )
      : [0, 0, 0, 0, 0]

  // let vals1 = allDays['day1'] ? Object.values(allDays['day1'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]
  // let vals2 = allDays['day2'] ? Object.values(allDays['day2'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]
  // let vals3 = allDays['day3'] ? Object.values(allDays['day3'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]
  // let vals4 = allDays['day4'] ? Object.values(allDays['day4'].packetsIn).map(v => v ? parseFloat(v, 10) : 0) : [0, 0, 0, 0, 0]

  // console.log(vals1)
  // console.log(vals2)
  // console.log(vals3)
  // console.log(vals4)

  const totalValsArr = vals1.concat(vals2, vals3, vals4)
  const totalCompArr = []

  const compound = (accval, curval, idx) => {
    // console.log("acc: " + accval)
    // console.log("cur: " + curval)
    // console.log("idx: " + idx)
    const comp = accval + curval

    if (idx === 1) {
      totalCompArr.push(accval)
      totalCompArr.push(comp)
    } else {
      totalCompArr.push(comp)
    }

    return comp
  }

  const TOTAL_ADDED = totalValsArr.reduce(compound)

  // console.log(totalValsArr)
  // console.log(totalCompArr)
  // console.log(TOTAL_ADDED)

  const ALL_PERC = totalCompArr.map(val => {
    return Number.parseInt((val / totalPacks) * 100)
  })

  // console.log(ALL_PERC)
  // console.log(ALL_PERC.slice(0, 5))
  // console.log(ALL_PERC.slice(5, 10))
  // console.log(ALL_PERC.slice(10, 15))
  // console.log(ALL_PERC.slice(15, 21))

  let linePlotVal = []

  switch (curDay) {
    case 'day1':
      linePlotVal = ALL_PERC.slice(0, 5)
      break
    case 'day2':
      linePlotVal = ALL_PERC.slice(5, 10)
      break
    case 'day3':
      linePlotVal = ALL_PERC.slice(10, 15)
      break
    case 'day4':
      linePlotVal = ALL_PERC.slice(15, 21)
      break
    default:
      linePlotVal = [0, 0, 0, 0, 0, 0]
  }

  return linePlotVal

  // return ALL_PERC
}

export const calcTotalPacks = (dayName, countyName, stationName) => {
  // console.log(countyName)
  // console.log(stationName)
  const dayNum = calcDayNum(dayName)
  // const county = countyName
  // const station = stationName
  const county = countyName.toLowerCase()
  const station = stationName.toLowerCase().replace(' ', '_') // .replace("(", "").replace(")", "")
  const packArray = TOTAL_PACKETS[county][station]
  return packArray
  // return packArray[dayNum]
}

export const calcAllPacks = (countyName, stationName) => {
  // const dayNum = calcDayNum(dayName)
  // const county = countyName
  // const station = stationName
  const county = countyName.toLowerCase()
  const station = stationName.toLowerCase().replace(' ', '_') // .replace("(", "").replace(")", "")
  const packArray = TOTAL_PACKETS[county][station]
  return packArray
  // return compoundArray(packArray)
}

/* export const calcTotalPacks = (dayName, countyName, stationName) => {
	const dayNum = calcDayNum(dayName)
	const county = countyName.toUpperCase()
	const station = stationName.toUpperCase().replace(" ", "_").replace("(", "").replace(")", "")
	const packArray = TOTAL_PACKETS_MAP[county][station]
	return packArray[dayNum]

}

export const calcAllPacks = (countyName, stationName) => {
	// const dayNum = calcDayNum(dayName)
	const county = countyName.toUpperCase()
	const station = stationName.toUpperCase().replace(" ", "_").replace("(", "").replace(")", "")
	const packArray = TOTAL_PACKETS_MAP[county][station]
	return compoundArray(packArray)

}
 */

/**
 * CALC TOTAL PERC BY DAY
 * @param {Object} allResp
 *
 */
export const calcTotalPercByDayStation = (
  allResp,
  dayName,
  countyName,
  stationName
) => {
  const allDays = allResp
  // const dayNum = calcDayNum(dayName)
  // const county = countyName.toUpperCase()
  // const station = stationName.toUpperCase().replace(" ", "_").replace("(", "").replace(")", "")
  // const packArray = TOTAL_PACKETS_MAP[county][station]

  const totalPacks = calcTotalPacks(dayName, countyName, stationName)

  let vals1 = allDays[dayName]
    ? Object.values(allDays[dayName].packetsIn).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]

  // console.log(dayName)
  // console.log(countyName)
  // console.log(stationName)
  // console.log(packArray)
  // console.log(dayNum)
  // console.log(totalPacks)
  // console.log(vals1)

  const totalValsArr = vals1 // .concat(vals2, vals3, vals4)
  const totalCompArr = []

  const compound = (accval, curval, idx) => {
    // console.log("acc: " + accval)
    // console.log("cur: " + curval)
    // console.log("idx: " + idx)
    const comp = accval + curval

    if (idx === 1) {
      totalCompArr.push(accval)
      totalCompArr.push(comp)
    } else {
      totalCompArr.push(comp)
    }

    return comp
  }

  const TOTAL_ADDED = totalValsArr.reduce(compound)

  // console.log(totalValsArr)
  // console.log(totalCompArr)
  // console.log(TOTAL_ADDED)

  const ALL_PERC = totalCompArr.map(val => {
    return Number.parseInt((val / totalPacks) * 100)
  })

  return ALL_PERC
}

/**
 * COMPOUND ARRAY
 * @param {Array} arr
 */
export const compoundArray = arr => {
  const totalValsArr = arr // .concat(vals2, vals3, vals4)
  const totalCompArr = []

  const compound = (accval, curval, idx) => {
    // console.log("acc: " + accval)
    // console.log("cur: " + curval)
    // console.log("idx: " + idx)
    const comp = accval + curval

    if (idx === 1) {
      totalCompArr.push(accval)
      totalCompArr.push(comp)
    } else {
      totalCompArr.push(comp)
    }

    return comp
  }

  const TOTAL_ADDED = totalValsArr.reduce(compound)
  return TOTAL_ADDED
}

/**
 * CALC GRAND TOTAL
 * @param {} allResp
 * @param {*} dayName
 * @param {*} countyName
 * @param {*} stationName
 */
export const calcGrandTotalPerc = (
  allResp,
  dayName,
  countyName,
  stationName
) => {
  const allDays = allResp
  const totalPacks = calcAllPacks(countyName, stationName)

  let vals1 = allDays['day1']
    ? Object.values(allDays['day1'].packetsIn).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]
  let vals2 = allDays['day2']
    ? Object.values(allDays['day2'].packetsIn).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]
  let vals3 = allDays['day3']
    ? Object.values(allDays['day3'].packetsIn).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]
  let vals4 = allDays['day4']
    ? Object.values(allDays['day4'].packetsIn).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]

  // console.log(dayName)
  // console.log(countyName)
  // console.log(stationName)
  // console.log(packArray)
  // console.log(dayNum)
  // console.log(totalPacks)
  // console.log(vals1)

  const totalValsArr = vals1.concat(vals2, vals3, vals4)
  const totalCompArr = []

  const compound = (accval, curval, idx) => {
    // console.log("acc: " + accval)
    // console.log("cur: " + curval)
    // console.log("idx: " + idx)
    const comp = accval + curval

    if (idx === 1) {
      totalCompArr.push(accval)
      totalCompArr.push(comp)
    } else {
      totalCompArr.push(comp)
    }

    return comp
  }

  const TOTAL_ADDED = totalValsArr.reduce(compound)

  // console.log(totalValsArr)
  // console.log(totalCompArr)
  // console.log(TOTAL_ADDED)

  const ALL_PERC = totalCompArr.map(val => {
    return Number.parseInt((val / totalPacks) * 100)
  })

  return ALL_PERC
}

/**
 * CALC TOTALS
 * @param {object} allResp
 * @param {string} objName
 */
export const calcTotals = (allResp, objName) => {
  // const curDay = this.state.day

  const allDays = allResp
  // const totalPacks = allResp['day1'] && allResp['day1'].info && allResp['day1'].info.totalPackets

  // console.log(allResp)
  // console.log(curDay)
  // console.log(allDays)
  // console.log(totalPacks)

  let vals1 = allDays['day1']
    ? Object.values(allDays['day1'][objName]).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]
  let vals2 = allDays['day2']
    ? Object.values(allDays['day2'][objName]).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]
  let vals3 = allDays['day3']
    ? Object.values(allDays['day3'][objName]).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]
  let vals4 = allDays['day4']
    ? Object.values(allDays['day4'][objName]).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]

  // console.log(vals1)
  // console.log(vals2)
  // console.log(vals3)
  // console.log(vals4)

  const totalValsArr = vals1.concat(vals2, vals3, vals4)
  const totalCompArr = []

  const compound = (accval, curval, idx) => {
    // console.log("acc: " + accval)
    // console.log("cur: " + curval)
    // console.log("idx: " + idx)
    const comp = accval + curval

    if (idx === 1) {
      totalCompArr.push(accval)
      totalCompArr.push(comp)
    } else {
      totalCompArr.push(comp)
    }

    return comp
  }

  const TOTAL_ADDED = totalValsArr.reduce(compound)

  return TOTAL_ADDED
  // return ALL_PERC
}

/**
 * CALC TOTALS
 * @param {object} allResp
 * @param {string} objName
 */
export const calcDaily = (allResp, objName, dayName) => {
  // const curDay = this.state.day

  const allDays = allResp
  // const totalPacks = allResp['day1'] && allResp['day1'].info && allResp['day1'].info.totalPackets

  // console.log(allResp)
  // console.log(curDay)
  // console.log(allDays)
  // console.log(totalPacks)

  let vals1 = allDays[dayName]
    ? Object.values(allDays[dayName][objName]).map(v =>
        v ? parseFloat(v, 10) : 0
      )
    : [0, 0, 0, 0, 0]

  const totalValsArr = vals1 // .concat(vals2, vals3, vals4)
  const totalCompArr = []

  const compound = (accval, curval, idx) => {
    // console.log("acc: " + accval)
    // console.log("cur: " + curval)
    // console.log("idx: " + idx)
    const comp = accval + curval

    if (idx === 1) {
      totalCompArr.push(accval)
      totalCompArr.push(comp)
    } else {
      totalCompArr.push(comp)
    }

    return comp
  }

  const TOTAL_ADDED = totalValsArr.reduce(compound)

  return TOTAL_ADDED
  // return ALL_PERC
}

export const findCurrentDay = obj => {
  let currentDay

  Object.entries(obj).map(item => {
    // 	console.log(item)
    switch (item[0]) {
      case 'day4':
        currentDay = 'day4'
        break
      case 'day3':
        currentDay = 'day3'
        break
      case 'day2':
        currentDay = 'day2'
        break
      case 'day1':
        currentDay = 'day1'
        break
      default:
        currentDay = undefined
    }
  })

  /* 	if (currentDay === undefined) {
			console.log(obj)
	
		} */
  // const events = this.props.events

  // console.log(currentDay)
  // const signups = this.queryVanSignupsAPI(events[currentDay])

  return currentDay
  // return 'nothing yet...'
}

export const findCurrentShift = obj => {
  let currentShift

  Object.entries(obj).map(item => {
    // console.log(item)
    if (item[0] === 'volsHubdialer') {
      Object.entries(item[1]).map((entry, idx) => {
        // console.log(entry)
        // console.log(typeof entry[1])
        if (entry[1]) {
          currentShift = idx
        }
      })
    }
  })
  // return 'nothing found yet...'
  return currentShift
}

/* 	setupData = () => {
			// console.log('setting up data...')
			locations.map(county => {
				const district = this.state[county]
				const dataLives = district ? true : false
	
				if (dataLives) {
					// console.log(`Data lives in: ${county}`)
					stations[county].map(stationName => {
						// console.log(stationName)
						const station = district[stationName.replace(' ', '_').toLowerCase()]
						// console.log(stationName.toLowerCase())
						const stationDataLives = station ? true : false
						// console.log(station)
						if (stationDataLives) {
							// console.log(`Data lives in: ${stationName}`)
							// console.log(station)
							let mostRecentDay = this.findCurrentDay(station)
							// console.log(mostRecentDay)
							// console.log(station[mostRecentDay])
							let mostRecentShift = this.findCurrentShift(station[mostRecentDay])
							// console.log(mostRecentShift)
	
							// TALLY CALLERS
							const callersHubdialer = Number(station[mostRecentDay]['volsHubdialer'][mostRecentShift])
							const callersPaper = Number(station[mostRecentDay]['volsPaper'][mostRecentShift])
							const totalCallers = callersHubdialer + callersPaper
	
							// SET TOTAL PACKETS
							const totalPackets = Number(station[mostRecentDay]['info']['totalPackets'])
	
							// SET TOTAL PACKETS
							const volsBegun = Number(station[mostRecentDay]['volsBegun'][mostRecentShift])
	
							// FIND TOTAL% vs PACKETS IN
							const stationPerc = this.calcTotalPerc(station).slice(-1)[0]
							// console.log(stationPerc)
	
							// SET STATION STATUS
							const stationStatus = station[mostRecentDay]['info']['stationOpen']
	
							// console.log()
							this.setState(
								{
									[stationName]: {
										callers: totalCallers,
										totalPackets: totalPackets,
										totalPerc: stationPerc,
										stationStatus: stationStatus,
										volsBegun: volsBegun
									}
								},
								() => {
									// console.log(this.state)
								}
							)
						} else {
							// console.log(`No data in ${stationName} station`)
						}
					})
	
					// NO DATA...YET
				} else {
					// console.log(`No data in ${county} district`)
				}
			})
		} */
