import React, { Component } from 'react'
import classes from './Graphs.scss'
import { Bar, Pie } from 'react-chartjs-2'
import Paper from '@material-ui/core/Paper'
import NativeSelect from '@material-ui/core/NativeSelect'
import randomColor from 'randomcolor'

import {
  compoundArray
} from '../Utils/CalcFunc' //'../Utils/CalcFunc'

import {
  shifts,
  graphOptionsVols,
  chartsDaily,
  bgBlue,
  bgGreen,
  bgOrange,
  bgRed,
  borderBlue,
  borderGreen,
  borderOrange,
  borderRed
} from '../../../../../constants/dashboard'

chartsDaily.title.text = ['LOCATION', 'Totals']

const dataTypes = {
  doorsKnocked: "Doors Knocked",
  callAttempts: "Call Attempts",
  digitalVols: "Digital Volunteers",
  shiftsCompleted: 'Shifts Completed',
  shiftsRecruited: 'Shifts Recruited',
  vbmForms: 'VBM Forms',
  votersID: "Voters ID'd",
  votersRegistered: 'Voters Registered'
}

class ChartsDaily extends Component {
  constructor(props) {
    super(props)

    this.state = {
      listData: props.list,
      selectPlace: '',
      selectType: 'doorsKnocked',
      selectTypeLabel: 'Doors Knocked'
    }
  }

  componentDidMount() {
    this.setupData()
  }

  processData = (objGroup, objName, dataList) => {

    let dataTmpObj = {}

    const dataObj = Object.values(dataList).map(item => {

      let itemData = Number(item[objGroup][objName])
      let valData = Number.isInteger(itemData) ? itemData : 0

      dataTmpObj = {
        ...dataTmpObj,
        // ...tmpObj
        [item.location]: dataTmpObj[item.location]
          ? dataTmpObj[item.location].concat(valData)
          : [valData]
      }

      return valData
    })

    return dataTmpObj

  }

  setupData = () => {

    let theData = this.processData('numbers', this.state.selectType, this.state.listData)

    let dataSets = Object.entries(theData).map((item, idx) => {

      let addDoors = compoundArray(item[1])

      return {
        label: item[0],
        backgroundColor: bgBlue,
        borderColor: borderBlue,
        borderWidth: 1,
        hoverBackgroundColor: bgOrange,
        hoverBorderColor: borderOrange,
        yAxisID: 'y-axis-1',
        data: [addDoors]
      }
    })


    let currentLabel = dataTypes[this.state.selectType]
    // let graphLabels = [currentLabel]


    let tmpData = []
    let tmpLabels = []
    let bgColor = []
    let bgColorHover = []

    dataSets.map((itm, idx) => {
      tmpData = [
        ...tmpData,
        itm.data[0]
      ]

      tmpLabels = [
        ...tmpLabels,
        itm.label
      ]

      let newBG = randomColor({
        count: 1,
        seed: itm.label,
        // hue: 'blue',
        luminosity: 'bright'
      })

      bgColor.push(newBG[0])
      // bgColorHover.push(newBG[1])

    })

    // SET DATA
    const data1 = {
      labels: tmpLabels,
      datasets: [
        {
          data: tmpData,
          backgroundColor: bgColor,
          hoverBackgroundColor: bgColor
        }
      ]
    }


    // EXAMPLE DATA
    const dataExample = {
      labels: [
        'Red',
        'Green',
        'Yellow'
      ],
      datasets: [{
        data: [12, 44, 55],
        backgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56'
        ],
        hoverBackgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56'
        ]
      }]
    }

    this.setState({
      data1: data1
    })
  }

  handleSelect = () => evt => {
    this.setState({
      selectPlace: evt.target.value
    })
  }

  handleTypeSelect = () => evt => {

    this.setState(
      {
        selectType: evt.target.value,
        selectTypeLabel: ''
      },
      () => {
        this.setupData()
      }
    )
  }

  render() {
    return (
      <div className={classes.container}>
        <br />

        <NativeSelect
          onChange={this.handleTypeSelect()}
          value={this.state.selectType}>
          <option value="disabledselect" disabled>
            Please Select Option
          </option>
          {Object.entries(dataTypes).map((itm, idx) => {
            return (
              <option value={itm[0]} key={`datatype-${idx}`}>
                {itm[1]}
              </option>
            )
          })}
        </NativeSelect>

        <br />
        <Paper elevation={1} className={classes.barGraphPaper}>
          {this.state.data1 ? (
            <Pie
              data={this.state.data1}
              options={chartsDaily}
            />
          ) : null}

        </Paper>
      </div>
    )
  }
}

export default ChartsDaily
