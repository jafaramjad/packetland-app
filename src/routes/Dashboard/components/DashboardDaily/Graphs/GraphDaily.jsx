import React, { Component } from 'react'
import classes from './Graphs.scss'
import { Bar } from 'react-chartjs-2'
import Paper from '@material-ui/core/Paper'
import NativeSelect from '@material-ui/core/NativeSelect'
import randomColor from 'randomcolor'


import {
  compoundArray
} from '../Utils/CalcFunc' //'../Utils/CalcFunc'

import {
  shifts,
  graphOptionsVols,
  graphDaily,
  bgBlue,
  bgGreen,
  bgOrange,
  bgRed,
  borderBlue,
  borderGreen,
  borderOrange,
  borderRed
} from '../../../../../constants/dashboard'

graphDaily.title.text = ['LOCATION', 'Totals']


const dataTypes = {
  doorsKnocked: "Doors Knocked",
  callAttempts: "Call Attempts",
  digitalVols: "Digital Volunteers",
  shiftsCompleted: 'Shifts Completed',
  shiftsRecruited: 'Shifts Recruited',
  vbmForms: 'VBM Forms',
  votersID: "Voters ID'd",
  votersRegistered: 'Voters Registered'

}

class GraphDaily extends Component {
  constructor(props) {
    super(props)

    this.state = {
      listData: props.list,
      selectPlace: '',
      selectType: 'doorsKnocked',
      selectTypeLabel: 'Doors Knocked',

    }
  }

  componentDidMount() {
    // console.log(this.props.list)
    this.setupData()
  }

  processData = (objGroup, objName, dataList) => {

    console.log(objGroup)
    console.log(objName)
    console.log(dataList)

    let dataTmpObj = {}

    const dataObj = Object.values(dataList).map(item => {




      let itemData = Number(item[objGroup][objName])

      let valData = Number.isInteger(itemData) ? itemData : 0

      dataTmpObj = {
        ...dataTmpObj,
        // ...tmpObj
        [item.location]: dataTmpObj[item.location] ? dataTmpObj[item.location].concat(valData) : [valData]
      }


      return valData
    })

    console.log(dataTmpObj)
    // console.log(dataObj)

    return dataTmpObj

  }

  setupData = () => {


    let theData = this.processData('numbers', this.state.selectType, this.state.listData)

    // console.log(dataDoors)
    // console.log(callAttempts)


    let dataSets = Object.entries(theData).map((item, idx) => {

      let addDoors = compoundArray(item[1])
      // console.log(addDoors)

      return {
        label: item[0],
        backgroundColor: bgBlue,
        borderColor: borderBlue,
        borderWidth: 1,
        hoverBackgroundColor: bgOrange,
        hoverBorderColor: borderOrange,
        yAxisID: 'y-axis-1',
        data: [addDoors]
        // data: [addDoors, addDoors]
      }
    })

    // let graphLabels = ['Door Knocks', 'Call Attempts']
    let currentLabel = dataTypes[this.state.selectType]
    let graphLabels = [currentLabel]

    let tmpData = []
    let tmpLabels = []
    let bgColor = []

    dataSets.map((itm, idx) => {
      tmpData = [
        ...tmpData,
        itm.data[0]
      ]

      tmpLabels = [
        ...tmpLabels,
        itm.label
      ]

      let newBG = randomColor({
        count: 1,
        seed: itm.label,
        // hue: 'blue',
        luminosity: 'bright'
      })

      bgColor.push(newBG[0])

    })

    // SET DATA
    const data1 = {
      labels: tmpLabels,
      datasets: [
        {
          label: dataTypes[this.state.selectType], // item[0],
          backgroundColor: bgBlue,
          borderColor: borderBlue,
          borderWidth: 1,
          hoverBackgroundColor: bgOrange,
          hoverBorderColor: borderOrange,
          yAxisID: 'y-axis-1',
          data: tmpData,
          backgroundColor: bgColor,
          hoverBackgroundColor: bgColor
        }
      ]
    }

    // const data1 = {
    //   labels: tmpLabels,
    //   datasets: dataSets
    // }

    this.setState({
      data1: data1
    })
  }


  handleSelect = () => evt => {
    // console.log(evt.target)
    // console.log(evt.target.value)

    this.setState(
      {
        selectPlace: evt.target.value

      },
      () => {
        // this.getData()
        // this.autoData()
      }
    )
  }

  handleTypeSelect = () => evt => {
    // console.log(evt)
    // console.log(evt.target.value)

    this.setState(
      {
        selectType: evt.target.value,
        selectTypeLabel: ''
      },
      () => {
        this.setupData()
      }
    )
  }

  render() {
    return (
      <div className={classes.container}>
        <br />

        <NativeSelect
          onChange={this.handleTypeSelect()}
          value={this.state.selectType}>
          <option value="disabledselect" disabled>
            Please Select Option
          </option>
          {Object.entries(dataTypes).map((itm, idx) => {
            return (
              <option value={itm[0]} key={`datatype-${idx}`}>
                {itm[1]}
              </option>
            )
          })}

          {/*   <option value="callAttempts">Call Attempts</option>
          <option value="doorsKnocked">Doors Knocked</option>
          <option value="digitalVols">Digital Vols</option> */}
          }
        </NativeSelect>

        {/* <NativeSelect
          onChange={this.handleSelect()}
          value={this.state.selectPlace}>
          <option value="disabledselect" disabled>
            Please Select Option
          </option>
          {Object.values(this.props.places).map((place, placeIDX) => {

            return (
              Object.entries(place).map(item => {
                // console.log(item)
                return (
                  <optgroup key={`optgroup-${placeIDX}`} label={item[0]}>
                    {item[1].map((name, idx2) => {
                      return (
                        <option key={`station-${idx2}`} value={[item[0], name]}>
                          {name}
                        </option>
                      )
                    })}
                  </optgroup>
                )
              })
            )
          })

          }
        </NativeSelect> */}
        <br />
        <Paper elevation={1} className={classes.barGraphPaper}>
          {this.state.data1 ? (
            <Bar
              data={this.state.data1}
              // width={100}
              // height={50}
              options={graphDaily}
            // plugins={plugins}
            />
          ) : null}

        </Paper>
      </div>
    )
  }
}

export default GraphDaily
