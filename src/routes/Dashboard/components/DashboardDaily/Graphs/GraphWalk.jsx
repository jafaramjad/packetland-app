import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classes from './Graphs.scss'
import SelectCountyDay from '../Select/SelectCountyDay'

import { Bar } from 'react-chartjs-2'
import Paper from '@material-ui/core/Paper'
import {
    calcVolShifts,
    calcDayNum,
    calcVolDays,
    calcShiftName,
    calcShiftNameEDay,
    calcTotalPerc,
    calcTotals,
    calcDaily,
    calcTotalPercByDay,
    calcTotalPercByDayStation,
    calcTotalPacks,
    compoundArray
} from '../Utils/CalcFunc'
import {
    shifts,
    graphOptionsWalk,
    bgBlue,
    bgGreen,
    bgOrange,
    bgRed,
    borderBlue,
    borderGreen,
    borderOrange,
    borderRed
} from '../../../../../constants/dashboard'

graphOptionsWalk.title.text = ['WALK', 'Attempts vs Contacts']

/* const plugins = [{
	afterDraw: (chartInstance, easing) => {
		const ctx = chartInstance.chart.ctx;
		ctx.fillText("This text drawn by a plugin", 100, 100);
	}
}]; */

class GraphWalk extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data1: '',
            packetsOut: '',
            packetsIn: '',
            linePlot: '',
            county: 'oneida',
            station: 'utica',
            day: 'day1',
            cancelCB: ''
        }
    }


    componentDidUpdate(prevProps) {
        if (this.props.dbData !== prevProps.dbData) {
            // console.log(this.props)
            const county = this.state.county
            const station = this.state.station
            const day = this.state.day
            const DB = this.props.dbData

            // const linePlot = calcTotalPerc(DB[county][station], county, station, day)
            const walkAttempts = DB[county] && DB[county][station] && DB[county][station][day] ? DB[county][station][day].walkAttempts : 0
            const walkContacts = DB[county] && DB[county][station] && DB[county][station][day] ? DB[county][station][day].walkContacts : 0

            this.setState({
                walkAttempts: walkAttempts,
                walkContacts: walkContacts,
                // linePlot: linePlot,
                dbData: this.props.dbData
            }, () => {
                this.setupData()
                // this.getData()
                // this.autoData()
            })

        }

		/* 	if (this.props.dayEvents !== prevProps.dayEvents) {
				this.setState({
					dayEvents: this.props.dayEvents
				}, () => {
					this.setupData()
				})
			} */
    }

    setupData = () => {
        const dataPackOut = Object.values(this.state.walkAttempts).map(item => {
            return item
        })

        const dataPackIn = Object.values(this.state.walkContacts).map(item => {
            return item
        })

        // console.log(dataPackIn)
        // console.log(dataPackOut)

        const data1 = {
            labels: shifts,
            datasets: [
                // {
                //     label: 'Total %',
                //     type: 'line',
                //     data: this.state.linePlot,
                //     fill: false,
                //     borderColor: '#EC932F',
                //     backgroundColor: '#EC932F',
                //     pointBorderColor: '#EC932F',
                //     pointBackgroundColor: '#EC932F',
                //     pointHoverBackgroundColor: '#EC932F',
                //     pointHoverBorderColor: '#EC932F',
                //     yAxisID: 'y-axis-2'
                // },
                {
                    label: 'ATTEMPTS',
                    backgroundColor: bgBlue,
                    borderColor: borderBlue,
                    borderWidth: 1,
                    hoverBackgroundColor: bgOrange,
                    hoverBorderColor: borderOrange,
                    yAxisID: 'y-axis-1',
                    data: dataPackOut
                },
                {
                    label: 'CONTACTS',
                    backgroundColor: bgGreen,
                    borderColor: borderGreen,
                    borderWidth: 1,
                    hoverBackgroundColor: bgRed,
                    hoverBorderColor: borderRed,
                    yAxisID: 'y-axis-1',
                    data: dataPackIn
                }
            ]
        }

        this.setState({
            data1: data1
        })
    }



    getData = () => {

        // const getQuery = {
        // 	county: this.state.county,
        // 	station: this.state.station,
        // 	day: this.state.day
        // }

        const county = this.state.county
        const station = this.state.station
        const day = this.state.day
        const DB = this.state.dbData


        if (DB[county] && DB[county][station]) {

            // console.log(DB[county][station])


            // const linePlot = calcTotalPerc(DB[county][station], county, station, day)
            const walkAttempts = DB[county][station][day] && DB[county][station][day].walkAttempts || 0
            const walkContacts = DB[county][station][day] && DB[county][station][day].walkContacts || 0

            // console.log('raising the bar...')
            // console.log(linePlot)

            this.setState(
                {
                    //  linePlot,
                    walkAttempts,
                    walkContacts
                },
                () => {
                    this.setupData()
                }
            )
        } else {
            // console.log('settin to zero...')
            this.setState(
                {
                    // linePlot: [0, 0, 0, 0, 0],
                    walkAttempts: [0, 0, 0, 0, 0],
                    walkContacts: [0, 0, 0, 0, 0]
                },
                () => {
                    this.setupData()
                }
            )
        }
        // })
    }


    handleSelect = options => {
        // console.log('handle new select options...')
        // console.log(options)
        // console.log('old state...')
        // console.log(this.state)

        // this.unsubAutoData(this.state)

        this.setState(
            {
                county: options.county,
                station: options.station,
                day: options.day
            },
            () => {
                this.getData()
                // this.autoData()
            }
        )
    }

    render() {
        return (
            <div className={classes.container}>
                <br />
                <SelectCountyDay handleSelect={this.handleSelect} />
                <br />
                <Paper elevation={1} className={classes.barGraphPaper}>
                    {this.state.data1 ? (
                        <Bar
                            data={this.state.data1}
                            // width={100}
                            // height={50}
                            options={graphOptionsWalk}
                        // plugins={plugins}
                        />
                    ) : null}

                </Paper>
            </div>
        )
    }
}
/* GraphPackets.propTypes = {
	getShiftData: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
	autoUpdateData: PropTypes.func.isRequired // from enhancer (withHandlers - firebase)
}
 */
export default GraphWalk
