import React, { Component } from 'react'
import axios from 'axios'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import MaterialTable from 'material-table'
import Paper from '@material-ui/core/Paper'

import FirstPageIcon from '@material-ui/icons/FirstPage'
import LastPageIcon from '@material-ui/icons/LastPage'

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ViewColumnIcon from '@material-ui/icons/ViewColumn'
import SearchIcon from '@material-ui/icons/Search'
import ClearIcon from '@material-ui/icons/Clear'
import SaveAlt from '@material-ui/icons/SaveAlt'



import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'


// import ChevronRight from '@material-ui/icons/ChevronRight'

import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'

// import moment from 'moment'

import GraphDaily from './Graphs/GraphDaily'
import ChartsDaily from './Graphs/ChartsDaily'


import classes from './DashboardDaily.scss'

function TabContainer(props) {
  return (
    <Typography component="div">
      {props.children}
    </Typography>
  );
}

class DashboardDaily extends Component {
  constructor(props) {
    super(props)
    this.state = {
      places: null,
      dataLoading: true,
      value: 0,
      viewDashboard: null,
      hasRole: '',
      dbData: '',
      cancelCB: '',
      dailyList: [],
      tabVal: 0
    }
    // console.log(props)
    // this.queryAPI()
  }


  componentDidMount() {

    this.props.getDataByDate()
    
      // .then(resp => {
      //   console.log(resp)
      // })



    // this.queryAPI()

    this.props.firebase
      .auth()
      .currentUser.getIdTokenResult()
      .then(idTokenResult => {
        // console.log(idTokenResult)
        let hasRole

        if (idTokenResult.claims.role) {
          hasRole = idTokenResult.claims.role[0]
        } else {
          if (idTokenResult.claims.admin === true) {
            hasRole = 'admin'
          } else {
            hasRole = false
          }
        }

        this.setState(
          {
            hasRole
          },
          () => {
            //  this.getData()
            //  this.getDashData() // SLOW
            this.props.getPlaces()
              .then(doc => {
                // console.log(doc)
                // console.log(doc.data())
                const data = Object.entries(doc.data().stations)

                // console.log(data)
                let tmpArr = []
                data.map(itm => {
                  tmpArr = [
                    ...tmpArr,
                    { [itm[0]]: itm[1] }
                  ]
                })

                // console.log(tmpArr)

                this.setState({
                  places: tmpArr
                }, () => {
                  this.loadData(tmpArr)
                })
              })
              .catch(err => {
                console.log(err)
              })
          }
        )

        // console.log(hasRole)
      })
      .catch(error => {
        console.log(error)
      })
  }

  getDashData = () => {

    const dailyData = this.props.getDailyCollections('DAILY').then(doc => {
      // console.log(doc)
      // console.log(doc.data())
      // console.log(doc.docs[0].data())

      if (!doc.empty) {
        let tmpArr = []
        doc.docs.map(itm => {
          // console.log(itm.id)
          // let returnList = this.getData(itm.id)
          tmpArr = [...tmpArr, itm.id]
        })

        // console.log(tmpArr)
        this.getData(tmpArr)
      } else {
        console.log('no doc exists...')
      }
    })

  }


  getData = (docList) => {

    const token = this.props.firebase.auth().currentUser.getIdToken()

    // console.log(token)

    token
      .then(tok => {
        // console.log(docList)
        let apiArr = docList.map(doc => {
          return axios({
            method: 'get',
            headers: {
              'X-Requested-With': 'XMLHttpRequest',
              Authorization: 'Bearer' + ' ' + tok
            },
            url: `https://us-central1-packet-land.cloudfunctions.net/apiFire/daily/sub?path=DAILY&doc=${doc}`
          })
        })

        // console.log(apiArr)
        axios.all(apiArr).then(
          axios.spread((...respItems) => {
            // console.log(respItems)
            this.processData(docList, respItems)
          })
        )

        // END THEN
      })
      .catch(err => {
        // console.log(err)
      })
  }

  processData = (docList, respItems) => {
    let placesArr = []

    docList.map((itm, idx) => {
      placesArr = [...placesArr, {
        [itm]: respItems[idx].data.list
      }]
    })



    this.loadData(placesArr)

  }

  loadData = placesArr => {

    // console.log(placesArr)


    placesArr.map(place => {

      let placeObj = Object.entries(place)
      let location = placeObj[0][0]
      let territoryList = placeObj[0][1]

      // console.log(placeObj)
      // console.log(location)
      // console.log(territoryList)

      let tmpList = []

      territoryList.map(territory => {
        this.props.getDailyData(location, territory)
          .then(dailyResp => {
            // console.log(dailyResp)
            // console.log(dailyResp.docs)
            dailyResp.docs.map(entry => {
              // console.log(entry.data())

              let tmpEntry = {
                ...entry.data(),
                // date: moment(entry.data().info.currentDate).format('M-D'),
                location: location,
                territory: territory
              }
              tmpList = [...tmpList, tmpEntry]
            })

            let tmpObj = [
              ...this.state.dailyList,
              ...tmpList
            ]

            // console.log(location)
            // console.log(tmpList)

            this.setState({
              dailyList: tmpObj,
              dataLoading: false
            })

          })
      })

    })
  }

  handleTabChange = (event, tabVal) => {
    this.setState({ tabVal })
  };


  render() {

    // console.log(this.state.dailyList)
    // let dataObj = {
    //   date: this.state.dailyList.info.currentDate,
    //   user: this.state.dailyList.info.createdByUsername,
    //  doorsKnocked: this.state.dailyList.numbers.doorsKnocked,
    //  callAttempts: this.state.dailyList.numbers.callAttempts,
    //  votersID: this.state.dailyList.numbers.votersID,
    //  votersRegistered: this.state.dailyList.numbers.votersRegistered,
    //  shiftsRecruited: this.state.dailyList.numbers.shiftsRecruited,
    //  shiftsCompleted: this.state.dailyList.numbers.shiftsCompleted,
    //  vbmForms: this.state.dailyList.numbers.vbmForms,
    //  digitalVols: this.state.dailyList.numbers.digitalVols,
    //  outreach: this.state.dailyList.feedback.outreach,
    //  high: this.state.dailyList.feedback.dayHigh,
    //  low: this.state.dailyList.feedback.dayLow,
    //  problems: this.state.dailyList.feedback.problems,
    //  solutions: this.state.dailyList.feedback.solutions,
    //  bestPractices: this.state.dailyList.feedback.bestPractices,
    // }




    return (this.state.hasRole === 'admin' || this.state.hasRole === 'director') ? (
      <div className={classes.dailyDashShell}>
        <Tabs value={this.state.tabVal} onChange={this.handleTabChange}>
          <Tab label="Daily" />
          <Tab label="Graphs" />
          <Tab label="Charts" />
        </Tabs>

        {this.state.tabVal === 0 && <TabContainer>
          <Paper>
            <MaterialTable
              options={{
                toolbar: true,
                paging: true,
                pageSize: 10,
                pageSizeOptions: [10, 25, 50, 100, 500],
                columnsButton: true,
                exportButton: true
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: this.state.dataLoading ? 'Loading Data...' : 'No Data Availble for Display...'
                }
              }}

              isLoading={this.state.dataLoading}
              columns={[
                {
                  title: 'Date',
                  field: 'info.currentDate',
                  defaultSort: 'desc',
                  type: 'date'
                },
                { title: 'Location', field: 'location' },
                { title: 'User', field: 'info.createdByName' },
                { title: 'Door Knocks', field: 'numbers.doorsKnocked', hidden: true },
                { title: 'Call Attempts', field: 'numbers.callAttempts', hidden: true },
                { title: "Voters ID'd", field: 'numbers.votersID', hidden: true },
                { title: 'Voters Registered', field: 'numbers.votersRegistered', hidden: true },
                { title: 'Shifts Recruited', field: 'numbers.shiftsRecruited', hidden: true },
                { title: 'Shifts Completed', field: 'numbers.shiftsCompleted', hidden: true },
                { title: 'VBM Forms', field: 'numbers.vbmForms', hidden: true },
                { title: 'Digital Vols', field: 'numbers.digitalVols', hidden: true },
                { title: 'Outreach', field: 'feedback.outreach', hidden: true },
                { title: 'Highs', field: 'feedback.dayHigh', hidden: true },
                { title: 'Lows', field: 'feedback.dayLow', hidden: true },
                { title: 'Problems', field: 'feedback.problems', hidden: true },
                { title: 'Solutions', field: 'feedback.solutions', hidden: true },
                { title: 'Best Practices', field: 'feedback.bestPractices', hidden: true },
              ]}
              data={this.state.dailyList}
              title="Daily Dashboard"
              icons={{
                FirstPage: FirstPageIcon,
                LastPage: LastPageIcon,
                PreviousPage: ChevronLeftIcon,
                NextPage: ChevronRightIcon,
                DetailPanel: ChevronRightIcon,
                Clear: ClearIcon,
                Search: SearchIcon,
                ViewColumn: ViewColumnIcon,
                ResetSearch: ClearIcon,
                Export: SaveAlt
              }}
              detailPanel={rowData => {
                return (
                  <div className={classes.dailyDataInfoShell}>
                    <Grid container spacing={16} alignItems="stretch">

                      <Grid item xs={12} sm={6}>
                        <Card>
                          <CardActionArea>
                            <CardContent>
                              <Typography gutterBottom variant="subtitle1">
                                Feedback
                          </Typography>
                              <Typography component="ul" className={classes.listUL}>

                                <li><b>Outreach: </b>{rowData.feedback.outreach}</li>
                                <li><b>High: </b>{rowData.feedback.dayHigh}</li>
                                <li><b>Low: </b>{rowData.feedback.dayLow}</li>
                                <li><b>Problems: </b>{rowData.feedback.problems}</li>
                                <li><b>Solutions: </b>{rowData.feedback.solutions}</li>
                                <li><b>Best Practices: </b>{rowData.feedback.bestPractices}</li>

                              </Typography>
                              <br />
                            </CardContent>
                          </CardActionArea>

                        </Card>
                      </Grid>



                      <Grid item xs={12} sm={6}>
                        <Card>
                          <CardActionArea>
                            <CardContent>
                              <Typography gutterBottom variant="subtitle1">
                                Numbers
                          </Typography>
                              <Typography component="ul" className={classes.listUL}>

                                <li><b>Doors Knocked: </b> {rowData.numbers.doorsKnocked}</li>
                                <li><b>Call Attempts: </b> {rowData.numbers.callAttempts}</li>
                                <li><b>Voters ID'd: </b> {rowData.numbers.votersID}</li>
                                <li><b>Voters Registered: </b> {rowData.numbers.votersRegistered}</li>
                                <li><b>Shifts Recruited: </b> {rowData.numbers.shiftsRecruited}</li>
                                <li><b>Shifts Completed: </b> {rowData.numbers.shiftsCompleted}</li>
                                <li><b>VBM Forms: </b> {rowData.numbers.vbmForms}</li>
                                <li> <b>Digital Vols: </b> {rowData.numbers.digitalVols}</li>

                              </Typography>
                              <br />
                            </CardContent>
                          </CardActionArea>
                        </Card>
                      </Grid>

                    </Grid>
                  </div>
                )
              }}
              onRowClick={(event, rowData, togglePanel) => togglePanel()}
            />
          </Paper>
        </TabContainer>}

        {this.state.tabVal === 1 && <TabContainer><GraphDaily places={this.state.places} list={this.state.dailyList} /></TabContainer>}
        {this.state.tabVal === 2 && <TabContainer><ChartsDaily places={this.state.places} list={this.state.dailyList} /></TabContainer>}

      </div>
    ) : (
        <div>Sorry you are Not Authorized to view this area...</div>
      )
  }
}

export default DashboardDaily

/**
 *
 * FIXED BUG in material-table
 * m-table-toolbar.js

 var data = _this.props.renderData.map(function (rowData) {
          return columns.map(function (columnDef) {
            let splitField = columnDef.field.split('.')
            return String(columnDef.field).includes('.') ? rowData[splitField[0]][splitField[1]] : rowData[columnDef.field];
            // return columnDef.lookup ? columnDef.lookup[rowData[columnDef.field]] : rowData[columnDef.field];
          });
        });
 */

// doorsKnocked: '',
// callAttempts: '',
// votersID: '',
// votersRegistered: '',
// shiftsRecruited: '',
// shiftsCompleted: '',
// vbmForms: '',
// digitalVols: ''

// feedback: {
//   outreach: '',
//   dayHigh: '',
//   dayLow: '',
//   problems: '',
//   solutions: '',
//   bestPractices: ''
// }


/* onRowClick={(event, rowData) => {
              this.clickEdit(rowData.info.cuid)
            }} */


/*

https://github.com/mbrn/material-table/issues/93

 import { SvgIconProps } from '@material-ui/core/SvgIcon'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
import SaveAlt from '@material-ui/icons/SaveAlt'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Add from '@material-ui/icons/Add'
import Check from '@material-ui/icons/Check'
import FilterList from '@material-ui/icons/FilterList'
import Remove from '@material-ui/icons/Remove' */





/*
 icons={{
            Check: Check,
            DetailPanel: ChevronRight,
            Export: SaveAlt,
            Filter: FilterList,
            FirstPage: FirstPage,
            LastPage: LastPage,
            NextPage: ChevronRight,
            PreviousPage: ChevronLeft,
            Search: Search,
            ThirdStateCheck: Remove,
          }}

icons={{
      Check: () => <Check /> as React.ReactElement<SvgIconProps>,
      Export: () => <SaveAlt /> as React.ReactElement<SvgIconProps>,
      Filter: () => <FilterList /> as React.ReactElement<SvgIconProps>,
      FirstPage: () => <FirstPage /> as React.ReactElement<SvgIconProps>,
      LastPage: () => <LastPage /> as React.ReactElement<SvgIconProps>,
      NextPage: () => <ChevronRight /> as React.ReactElement<SvgIconProps>,
      PreviousPage: () => <ChevronLeft /> as React.ReactElement<SvgIconProps>,
      Search: () => <Search /> as React.ReactElement<SvgIconProps>,
      ThirdStateCheck: () => <Remove /> as React.ReactElement<SvgIconProps>,
      ViewColumn: () => <ViewColumn /> as React.ReactElement<SvgIconProps>,
      DetailPanel: () => <ChevronRight /> as React.ReactElement<SvgIconProps>,
    }} */