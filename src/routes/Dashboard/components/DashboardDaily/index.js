import DashboardDaily from './DashboardDaily'
import enhance from './DashboardDaily.enhancer'

export default enhance(DashboardDaily)
