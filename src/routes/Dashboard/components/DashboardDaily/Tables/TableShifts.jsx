import React, { Component } from 'react'
import axios from 'axios'
import moment from 'moment'
import classes from './Tables.scss'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import NativeSelect from '@material-ui/core/NativeSelect'
import ToggleButton from '@material-ui/lab/ToggleButton'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'
import Paper from '@material-ui/core/Paper'
import { stations, locations, STATION_MAP } from '../../../../../constants/places'
import { CURRENT_DAY } from '../../../../../constants/dashboard'

import {
	calcVolShifts,
	calcShiftName,
	calcShiftNameEDay,
	calcDayNum,
	calcTotalPerc,
	calcTotalPercByDay,
	calcTotalPercByDayStation,
	calcTotalPacks,
	compoundArray
} from '../Utils/CalcFunc'

import { numToPerc, secToDate } from '../Utils/FormatFunc'

import { queryVanSignupsAPI } from '../Utils/QueryAPI'

class TableShifts extends Component {
	constructor(props) {
		super(props)
		this.state = {
			selectDay: CURRENT_DAY,
			shiftBtn: '0',
			// cancelCB: '',
			dayEvents: '',
			dbData: '',
			grandTotalPacketsIn: [0],
			grandTotalCallers: [0],
			grandTotalPackets: [0],
			grandTotalPacketsOut: [0],
			grandTotalVolsBegun: [0],
			grandTotalVolsScheduled: [0],
			grandTotalWalkAttempts: [0],
			grandTotalWalkContacts: [0]
			// Oneida: '',
			// Herkimer: '',
			// Oswego: '',
			// Madison: '',
			// Chenango: '',
			// Cortland: '',
			// Broome: '',
			// Tioga: ''
		}

		//	console.log(props)

	}

	componentDidUpdate(prevProps) {
		if (this.props.dbData !== prevProps.dbData) {
			// console.log(this.props)

			const DB = this.props.dbData

			this.setState({
				// events: this.props.events
				dbData: this.props.dbData,
				Oneida: DB.oneida ? DB.oneida : '',
				Herkimer: DB.herkimer ? DB.herkimer : '',
				Oswego: DB.oswego ? DB.oswego : '',
				Madison: DB.madison ? DB.madison : '',
				Chenango: DB.chenango ? DB.chenango : '',
				Cortland: DB.cortland ? DB.cortland : '',
				Broome: DB.broome ? DB.broome : '',
				Tioga: DB.tioga ? DB.tioga : ''
			}, () => {
				this.setupData()
				// this.getData()
				// this.autoData()
			})

		}

		if (this.props.dayEvents !== prevProps.dayEvents) {
			this.setState({
				dayEvents: this.props.dayEvents
			}, () => {

				// console.log(this.props.dayEvents)
				this.setupData()
			})
		}


	}


	setupData() {

		let grandTotalPacketsIn = []
		let grandTotalPacketsOut = []
		let grandTotalVolsBegun = []
		let grandTotalVolsScheduled = [0]
		let grandTotalCallers = []
		let grandTotalWalkAttempts = []
		let grandTotalWalkContacts = []


		// console.log('setting up data...')
		locations.map(county => {
			const signups = this.state.dayEvents
			const district = this.state[county]
			const dataLives = district ? true : false

			if (dataLives) {
				// console.log(`Data in county: ${county}`)
				stations[county].map(stationName => {

					//stationNameX = stationName
					// console.log(stationName)
					const station = district[stationName.replace(' ', '_').toLowerCase()]
					// console.log(stationName.toLowerCase())
					// const stationDataLives = station ? true : false
					const stationDataLives = station ? true : false

					// console.log(station)
					if (stationDataLives) {
						// console.log(`Data in station: ${stationName}`)
						// console.log(station)

						// VARS
						let stationStatus
						let stationPerc
						let packetsIn
						let packetsOut
						let totalPackets
						let callersHubdialer
						let callersPaper
						let totalCallers
						let volsBegun
						let volsScheduled
						let walkAttempts
						let walkContacts
						let shiftUpdate

						let mostRecentDay = this.state.selectDay
						// console.log(station[mostRecentDay])

						let mostRecentShift = Number(this.state.shiftBtn)

						// let shiftName = calcShiftName(mostRecentShift)

						let shiftName


						if (mostRecentDay === 'day4') {
							shiftName = calcShiftNameEDay(mostRecentShift)
						} else {
							shiftName = calcShiftName(mostRecentShift)
						}



						let dayNum = calcDayNum(mostRecentDay)




						// console.log(mostRecentShift)
						// console.log(station)
						// console.log(county)
						// console.log(stationName)



						// CHECK IF DAY EXISTS
						if (station[mostRecentDay]) {

							// SET STATION STATUS
							stationStatus = station[mostRecentDay]['info']['stationOpen']
							// console.log(stationStatus)

							// SET TOTAL PACKETS
							//	totalPackets = calcTotalPacks(mostRecentDay, county, stationName) // Number(station[mostRecentDay]['info']['totalPackets'])


							// FIND TOTAL% vs PACKETS IN
							// stationPerc = calcTotalPercByDayStation(station, mostRecentDay, county, stationName).slice(-1)[0]
							//stationPerc = calcTotalPercByDay(station).slice(-1)[0]
							// stationPerc = calcTotalPerc(station).slice(-1)[0]
							// console.log(stationPerc)

							// PACKETS IN
							if (station[mostRecentDay]['packetsIn'][mostRecentShift]) {
								packetsIn = Number(station[mostRecentDay]['packetsIn'][mostRecentShift])
							}

							// PACKETS IN
							if (station[mostRecentDay]['packetsOut'][mostRecentShift]) {
								packetsOut = Number(station[mostRecentDay]['packetsOut'][mostRecentShift])
							}



							// TALLY CALLERS
							if (station[mostRecentDay]['volsHubdialer'][mostRecentShift] || station[mostRecentDay]['volsPaper'][mostRecentShift]) {
								callersHubdialer = station[mostRecentDay]['volsHubdialer'][mostRecentShift] ? Number(station[mostRecentDay]['volsHubdialer'][mostRecentShift]) : 0
								callersPaper = station[mostRecentDay]['volsPaper'][mostRecentShift] ? Number(station[mostRecentDay]['volsPaper'][mostRecentShift]) : 0
								totalCallers = callersHubdialer + callersPaper
							}

							// SET VOLS BEGUN
							if (station[mostRecentDay]['volsBegun'][mostRecentShift]) {
								volsBegun = Number(station[mostRecentDay]['volsBegun'][mostRecentShift])
							}

							// SET WALK ATTEMPTS
							if (station[mostRecentDay]['walkAttempts'][mostRecentShift]) {
								walkAttempts = Number(station[mostRecentDay]['walkAttempts'][mostRecentShift])
							}

							// SET WALK CONTACTS
							if (station[mostRecentDay]['walkContacts'][mostRecentShift]) {
								walkContacts = Number(station[mostRecentDay]['walkContacts'][mostRecentShift])
							}

							// SHIFT UPDATE
							if (station[mostRecentDay]['shiftUpdate'][mostRecentShift]) {
								shiftUpdate = station[mostRecentDay]['shiftUpdate'][mostRecentShift]
							}


							/* 	if (signups.length > 0) {
									// const volShifts = calcVolShifts(signups, county, stationName, STATION_MAP)
									const volShifts = calcVolShifts(signups[dayNum], county, stationName, STATION_MAP)
	
									if (volShifts[shiftName] && volShifts[shiftName].length > 0) {
										// console.log(volShifts[shiftName])
										volsScheduled = volShifts[shiftName].length
									}								// console.log(volShifts)
									// console.log(shiftName)
								} */
						}
						// console.log(signups)
						if (signups.length > 0) {
							//CALC VOL SHIFTS
							const volShifts = calcVolShifts(signups[dayNum], county, stationName, STATION_MAP)
							// VOL SHIFTS SCHEDULES
							if (volShifts[shiftName] && volShifts[shiftName].length > 0) {
								//	console.log(volShifts[shiftName])
								volsScheduled = volShifts[shiftName].length
							}

							grandTotalVolsScheduled.push(volsScheduled ? Number(volsScheduled) : 0)

						}

						// volsScheduled = 0


						grandTotalPacketsIn.push(packetsIn ? Number(packetsIn) : 0)
						grandTotalPacketsOut.push(packetsOut ? Number(packetsOut) : 0)
						grandTotalVolsBegun.push(volsBegun ? Number(volsBegun) : 0)

						grandTotalCallers.push(totalCallers ? Number(totalCallers) : 0)
						grandTotalWalkAttempts.push(walkAttempts ? Number(walkAttempts) : 0)
						grandTotalWalkContacts.push(walkContacts ? Number(walkContacts) : 0)

						// console.log()
						this.setState({
							grandTotalPacketsIn,
							grandTotalPacketsOut,
							grandTotalVolsBegun,
							grandTotalVolsScheduled,
							grandTotalCallers,
							grandTotalWalkAttempts,
							grandTotalWalkContacts,
							[stationName.toLowerCase()]: {
								callers: totalCallers,
								// totalPackets: totalPackets,
								packetsIn: packetsIn,
								packetsOut: packetsOut,
								// totalPerc: stationPerc,
								walkAttempts: walkAttempts,
								walkContacts: walkContacts,
								volsScheduled: volsScheduled,
								volsBegun: volsBegun,
								stationStatus: stationStatus,
								shiftUpdate: shiftUpdate
							}
						})

					} else {
						// console.log(`No data in ${stationName} station`)
					}
				})

				// NO DATA...YET
			} else {
				// console.log(`No data in ${county} district`)
			}


		})






		// END SETUP
	}

	onSelectDay = (evt) => {
		const day = evt.target.value
		// console.log(day)
		this.setState({
			selectDay: day,
			shiftBtn: '0'
		}, () => {
			// this.getSignups()
			this.setupData()
		})
	}
	handleShiftBtn = (evt, btnVal) => {
		//console.log(evt)
		// console.log(btnVal)
		this.setState({
			shiftBtn: btnVal
		}, () => {
			// this.getSignups()
			this.setupData()
		})
	}

	render() {
		return (
			<div className={classes.container}>

				<Typography variant="headline" component="h3" style={{ textAlign: 'center' }}>
					Shifts
				</Typography>
				<br />
				<Grid container>


					<Grid item xs={3} md={5}>
						<NativeSelect onChange={this.onSelectDay} value={this.state.selectDay}>
							<option value="day1">Day 1</option>
							<option value="day2">Day 2</option>
							<option value="day3">Day 3</option>
							<option value="day4">Day 4</option>
						</NativeSelect>
					</Grid>

					<Grid item xs={9} md={7} style={{ flexBasis: 'auto' }}>
						<ToggleButtonGroup className={classes.toggleBtns} value={this.state.shiftBtn} exclusive onChange={this.handleShiftBtn}>
							<ToggleButton value={'0'}>{this.state.selectDay === 'day4' ? `7AM` : `9AM`}</ToggleButton>
							<ToggleButton value={'1'}>{this.state.selectDay === 'day4' ? `9AM` : `12PM`}</ToggleButton>
							<ToggleButton value={'2'}>{this.state.selectDay === 'day4' ? `12PM` : `3PM`}</ToggleButton>
							<ToggleButton value={'3'}>{this.state.selectDay === 'day4' ? `3PM` : `6PM`}</ToggleButton>
							<ToggleButton value={'4'}>{this.state.selectDay === 'day4' ? `6PM` : `9PM`}</ToggleButton>
						</ToggleButtonGroup>
					</Grid>

				</Grid>
				<br />
				<Paper elevation={1} className={classes.totalPaper} style={{ overflow: 'auto' }}>
					<table>
						<tbody>
							{locations.map((item, idx) => {
								const grandTotalPacketsIn = compoundArray(this.state.grandTotalPacketsIn)
								const grandTotalPacketsOut = compoundArray(this.state.grandTotalPacketsOut)
								const grandTotalVolsBegun = compoundArray(this.state.grandTotalVolsBegun)
								const grandTotalVolsScheduled = compoundArray(this.state.grandTotalVolsScheduled)
								const grandTotalCallers = compoundArray(this.state.grandTotalCallers)
								const grandTotalWalkAttempts = compoundArray(this.state.grandTotalWalkAttempts)
								const grandTotalWalkContacts = compoundArray(this.state.grandTotalWalkContacts)

								// let grandTotalPackPerc = (grandTotalPacketsIn / grandTotalPackets) * 100
								let grandTotalWalkHitsPerc = (grandTotalWalkContacts / grandTotalWalkAttempts) * 100

								// grandTotalPackPerc = numToPerc(grandTotalPackPerc, grandTotalPacketsIn)
								grandTotalWalkHitsPerc = numToPerc(grandTotalWalkHitsPerc, grandTotalWalkContacts)

								let grandTotalVolHits = ((grandTotalVolsScheduled - grandTotalVolsBegun) / grandTotalVolsScheduled) * 100
								grandTotalVolHits = numToPerc(grandTotalVolHits, grandTotalVolsScheduled - grandTotalVolsBegun)
								grandTotalVolHits = parseInt(grandTotalVolHits) // * -1

								let inRed
								let inGreen
								if (grandTotalVolHits > 0) {
									// grandTotalVolHits = '+' + grandTotalVolHits
									inRed = true
								} else if (grandTotalVolHits < 0) {

									inGreen = true
								}
								return (
									<tr key={idx}>
										<td className={classes.tableTD} />
										<td>
											<table cellSpacing="0">
												<tbody>
													{idx === 0 ? (
														<tr>
															<th className={classes.totalsTH}>TOTALS</th>
															<th>{''}</th>
															<th>{grandTotalPacketsIn}</th>
															<th>{grandTotalPacketsOut}</th>
															<th>{grandTotalVolsBegun}</th>
															<th>{grandTotalVolsScheduled}</th>
															<th><div className={inRed ? classes.inRed : inGreen ? classes.inGreen : classes.isZero}>{grandTotalVolHits}%</div></th>
															<th>{grandTotalCallers}</th>
															<th>{grandTotalWalkAttempts}</th>
															<th>{grandTotalWalkContacts}</th>
															<th>{grandTotalWalkHitsPerc}</th>

														</tr>
													) : null}

													{idx === 0 || idx === 4 ? (
														<tr className={classes.shownText}>
															<th />
															{/*<th>{idx === 0 || idx === 4 ? <span className={classes.shownText}>% Packets</span> : <span className={classes.blankText}>% Packets</span>}</th>*/}
															<th>Shift Updated</th>
															<th>Packets In</th>
															<th>Packets Out</th>
															{/* <th>{idx === 0 || idx === 4 ? <span className={classes.shownText}>#Packets</span> : <span className={classes.blankText}>#Packets</span>}</th> */}
															<th>#Vols Begun</th>
															<th>#Vols Scheduled</th>
															<th>%Flake Rate</th>
															<th>#People Calling</th>
															<th>Walk Attempts</th>
															<th>Walk Contacts</th>
															<th>Walk Hits%</th>
														</tr>
													) : null}
													<tr>
														<th className={classes.tableCountyName}>{item}</th>
													</tr>
													{stations[item].map((stationNameCap, idx2) => {
														const stationName = stationNameCap.toLowerCase()

														let shiftUpdate = this.state[stationName] && this.state[stationName].shiftUpdate ? this.state[stationName].shiftUpdate : '-'

														const callers = this.state[stationName] && this.state[stationName].callers ? this.state[stationName].callers : 0
														// const totalPackets = this.state[stationName] && this.state[stationName].totalPackets ? this.state[stationName].totalPackets : 0
														let packetsIn = this.state[stationName] && this.state[stationName].packetsIn ? this.state[stationName].packetsIn : 0
														const packetsOut = this.state[stationName] && this.state[stationName].packetsOut ? this.state[stationName].packetsOut : 0

														// const totalPerc = this.state[stationName] && this.state[stationName].totalPerc ? this.state[stationName].totalPerc : 0
														const stationStatus = this.state[stationName] && this.state[stationName].stationStatus ? this.state[stationName].stationStatus : false
														const volsScheduled = this.state[stationName] && this.state[stationName].volsScheduled ? this.state[stationName].volsScheduled : 0
														const volsBegun = this.state[stationName] && this.state[stationName].volsBegun ? this.state[stationName].volsBegun : 0
														let walkAttempts = this.state[stationName] && this.state[stationName].walkAttempts ? this.state[stationName].walkAttempts : 0
														let walkContacts = this.state[stationName] && this.state[stationName].walkContacts ? this.state[stationName].walkContacts : 0
														let walkHits

														// WALK ZERO OUT FIRST SHIFT
														if (this.state.shiftBtn === '0') {
															packetsIn = '-'
														}

														// WALK ZERO OUT FIRST SHIFT
														if (this.state.shiftBtn === '0') {
															walkAttempts = '-'
															walkContacts = '-'
															walkHits = '-'
														} else {
															walkHits = walkContacts / walkAttempts * 100
															isNaN(walkHits) ? walkHits = 0 : null
															walkHits === Infinity || walkHits === -Infinity ? walkHits = walkContacts / 1 * 100 : null
															walkHits = String(parseInt(walkHits)) + '%'
														}

														// SHIFT UPDATE FORMAT
														// console.dir(lastUpdate)
														if (shiftUpdate !== '-') {
															let m = moment(shiftUpdate.seconds * 1000).utc().local()
															let s = m.format("MMM Do h:mma")
															shiftUpdate = s // new Date(lastUpdate.seconds).toString() // String(lastUpdate.seconds)
														}

														// FLAKE RATE
														let flakeRate = (Number(volsScheduled) - Number(volsBegun)) / Number(volsScheduled) * 100
														flakeRate = numToPerc(flakeRate, (Number(volsScheduled) - Number(volsBegun)))
														flakeRate = parseInt(flakeRate) //* -1

														let inRed
														let inGreen
														// console.log(flakeRate)

														if (flakeRate > 0) {
															// flakeRate = '+' + flakeRate
															inRed = true
														} else if (flakeRate < 0) {
															inGreen = true
														}

														return (
															<tr key={`data-${idx2}`} className={classes.tableRowData}>
																<td className={classes.subTableStationName}>
																	{stationName}<div className={stationStatus ? classes.stationStatusOpen : classes.stationStatusClosed}></div>
																</td>
																<td className={classes.subTableDataDate}><div className={classes.dataCellDate}>{shiftUpdate}</div></td>
																{/* <td className={classes.subTableData}><div className={classes.dataCell}>{totalPerc}%</div></td> */}
																<td className={classes.subTableData}><div className={classes.dataCell}>{packetsIn}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{packetsOut}</div></td>

																{/* <td className={classes.subTableData}><div className={classes.dataCell}>{totalPackets}</div></td> */}
																<td className={classes.subTableData}><div className={classes.dataCell}>{volsBegun}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{volsScheduled}</div></td>
																<td className={classes.subTableData}>
																	<div className={classes.dataCell}>
																		<div className={inRed ? classes.inRed : inGreen ? classes.inGreen : classes.isZero}>{flakeRate}%</div>
																	</div>
																</td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{callers}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{walkAttempts}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{walkContacts}</div></td>
																<td className={classes.subTableData}><div className={classes.dataCell}>{walkHits}</div></td>

															</tr>
														)
													})}
												</tbody>
											</table>
										</td>
									</tr>
								)
							})}
						</tbody>
					</table>
					<br />
				</Paper>
				<br />
				<br />
			</div>
		)
	}
}

/* TableShifts.propTypes = {
	getAllShiftData: PropTypes.func.isRequired // from enhancer (withHandlers - firebase)
	// autoUpdateData: PropTypes.func.isRequired // from enhancer (withHandlers - firebase)
} */

export default TableShifts


/* <th>{idx === 0 ? <span className={classes.shownText}>% Packets In</span> : <span className={classes.blankText}>% Packets In</span>}</th> */
/*<td className={classes.subTableData}><div className={classes.dataCell}>{totalPerc}% | {totalPackets}</div></td>*/


/*

					{this.state.selectDay === 'day4' ? <ToggleButton value={'0'}>7AM</ToggleButton> : null}
							<ToggleButton value={this.state.selectDay === 'day4' ? '1' : '0'}>9AM</ToggleButton>
							<ToggleButton value={this.state.selectDay === 'day4' ? '2' : '1'}>12PM</ToggleButton>
							<ToggleButton value={this.state.selectDay === 'day4' ? '3' : '2'}>3PM</ToggleButton>
							<ToggleButton value={this.state.selectDay === 'day4' ? '4' : '3'}>6PM</ToggleButton>
							{this.state.selectDay !== 'day4' ? <ToggleButton value={this.state.selectDay === 'day4' ? '5' : '4'}>9PM</ToggleButton> : null}

							*/

