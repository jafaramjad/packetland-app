import { compose } from 'redux'
import { connect } from 'react-redux'
import { LIST_PATH } from 'constants'
import { withHandlers, withProps, withStateHandlers, pure } from 'recompose'
import { firestoreConnect, isEmpty, isLoaded } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'

export default compose(
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  // Map auth uid from state to props
  // connect(({ firebase: { auth: { uid } } }) => ({ uid })),
  // // Create listeners based on current users UID
  // firestoreConnect(({ params }) => []),
  connect((store) => {
    return {
      accessDash: store.accessDash,
      uid: store.firebase.auth.uid,
      profile: store.firebase.profile
    }
  }),
  // Wait for uid to exist before going further
  // Create listeners based on current users UID
  firestoreConnect(({ params, uid }) => [
    // Listener for projects the current user created
    // {
    // 	collection: 'projects',
    // 	where: ['createdBy', '==', uid]
    // }
  ]),
  spinnerWhileLoading(['uid']),
  // Map projects from state to props
  /* 	connect(({ firestore: { ordered } }) => ({
			projects: ordered.projects
		})), */
  // Show loading spinner while projects and collabProjects are loading
  // spinnerWhileLoading(['Dashboard']),
  // Add props.router
  withRouter,
  // Add props.showError and props.showSuccess
  withNotifications,
  // Add state and state handlers as props
  withStateHandlers(
    // Setup initial state
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    // Add state handlers as props
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),

  // Add handlers as props
  withHandlers({
    getPlaces: props => newInstance => {
      const { firestore, uid, showError, showSuccess, toggleDialog } = props
      if (!uid) {
        return console.error('You must be logged in')
      }

      return (
        firestore
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` })
          .get({ collection: `NC09/MCCREADY/CONSTANTS`, doc: `PLACES` })
      )
    },
    getDailyCollections: props => (id) => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props


      return (

        firestore

          .collection(`NC09/MCCREADY/${id}`)
          .get()


      )

    },

    getDailyData: props => (territory, location) => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props

      // console.log(territory)
      // console.log(location)
      return (

        firestore
          .collection(`NC09/MCCREADY/DAILY/${territory.toUpperCase()}/${location.toUpperCase()}`)
          .get()
      )
    },
    getDataByDate: props => () => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props

      console.log(firestore)

      return true
      // console.log(territory)
      // console.log(location)
      // return (
      //   firestore.collection(`NC09/MCCREADY/DAILY/${territory.toUpperCase()}/${location.toUpperCase()}`)
      //     .get()
      //   // firestore
      //   //   .collectionGroup(`NC09/MCCREADY/DAILY`).where('currentDate', '==', '2019-04-09')
      //   //   .get()
      // )
    },


  }),
  withProps(({ auth, profile }) => ({
    authExists: isLoaded(auth) && !isEmpty(auth),
    profileEmail: profile.email
  })),
  pure // shallow equals comparison on props (prevent unessesary re-renders)
)
