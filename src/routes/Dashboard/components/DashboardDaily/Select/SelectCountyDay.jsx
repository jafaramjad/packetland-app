import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid'
import { stations } from '../../../../../constants/places'
import NativeSelect from '@material-ui/core/NativeSelect'

class SelectCountyDay extends Component {
	constructor(props) {
		super(props)

		this.state = {
			county: 'oneida',
			station: 'utica',
			day: 'day1'
		}
	}

	handleSelectStation = evt => {
		const selectedStation = evt.target.value
		const entries = Object.entries(stations)
		let county

		entries.map((place, idx) => {
			place[1].map((station, idx2) => {
				if (selectedStation === station.replace(' ', '_').toLowerCase()) {
					county = place[0].toLowerCase()
				}
			})
		})

		this.setState({
			county: county,
			station: selectedStation
		})
	}

	onChangeSelect = name => evt => {
		if (name === 'day') {
			const newDay = evt.target.value

			this.setState(
				{
					day: newDay,
					county: this.state.county,
					station: this.state.station
				},
				() => {
					this.props.handleSelect(this.state)
				}
			)
		} else {
			let county
			const selectedStation = evt.target.value
			const entries = Object.entries(stations)

			entries.map((place, idx) => {
				place[1].map((station, idx2) => {
					if (selectedStation === station.replace(' ', '_').toLowerCase()) {
						county = place[0].toLowerCase()
					}
				})
			})

			this.setState(
				{
					day: this.state.day,
					county: county,
					station: selectedStation
				},
				() => {
					this.props.handleSelect(this.state)
				}
			)
		}
	}

	render() {
		return (
			<Grid container>
				<Grid item xs={5} sm={3}>
					<NativeSelect
						onChange={this.onChangeSelect('station')}
						value={this.state.station}>
						<option disabled>Select Station</option>

						<optgroup label="Oneida">
							<option value="utica">Utica</option>
							<option value="new_hartford">New Hartford</option>
							<option value="whitestown">Whitestown</option>
							<option value="rome">Rome</option>
							<option value="trenton">Trenton</option>
							<option value="camden">Camden</option>
						</optgroup>
						<optgroup label="Herkimer">
							<option value="ilion">Ilion</option>
							<option value="little_falls">Little Falls</option>
						</optgroup>
						<optgroup label="Oswego">
							<option value="mexico">Mexico</option>
							<option value="central_square">Central Square</option>
						</optgroup>
						<optgroup label="Madison">
							<option value="cazenovia">Cazenovia</option>
							<option value="oneida">Oneida</option>
							<option value="hamilton_(doors)">Hamilton</option>
						</optgroup>
						<optgroup label="Chenango">
							<option value="norwich_(doors)">Norwich</option>
						</optgroup>
						<optgroup label="Cortland">
							<option value="cortland">Cortland</option>
						</optgroup>
						<optgroup label="Broome">
							<option value="binghamton">Binghamton</option>
							<option value="endicott">Endicott</option>
							<option value="whitney_point">Whitney Point</option>
							<option value="windsor">Windsor</option>
						</optgroup>
						<optgroup label="Tioga">
							<option value="owego">Owego</option>
						</optgroup>
					</NativeSelect>
				</Grid>
				<Grid item xs={7} sm={9}>
					<NativeSelect onChange={this.onChangeSelect('day')} value={this.state.day}>
						<option value="day1">Day 1</option>
						<option value="day2">Day 2</option>
						<option value="day3">Day 3</option>
						<option value="day4">Day 4</option>
					</NativeSelect>
				</Grid>
			</Grid>
		)
	}
}

export default SelectCountyDay
