

// https://stackoverflow.com/questions/49330139/date-toisostring-but-local-time-instead-of-utc
export const toISOLocal = (d) => {
    var z = n => (n < 10 ? '0' : '') + n;
    var off = d.getTimezoneOffset();
    var sign = off < 0 ? '+' : '-';
    off = Math.abs(off);

    return d.getFullYear() + '-' + z(d.getMonth() + 1) + '-' +
        z(d.getDate()) + 'T' + z(d.getHours()) + ':' + z(d.getMinutes()) +
        ':' + z(d.getSeconds()) + sign + z(off / 60 | 0) + z(off % 60);
}
