import { DASHBOARD_PATH as path } from 'constants'

export default store => ({
  path,
  /*  Async getComponent is only invoked when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure(
      [],
      require => {
        /*  Webpack - use require callback to define
          dependencies for bundling   */
        const Dashboard = require('./components/DashboardDaily').default

        /*  Return getComponent   */
        cb(null, Dashboard)

        /* Webpack named bundle   */
      },
      'Dashboard'
    )
  } /* ,
  getChildRoutes(partialNextState, cb) {
    require.ensure([], require => {

      const Project = require('./routes/Project').default

      cb(null, [Project(store)])
    })
  } */
})
