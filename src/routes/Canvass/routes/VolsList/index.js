export default store => ({
  path: ':volsList',
  /*  Async getComponent is only invoked when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure(
      [],
      require => {
        /*  Webpack - use require callback to define
          dependencies for bundling   */
        const VOLS_LIST = require('./components/VolsList').default

        /*  Return getComponent   */
        cb(null, VOLS_LIST)

        /* Webpack named bundle   */
      },
      'VolsList'
    )
  }
})
