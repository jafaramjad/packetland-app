import React, { Component } from 'react'
import axios from 'axios'
import classes from './VolsList.scss'
import NativeSelect from '@material-ui/core/NativeSelect'
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalkOutlined'
import CallIcon from '@material-ui/icons/Call'
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

// const statusTypes = {
//     Scheduled: 1,
//     Confirmed: 11,
//     ConfTwice: 23
// }

const DAYS = ['NOV 3', 'NOV 4', 'NOV 5', 'NOV 6']


class AllVols extends Component {

    constructor(props) {
        super(props)

        // const locationStation = this.props.location.split(",")
        const location = this.props.location  // locationStation[0]
        const station = this.props.station // locationStation[1].replace(" ", "")

        this.state = {
            selectStatus: {},
            location: location,
            station: station,
            day: '',
            allVols: []
        }

        // console.log(props)
    }


    componentWillUpdate(props) {
        // console.log(props)
        // console.log(props.volunteers)
    }

    componentDidUpdate(prevProps) {
        if (this.props.volunteers !== prevProps.volunteers) {
            // console.log(this.props)

            let selectStatus = {}
            this.props.volunteers.map(item => {

                selectStatus[item.eventSignupId] = item.status.statusId
            })

            this.setState({
                allVols: this.props.volunteers, //.reverse(),
                selectStatus: selectStatus
                // selectStatus: this.props.volStatusUpdates.status

            })
        } else if (this.props.volStatusUpdates !== prevProps.volStatusUpdates) {
            // console.log(this.props)

            // console.log(this.props.volStatusUpdates)
            let selectStatus = {}

            Object.entries(this.props.volStatusUpdates).map(volItem => {
                Object.entries(volItem[1].status).map(item => {
                    selectStatus[item[0]] = item[1]
                })

            })

            // console.log(Object.entries(this.props.volStatusUpdates))
            // console.log(selectStatus)

            this.setState({

                // selectStatus: selectStatus
                selectStatus: { ...selectStatus }

            })
        } else {

        }
    }


    handleChangeStatus = (id, dateTime) => (evt) => {
        // console.log(id)
        // console.log(dateTime)
        // console.log(evt.target.value)
        const selectEl = document.getElementById('select-' + id)
        const day = dateTime.split(',')[0].toUpperCase() // .replace(" ", "").toLowerCase()
        selectEl.value = evt.target.value

        //   console.log(day)

        this.setState({
            selectStatus: {
                [id]: evt.target.value
                // day: day
            }
        }, () => {
            this.postVolStatus(day)
        })
    }

    findDayName = (dateName) => {

        // console.log(dateName)
        let dayName
        let dayNum
        switch (dateName) {
            case DAYS[0]:
                dayName = 'day1'
                dayNum = 0
                break
            case DAYS[1]:
                dayName = 'day2'
                dayNum = 1
                break
            case DAYS[2]:
                dayName = 'day3'
                dayNum = 2
                break
            case DAYS[3]:
                dayName = 'day4'
                dayNum = 3
                break
            default:
                break
        }

        return dayName
    }


    postVolStatus = (day) => {



        const dayName = this.findDayName(day)

        let passInfo = {
            "locationName": this.state.location.toUpperCase(),
            "stationName": this.state.station.toUpperCase(),
            "dayName": dayName,
        }

        const sendToDB = {
            "status": this.state.selectStatus,
            "info": passInfo
        }

        this.props.addVolData(sendToDB, "Updated volunteer status successfully")
        this.props.firebase.push('volunteers', sendToDB)


    }


    getPhone = id => (evt) => {

        // console.log(id)

        const vanId = id

        const token = this.props.firebase.auth().currentUser.getIdToken()

        token.then(tok => {

            axios({
                method: 'get',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Authorization': 'Bearer' + ' ' + tok
                },
                //url: `http://localhost:3000/van/events?q=`
                url: `https://us-central1-packet-land.cloudfunctions.net/apiVan/people?q=${vanId}`
            }).then(resp => {
                // console.info(resp)

                const phones = resp.data.phones
                let prefPhone

                phones.map(item => {
                    item.isPreferred ? prefPhone = item.phoneNumber : null
                })


                const divEl = document.getElementsByClassName('phone-' + vanId)

                // console.log(divEl)
                const divArr = [...divEl]

                divArr.map(item => {

                    prefPhone ? item.innerHTML = `<a href='tel:${prefPhone}'>${prefPhone.replace(/[^\d]+/g, '').replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3')}</a>` : item.innerHTML = `<div class=${classes.noPhone}>No Phone#</div>`

                })


            }).catch(err => {
                console.log(err)
            })
        })

    }




    render() {

        if (this.props.volunteers && this.props.volunteers.length > 0) {

            let previousVol

            let sortedVols = this.state.allVols // this.props.volunteers

            // sortedVols = sortedVols.reverse()
            sortedVols.sort(function (a, b) {

                const personA = a.person.lastName + a.person.firstName
                const personB = b.person.lastName + b.person.firstName
                if (personA === personB) {
                    return a.startTimeOverride < b.startTimeOverride ? -1 : 0
                }
                return personA > personB ? 1 : personB > personA ? -1 : 0
            })



            return (
                <div>


                    <table className={classes.shiftVolsTable}>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th />
                                <th>Day</th>
                                {/* <th>Phone</th> */}
                                <th>Status</th>

                            </tr>

                        </thead>
                        <tbody>

                            {sortedVols.map((item, idx) => {

                                // FORMAT TIME
                                const timeStart = item.startTimeOverride
                                let timeFormat = new Date(timeStart)
                                const formatOptions = {
                                    timeZone: 'America/New_York',
                                    month: 'short',
                                    day: 'numeric',
                                    hour: '2-digit',
                                    minute: '2-digit',
                                    hour12: true
                                }
                                let finalTime = timeFormat.toLocaleTimeString('en-US', formatOptions).replace(":00 ", "")
                                let timeSplit = finalTime.split(',')
                                let dayVal = timeSplit[0]
                                let shiftVal = timeSplit[1]

                                let volName = `${item.person.lastName}, ${item.person.firstName}`
                                let thisVol = item.person.lastName + item.person.firstName
                                let sameVol = previousVol === thisVol
                                let rowClass = sameVol ? classes.samePerson : classes.diffPerson


                                previousVol = item.person.lastName + item.person.firstName


                                return (
                                    <tr key={`vol-${idx}`} className={rowClass}>
                                        <td>{sameVol ? '' : <b>{volName}</b>}
                                            {sameVol ? '' : <div className={`phone-${item.person.vanId}`} onClick={this.getPhone(item.person.vanId)}><u className={classes.getPhone}>Get Phone</u></div>}
                                        </td>
                                        <td>
                                            {item.role.name === 'Phonebanker' ? <CallIcon style={{ fontSize: '16px' }} opacity={0.5} /> : <DirectionsWalkIcon style={{ fontSize: '16px' }} opacity={0.5} />}

                                        </td>
                                        <td>{`${dayVal}`}</td>

                                        {/*   <td className={classes.tdPhone}>
                                            <div className={`phone-${item.person.vanId}`} onClick={this.getPhone(item.person.vanId)}>Get Phone</div>
                                        </td> */}
                                        <td>
                                            <FormControl>
                                                <InputLabel margin='dense' shrink={true} htmlFor={`select-${item.eventSignupId}`}>
                                                    {shiftVal}
                                                </InputLabel>
                                                <NativeSelect
                                                    style={{ marginTop: '6px' }}

                                                    id={`select-${item.eventSignupId}`}
                                                    onChange={this.handleChangeStatus(item.eventSignupId, finalTime)}
                                                    value={this.state.selectStatus[item.eventSignupId]}>
                                                    <option value="1">Scheduled</option>
                                                    <option value="11">Confirmed</option>
                                                    <option value="23">Conf Twice</option>
                                                    <option value="3">Canceled</option>
                                                    <option value="6">No Show</option>
                                                </NativeSelect>
                                            </FormControl>
                                        </td>

                                    </tr>
                                )




                            }

                            )}

                        </tbody>
                    </table>
                </div>
            )
        } else {
            return (<div>Retrieving Volunteer List...</div>)
        }




    }

}

export default AllVols


//  <td>{item.role.name}</td>

// {item.role.name.substring(0, 1)}