import { compose } from 'redux'
import { connect } from 'react-redux'
import { CANVASS_PATH } from 'constants'

import { withHandlers, withStateHandlers, pure } from 'recompose'
import { firestoreConnect } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'

export default compose(
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  // Map auth uid from state to props
  // connect(({ firebase: { auth: { uid } } }) => ({ uid })),
  connect((store /* { firebase: { auth, profile } } */) => {
    // console.log(store)
    return {
      //  accessSite: store.accessSite,
      //    accessDash: store.accessDash,
      accessRole: store.accessRole,
      // auth: store.firebase.auth,
      uid: store.firebase.auth.uid,
      profile: store.firebase.profile
    }
  }),
  // Wait for uid to exist before going further
  // spinnerWhileLoading(['uid']),
  // Create listeners based on current users UID
  firestoreConnect(({ params, uid }) => [
    // Listener for projects the current user created
    /*   {
        collection: 'projects',
        where: ['createdBy', '==', uid]
      } */
  ]),
  // Map projects from state to props
  // connect(({ firestore: { ordered } }) => ({
  //   projects: ordered.projects
  // })),
  // Show loading spinner while projects and collabProjects are loading
  // spinnerWhileLoading(['projects']),
  // Add props.router
  withRouter,
  // Add props.showError and props.showSuccess
  withNotifications,
  // Add state and state handlers as props
  withStateHandlers(
    // Setup initial state
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    // Add state handlers as props
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),
  // Add handlers as props
  withHandlers({
    getAnyData: props => data => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props
      return (
        firestore
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` })
          // .get({ collection: `governor/ky_beshear/canvass`, doc: `adair` })
          .get({ collection: `kentucky/gov_beshear/canvass`, doc: `adair` })
          // .get({ collection: `governor/`, doc: `ky_gov` })
          .then(doc => {
            console.log(doc.data())
            // console.log('data has been received')
            /*   if (doc.exists) {
              const dbObj = doc.data()
              // console.log(doc)
              // console.log(dbObj[data.station])
              if (dbObj[data.station]) {
                return dbObj[data.station]
              } else {
                return undefined
              }
  
              // return doc.data()
            } else {
              // doc.data() will be undefined in this case
              // console.log("No such document!");
            } */
          })
          .catch(err => {
            console.error('Error:', err) // eslint-disable-line no-console
            showError(err.message || 'Could not get vol data')
            return Promise.reject(err)
          })
      )
    },
    clickStation: ({ router }) => (countyName, stationName) => {
      router.push(`${CANVASS_PATH}/${countyName}-${stationName}`)
    }
  }),
  pure // shallow equals comparison on props (prevent unessesary re-renders)
)
