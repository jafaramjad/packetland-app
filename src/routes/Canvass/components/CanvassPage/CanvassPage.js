import React, { Component, cloneElement } from 'react'
// import PropTypes from 'prop-types'
// import { isEmpty } from 'react-redux-firebase'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import classes from './CanvassPage.scss'

class CanvassPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hasRole: '',
      dataEvents: [],
      showLoader: true,
      value: 0
    }
  }

  componentDidMount() {
    // this.queryAPI()

    this.props.firebase
      .auth()
      .currentUser.getIdTokenResult()
      .then(idTokenResult => {
        console.log(idTokenResult)

        let hasRole

        if (idTokenResult.claims.role) {
          hasRole = idTokenResult.claims.role[0]
        } else {
          if (idTokenResult.claims.admin === true) {
            hasRole = 'admin'
          } else {
            hasRole = false
          }
        }

        this.setState(
          {
            hasRole
          },
          () => {
            this.props.getAnyData()
            // this.queryAPI()
          }
        )

        // console.log(hasRole)
      })
      .catch(error => {
        console.log(error)
      })
  }

  // QUERY
  queryAPI = () => {
    const token = this.props.firebase.auth().currentUser.getIdToken()

    /*
		token.then(tok => {
	
			axios({
				method: 'get',
				headers: {
					'X-Requested-With': 'XMLHttpRequest',
					'Authorization': 'Bearer' + ' ' + tok
				},
				// url: `http://localhost:3000/van/events?q=`
				// url: `https://us-central1-packet-land.cloudfunctions.net/apiVan/signups?q=${this.state.eventId}`
			}).then(resp => {
				// console.info(resp.data.items)
	
			
	
				// this.setState({
				// 	loaderModal: false,
				// 	signupsAll: resp.data.items,
				// 	signups: signups,
				// 	volShiftSlots: volShiftSlots
				// })
			})
		}).catch(err => {
			// this.setState({
			// 	loaderModal: false
			// })
			console.log(err)
		})
		*/
  }

  render() {
    return this.props.children ? (
      <div>
        {cloneElement(this.props.children, {
          firebase: this.props.firebase,
          vanEvents: this.state.vanEvents
        })}
      </div>
    ) : (
      <div>
        {this.props.accessRole === 'admin' ||
        this.props.accessRole === 'staff' ? (
          <Grid container className={classes.volsGrid}>
            <Grid item xs={12}>
              <Typography variant="title">CANVASS</Typography>
            </Grid>
          </Grid>
        ) : (
          <div>No Access</div>
        )}
      </div>
    )
  }
}

// VolsPage.propTypes = {
// 	children: PropTypes.object // from react-router
// }

export default CanvassPage

/* clickDay={this.props.clickDay}
					clickStation={this.props.clickStation} */
