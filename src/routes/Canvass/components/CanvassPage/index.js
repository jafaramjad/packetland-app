import CanvassPage from './CanvassPage'
import enhance from './CanvassPage.enhancer'

export default enhance(CanvassPage)
