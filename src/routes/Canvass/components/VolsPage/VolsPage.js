import React, { Component, cloneElement } from 'react'
// import PropTypes from 'prop-types'
// import { isEmpty } from 'react-redux-firebase'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import classes from './VolsPage.scss'
import Locations from './Locations'
import { vanEvents } from '../../../../constants/events'

class VolsPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      vanEvents: vanEvents,
      hasRole: '',
      dataEvents: [],
      showLoader: true,
      value: 0
    }
  }

  componentDidMount() {
    // this.queryAPI()
    /*
				this.props.firebase.auth().currentUser.getIdTokenResult()
					.then((idTokenResult) => {
		
						let hasRole
		
						if (idTokenResult.claims.role) {
							hasRole = idTokenResult.claims.role[0]
						} else {
							if (idTokenResult.claims.admin === true) {
								hasRole = 'admin'
							} else {
								hasRole = false
							}
		
						}
		
						this.setState({
							hasRole
						}, () => {
							// this.queryAPI()
						})
		
						// console.log(hasRole)
		
					})
					.catch(error => {
						console.log(error)
					})
		
		*/
  }

  render() {
    return this.props.children ? (
      <div>
        {cloneElement(this.props.children, {
          firebase: this.props.firebase,
          vanEvents: this.state.vanEvents
        })}
      </div>
    ) : (
      <div>
        {this.props.accessRole === 'admin' ||
        this.props.accessRole === 'staff' ? (
          <Grid container className={classes.volsGrid}>
            <Grid item xs={12}>
              <Typography variant="title">VOLS</Typography>
            </Grid>
            <Grid item xs={12}>
              <Locations
                hasRole={this.state.hasRole}
                clickStation={this.props.clickStation}
              />
            </Grid>
          </Grid>
        ) : (
          <div>No Access</div>
        )}
      </div>
    )
  }
}

// VolsPage.propTypes = {
// 	children: PropTypes.object // from react-router
// }

export default VolsPage

/* clickDay={this.props.clickDay}
						clickStation={this.props.clickStation} */
