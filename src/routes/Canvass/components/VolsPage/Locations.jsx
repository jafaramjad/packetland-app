import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classes from './VolsPage.scss'
import Stations from './Stations'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline'
import Collapse from '@material-ui/core/Collapse'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'
import ExpandMore from '@material-ui/icons/ExpandMore'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'


import { stations, locations } from '../../../../constants/places'
import { Typography } from '@material-ui/core'


class Locations extends Component {
	constructor(props) {
		super(props)
		this.state = {
			open: {
				0: false,
				1: false,
				2: false,
				3: false,
				4: false,
				5: false,
				6: false,
				7: false,
				8: false
			}
		}

		// console.log(this.props)
	}

	handleClick = idx => {
		const newOpen = [...this.state.open]
		newOpen[idx] = !newOpen[idx]

		this.setState({
			open: newOpen
		})
	}
	convertEpoch(epoch) {
		let d = new Date(0)
		return d.setUTCSeconds(epoch)
	}

	render() {
		return (
			<List className={classes.listCounties} component="ul">
				{locations.map((item, idx) => (
					<div key={idx}>
						<ListItem
							divider
							button
							onClick={() => {
								this.handleClick(idx)
							}}>
							<ListItemIcon>
								{this.state.open[idx] ? <RemoveCircleOutlineIcon /> : <AddCircleOutlineIcon />}
							</ListItemIcon>
							<ListItemText
								inset
								primary={item + " County"}
							/>
						</ListItem>
						<Collapse in={this.state.open[idx]} timeout="auto" unmountOnExit>
							<Stations
								location={item}
								stationList={stations[item]}
								// item={item}
								county={item}
								// clickDay={this.props.clickDay}
								clickStation={this.props.clickStation}
							/>
						</Collapse>
					</div>
				))}
			</List>
		)

	}
}

Locations.propTypes = {
	locations: PropTypes.array, // from react-router
	clickDay: PropTypes.func, // from enhancer (withHandlers - router)
	clickStation: PropTypes.func
}

export default Locations
