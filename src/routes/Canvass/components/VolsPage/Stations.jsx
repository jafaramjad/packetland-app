import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classes from './VolsPage.scss'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'

class Stations extends Component {
    constructor(props) {
        super(props)

        // console.log(props)

        // let openList = props.stationList.map((station, idx) => {
        //     return false
        // })

        // this.state = {
        //     open: openList
        // }
    }

    // handleClick = idx => {
    //     const newOpen = [...this.state.open]
    //     newOpen[idx] = !newOpen[idx]
    //     this.setState({
    //         open: newOpen
    //     })
    // }

    render() {
        return (
            <List className={classes.listStations} component="ul">
                {this.props.stationList.map((stationName, idx) => (
                    <ListItem key={idx} button onClick={() => {
                        this.props.clickStation(this.props.county.toLowerCase(), stationName.replace(" ", "_").toLowerCase())
                    }}>
                        <ListItemIcon><NavigateNextIcon /></ListItemIcon>
                        <ListItemText inset primary={stationName} />
                    </ListItem>
                ))}
            </List>
        )
    }
}

Stations.propTypes = {
    locations: PropTypes.array, // from react-router
    // clickDay: PropTypes.func, // from enhancer (withHandlers - router)
    clickStation: PropTypes.func,
    stationList: PropTypes.array
}

export default Stations
