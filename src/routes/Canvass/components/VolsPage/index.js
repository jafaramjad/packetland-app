import VolsPage from './VolsPage'
import enhance from './VolsPage.enhancer'

export default enhance(VolsPage)
