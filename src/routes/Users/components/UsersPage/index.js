import UsersPage from './UsersPage'
import enhance from './UsersPage.enhancer'

export default enhance(UsersPage)
