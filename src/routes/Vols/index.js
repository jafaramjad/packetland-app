import { VOLS_PATH as path } from 'constants'

export default store => ({
  path,
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const VolsPage = require('./components/VolsPage').default
        // const API = require('./routes/Project').default;

        /*  Return getComponent   */
        cb(null, VolsPage)

        /* Webpack named bundle   */
      },
      'Vols'
    )
  },
  getChildRoutes(partialNextState, cb) {
    require.ensure([], require => {
      // const TWITTER_API = require('./routes/Project').default;
      const VolsList = require('./routes/VolsList').default

      cb(null, [VolsList(store)])
    })
  }
})
