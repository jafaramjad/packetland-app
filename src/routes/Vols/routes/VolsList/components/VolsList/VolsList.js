import React, { Component } from 'react'
import axios from 'axios'
import classes from './VolsList.scss'
import AllVols from './AllVols'
import Paper from '@material-ui/core/Paper'
import LinearProgress from '@material-ui/core/LinearProgress'
import Grid from '@material-ui/core/Grid'
import { Typography } from '@material-ui/core'
import { STATION_MAP } from '../../../../../../constants/places'

// const VolsList = ({ params }) => <div>Vols List Render {params.apiName}</div>

class VolsList extends Component {
  constructor(props) {
    super(props)

    const location = this.props.params.volsList.split('-')
    const county = location[0]
    const station = location[1]

    this.state = {
      showLoader: true,
      county: county,
      station: station,
      volStatusUpdates: undefined
    }
    // console.log(props)
  }

  componentWillMount() {
    this.props.accessRole === 'admin' || this.props.accessRole === 'staff'
      ? this.getAllVanEvents()
      : null
  }

  componentDidUpdate(prevProps) {
    if (this.props.accessRole !== prevProps.accessRole) {
      this.props.accessRole === 'admin' || this.props.accessRole === 'staff'
        ? this.getAllVanEvents()
        : null
    }
  }

  getEvent = (eventId, tok) => {
    return axios({
      method: 'get',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        Authorization: 'Bearer' + ' ' + tok
      },
      url: `https://us-central1-packet-land.cloudfunctions.net/apiVan/signups?q=${eventId}`
    })
  }

  getAllVanEvents = () => {
    const vanEvents = this.props.vanEvents

    // console.log(vanEvents[0].eventId)

    const token = this.props.firebase.auth().currentUser.getIdToken()

    token.then(tok => {
      axios
        .all([
          this.getEvent(vanEvents[0], tok), // NOV 3
          this.getEvent(vanEvents[1], tok), // NOV 4
          this.getEvent(vanEvents[2], tok), // NOV 5
          this.getEvent(vanEvents[3], tok) // NOV 6
        ])
        .then(
          axios.spread((evt1, evt2, evt3, evt4) => {
            const day1Events = evt1.data.items
            const day2Events = evt2.data.items
            const day3Events = evt3.data.items
            const day4Events = evt4.data.items

            const allEvents = day1Events.concat(
              day2Events,
              day3Events,
              day4Events
            )

            this.setState(
              {
                // showLoader: false,
                allEvents: allEvents,
                dayEvents: [day1Events, day2Events, day3Events, day4Events]
              },
              () => {
                // 	console.log(this.state)
                this.processEvents(allEvents)
              }
            )
          })
        )

      // END THEN TOKEN
    })
  }

  getVolDataQuery = () => {
    const getQuery = {
      county: this.state.county.toUpperCase(),
      station: this.state.station.toUpperCase()
      // day: this.state.dayName
    }

    const queryVolsData = this.props.getAllVolData(getQuery)

    queryVolsData.then(res => {
      // 	console.log(res)
      if (res !== undefined) {
        this.setState({
          // [panel]: expandBool,
          volStatusUpdates: res
        })
      }
    })
  }

  processEvents = eventList => {
    const list = eventList
    const county = this.state.county.toUpperCase()
    const station = this.state.station
      .replace(' ', '_')
      .replace('(', '')
      .replace(')', '')
      .toUpperCase()
    const stagingLocationName = STATION_MAP[county][station]
    // console.log(stagingLocationName)
    let signups = []
    list.map(item => {
      if (
        item.status.name === 'Scheduled' ||
        item.status.name === 'Confirmed' ||
        item.status.name === 'Completed'
      ) {
        if (
          item.role.name === 'Canvasser' ||
          item.role.name === 'Phonebanker'
        ) {
          item.location.name === stagingLocationName ? signups.push(item) : null
        }
      }
    })
    // console.log(signups)

    // SORT VOL SHIFTS

    let volShiftSlots = {
      '7AM': [],
      '9AM': [],
      '12PM': [],
      '3PM': [],
      '6PM': [],
      '9PM': []
    }

    signups.map(item => {
      const timeStart = item.startTimeOverride
      // console.log(timeStart)
      // yyyy-MM-ddTHH:mm:ssZ
      let timeFormat = new Date(timeStart)
      const formatOptions = {
        timeZone: 'America/New_York',
        hour: '2-digit',
        minute: '2-digit',
        hour12: true
      }
      // const timeFormat = moment(timeStart, "yyyy-MM-dd[T]HH:mm:ssZ").format('HH:mm a')
      // console.log(timeFormat)
      let finalTime = timeFormat
        .toLocaleTimeString('en-US', formatOptions)
        .replace(':00 ', '')

      switch (finalTime) {
        case '6PM':
          volShiftSlots['6PM'].push(item)
          break
        case '3PM':
          volShiftSlots['3PM'].push(item)
          break
        case '12PM':
          volShiftSlots['12PM'].push(item)
          break
        case '9AM':
          volShiftSlots['9AM'].push(item)
          break
        case '6AM':
          volShiftSlots['7AM'].push(item)
          break
        default:
          break
      }

      // console.log(finalTime)
      /* 	switch (finalTime) {
					case '6AM':
						volShiftSlots['7AM'].push(item)
						break
					case '9AM':
						volShiftSlots['9AM'].push(item)
						break
					case '12PM':
						volShiftSlots['12PM'].push(item)
						break
					case '3PM':
						volShiftSlots['3PM'].push(item)
						break
					case '6PM':
						volShiftSlots['6PM'].push(item)
						break
					default:
						break
				} */
      // console.log(volShiftSlots)
    })

    this.setState(
      {
        showLoader: false,
        // signupsAll: resp.data.items,
        signups: signups,
        volShiftSlots: volShiftSlots
      },
      () => {
        // console.log(this.state)
        this.getVolDataQuery()
      }
    )
  }

  render() {
    return this.props.accessRole === 'admin' ||
      this.props.accessRole === 'staff' ? (
      <Grid container alignItems="center" justify="center">
        <Grid item>
          <div className={classes.volsTitle}>
            <Typography variant="title">{`${this.state.county.toUpperCase()} - ${this.state.station
              .replace('_', ' ')
              .toUpperCase()}`}</Typography>
            <Typography variant="subheading">VOLUNTEERS</Typography>
          </div>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.volsList} style={{ overflow: 'auto' }}>
            {this.state.showLoader ? (
              <LinearProgress
                color="secondary"
                className={classes.progress}
                size={50}
              />
            ) : null}

            <AllVols
              volStatusUpdates={this.state.volStatusUpdates}
              // getVolData={this.props.getVolData}
              addVolData={this.props.addVolData}
              firebase={this.props.firebase}
              volunteers={this.state.signups}
              location={this.state.county}
              station={this.state.station}
            />
            <br />
          </Paper>
          <br />
          <br />
        </Grid>
      </Grid>
    ) : (
      <div>No access...</div>
    )
  }
}

export default VolsList
