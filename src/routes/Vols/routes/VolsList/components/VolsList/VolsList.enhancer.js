import { compose } from 'redux'
import { connect } from 'react-redux'
import { get } from 'lodash'
import { firestoreConnect } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'

import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'
import { withHandlers } from 'recompose'

export default compose(
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  // connect(({ firebase: { auth: { uid } } }) => ({ uid })),
  connect((store /* { firebase: { auth, profile } } */) => {
    // console.log(store)
    return {
      //  accessSite: store.accessSite,
      //    accessDash: store.accessDash,
      accessRole: store.accessRole,
      // auth: store.firebase.auth,
      uid: store.firebase.auth.uid,
      profile: store.firebase.profile
    }
  }),
  // Create listeners based on current users UID
  firestoreConnect(({ params }) => [
    //   // Listener for projects the current user created
    /* 	{
				collection: 'projects',
				doc: '4sjDotrpFJSHFhoB69sf'
			} */
  ]),
  // // Map projects from state to props
  // connect(({ firestore: { data } }, { params }) => ({
  // 	project: get(data, `projects.${'4sjDotrpFJSHFhoB69sf'}`)
  // })),
  // Show loading spinner while project is loading
  // spinnerWhileLoading(['project'])
  // Add props.showError and props.showSuccess
  withNotifications,
  withHandlers({
    addVolData: props => (newData, successText) => {
      const { firestore, uid, showError, showSuccess, toggleDialog } = props
      // console.log(props)
      // console.log(newData)

      const locationName = newData.info.locationName
      const stationName = newData.info.stationName
      const dayName = newData.info.dayName

      if (!uid) {
        return console.error('You must be logged in to create a project')
      }
      return firestore
        .set(
          // { collection: `gotv/${locationName}/${stationName}`, doc: dayName },
          { collection: `volunteers/`, doc: locationName },
          {
            [stationName]: {
              [dayName]: {
                ...newData,
                createdBy: uid,
                createdAt: firestore.FieldValue.serverTimestamp()
              }
            }
          },
          { merge: true }
        )
        .then(() => {
          // toggleDialog()
          showSuccess(successText)
        })
        .catch(err => {
          console.error('Error:', err) // eslint-disable-line no-console
          showError(err.message || 'Could not add data')
          return Promise.reject(err)
        })
    },
    getAllVolData: props => data => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props
      return (
        firestore
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` })
          .get({ collection: `volunteers/`, doc: `${data.county}` })
          .then(doc => {
            // console.log(doc.data())
            // console.log('data has been received')
            if (doc.exists) {
              const dbObj = doc.data()
              // console.log(doc)
              // console.log(dbObj[data.station])
              if (dbObj[data.station]) {
                return dbObj[data.station]
              } else {
                return undefined
              }

              // return doc.data()
            } else {
              // doc.data() will be undefined in this case
              // console.log("No such document!");
            }
          })
          .catch(err => {
            console.error('Error:', err) // eslint-disable-line no-console
            showError(err.message || 'Could not get vol data')
            return Promise.reject(err)
          })
      )
    }
  })
)
