import ScoutPage from './ScoutPage'
import enhance from './ScoutPage.enhancer'

export default enhance(ScoutPage)
