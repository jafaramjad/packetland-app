import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import classes from './ScoutPage.scss'

import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import LinearProgress from '@material-ui/core/LinearProgress'

class ScoutPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showLoader: false,
      screenName: 'onetermtenney',
      twitterId: '',
      followerList: []
    }
  }

  // QUERY TWITTER NAME
  queryTwitterName = query => {
    axios({
      method: 'get',
      url: `http://localhost:3000/twitter/name?q=${query}`
    }).then(resp => {
      // console.log(resp)
      if (resp.data.statusCode !== 403) {
        const twitterId = resp.data[0].id_str
        this.setState(
          {
            twitterId
          },
          () => {
            this.queryTwitterFollowers(twitterId)
          }
        )
      } else {
        console.log(resp.data.errors)
      }
    })
  }

  // QUERY TWITTER FOLLOWERS
  queryTwitterFollowers = query => {
    // console.log(query)
    axios({
      method: 'get',
      url: `http://localhost:3000/twitter/followers?q=${query}`
    }).then(resp => {
      // console.log(resp)
      const respList = resp.data.ids

      let followerList = []

      respList.map(item => {
        String(item).length < 12 ? followerList.push(item) : null
      })

      const listIds = followerList.toString()
      // console.log(listIds)

      this.queryTwitterUserId(listIds)
    })
  }

  queryTwitterUserId = query => {
    axios({
      method: 'get',
      url: `http://localhost:3000/twitter/userid?q=${query}`
    }).then(resp => {
      console.log(resp)

      if (resp.data.statusCode !== 403) {
        this.setState(
          {
            showLoader: false,
            followerList: resp.data
          },
          () => {
            this.queryVanPeople()
          }
        )
      } else {
        this.setState({
          showLoader: false
        })

        console.log(resp.data.errors)
      }
    })
  }

  queryVanPeople = query => {
    axios({
      method: 'get',
      // url: `http://localhost:3000/van/signups?q=${28647}`
      url: `http://localhost:3000/van/people` // ?q=${query}`
    }).then(resp => {
      console.log(resp)
      this.setState({
        showLoader: false
      })
    })
  }

  handleChange = evt => {
    // console.log(evt.target.value)
    this.setState({
      screenName: evt.target.value
    })
  }

  clickBtn = evt => {
    console.log(evt)
    this.setState(
      {
        showLoader: true
      },
      () => {
        // this.queryTwitterName(this.state.screenName)
        this.queryVanPeople()
      }
    )
  }

  render() {
    return (
      <Paper className={classes.scoutPaper}>
        {this.state.showLoader ? (
          <LinearProgress
            color="secondary"
            className={classes.progress}
            size={50}
          />
        ) : null}
        <Grid
          container
          className={classes.scoutGrid}
          spacing={8}
          direction="row"
          alignItems="center"
          justify="center">
          <Grid item xs={12}>
            <Typography variant="title">Scout</Typography>
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="Screen Name(s)"
              className={classes.textField}
              value={this.state.screenName}
              onChange={this.handleChange}
              margin="normal"
            />

            <Button variant="contained" color="primary" onClick={this.clickBtn}>
              GO
            </Button>
          </Grid>
          <Grid item>
            <table>
              <tbody>
                {this.state.followerList.map((item, idx) => {
                  return (
                    <tr key={`follower-${idx}`}>
                      <td>{idx + 1}</td>
                      <td>{item.location}</td>
                      <td>{item.name}</td>
                      <td>@{item.screen_name}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </Grid>
        </Grid>
      </Paper>
    )
  }
}

ScoutPage.propTypes = {
  children: PropTypes.object // from react-router
}

export default ScoutPage
