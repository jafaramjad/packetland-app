import { SCOUT_PATH as path } from 'constants'

export default store => ({
  path,
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const ScoutPage = require('./components/ScoutPage').default
        // const API = require('./routes/Project').default;

        /*  Return getComponent   */
        cb(null, ScoutPage)

        /* Webpack named bundle   */
      },
      'API'
    )
  } /*,
	getChildRoutes(partialNextState, cb) {
		require.ensure([], (require) => {

			//const TWITTER_API = require('./routes/Project').default;
			const APIName = require('./routes/APIName').default;

		
			cb(null, [ APIName(store) ]);
		});
	} */
})
