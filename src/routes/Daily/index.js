import { DAILY_PATH as path } from 'constants'

export default store => ({
  path,
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const DailyPage = require('./components/DailyPage').default
        // const API = require('./routes/Project').default;

        /*  Return getComponent   */
        cb(null, DailyPage)

        /* Webpack named bundle   */
      },
      'Daily'
    )
  },
  getChildRoutes(partialNextState, cb) {
    require.ensure([], require => {
      const DailyEntry = require('./routes/DailyEntry').default

      cb(null, [DailyEntry(store)])
    })
  }
})

// require.ensure(
// 	dependencies: String[],
// 	callback: function(require),
// 	errorCallback: function(error),
// 	chunkName: String
//   )
