import React, { Component } from 'react'
import classes from './DailyEntry.scss'
import Fab from '@material-ui/core/Fab'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'

import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import noDataSam from 'static/sam.jpg'
import noDataRosie from 'static/rosie.jpg'

// import MUIDataTable from "mui-datatables";

import MaterialTable from 'material-table'

class DailyEntryList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      empty: true,
      loading: true,
      county: '',
      city: '',
      list: [],
      rawList: []
    }
    // console.log(props)
  }

  clickNew = props => event => {
    // console.log(event)
    // console.log(this)
    //	router.push(`${DAILY_PATH}/${countyName}/${stationName}/new`)
    // const county = this.props.params.county
    // const city = this.props.params.city
    const county = this.state.county
    const city = this.state.city
    this.props.clickNew(county, city)
  }
  clickEdit = id => {
    const county = this.state.county
    const city = this.state.city
    this.props.editById(county, city, id)
  }

  componentDidMount = () => {
    const county = this.props.params.county
    const city = this.props.params.city

    this.props.getList(county, city).then(doc => {
      // console.log(doc.docs)

      let tmpArr = []
      doc.docs.map(itm => {
        tmpArr = [...tmpArr, itm.data()]
      })

      let isEmpty = false
      if (tmpArr.length <= 0) {
        isEmpty = true
      }

      // console.log(tmpArr)
      // console.log(this.state)
      this.setState(
        {
          empty: isEmpty,
          loading: false,
          list: tmpArr,
          rawList: doc.docs,
          county: county,
          city: city
        },
        () => {
          // console.log(this.state)
          // console.log(this.state.list)
        }
      )
    })
    // .catch(err => {
    //   console.error('Error:', err) // eslint-disable-line no-console
    //   // showError(err.message || 'Could not get shift')
    //   // return Promise.reject(err)
    // })
  }

  uncleSam = () => {
    return (
      <div className={classes.noDataShell}>
        <h2 className={classes.noDataTitle}>No data has been added yet</h2>
        <img src={noDataSam} width={`240px`} />
        <h2 className={classes.noDataMsg}>Uncle Sam Wants You!</h2>
        <h3>To Enter the First Data</h3>
        <Button variant="contained" onClick={this.clickNew()}>
          <AddIcon />
          &nbsp;Start
        </Button>
      </div>
    )
  }

  rivetRosie = () => {
    return (
      <div className={classes.noDataShell}>
        <h2 className={classes.noDataTitle}>No data has been added yet</h2>
        <img src={noDataRosie} width={`240px`} />
        <h2 className={classes.noDataMsg}>You can do it!</h2>
        <h3>Be the first to Enter Data</h3>
        <Button variant="contained" onClick={this.clickNew()}>
          <AddIcon />
          &nbsp;Start
        </Button>
      </div>
    )
  }

  render() {
    const county =
      this.state.county.charAt(0).toUpperCase() + this.state.county.slice(1)
    const city =
      this.state.city.charAt(0).toUpperCase() + this.state.city.slice(1)
    return (
      <div className={classes.dailyListContainer}>
        {!this.state.loading ? (
          <h2>
            {county} - {city}
          </h2>
        ) : null}

        {this.state.empty ? (
          this.state.loading ? (
            <div>loading...</div>
          ) : (
              <Paper>
                {Math.floor(Math.random() * 2) == 0
                  ? this.rivetRosie()
                  : this.uncleSam()}
              </Paper>
            )
        ) : (
            <Paper>
              <MaterialTable
                options={{
                  toolbar: false,
                  paging: false
                }}
                columns={[
                  {
                    title: 'Date',
                    field: 'info.currentDate',
                    defaultSort: 'desc'
                  },
                  { title: 'User', field: 'info.createdByEmail' }
                ]}
                data={this.state.list}
                title="Daily Reports"
                onRowClick={(event, rowData) => {
                  this.clickEdit(rowData.info.cuid)
                }}
              />
            </Paper>
          )}

        {!this.state.empty ? (
          <Fab
            className={classes.DailyAddBtn}
            color={'primary'}
            onClick={this.clickNew()}>
            <AddIcon />
          </Fab>
        ) : null}
      </div>
    )
  }
}
export default DailyEntryList
