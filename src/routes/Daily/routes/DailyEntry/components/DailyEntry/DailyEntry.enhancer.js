import { compose } from 'redux'
import { connect } from 'react-redux'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { DAILY_PATH } from 'constants'
import { withNotifications } from 'modules/notification'

import { firestoreConnect } from 'react-redux-firebase'
import { withRouter } from 'utils/components'

import { UserIsAuthenticated } from 'utils/router'

export default compose(
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  connect(({ firebase: { auth: { uid } } }) => ({ uid })),
  // Create listeners based on current users UID
  firestoreConnect(({ params }) => [
    //   // Listener for projects the current user created
    /* 	{
				collection: 'gotv',
				doc: 'oneida'
			} */
  ]),
  // // Map projects from state to props
  /* 	connect(({ firestore: { data } }, { params }) => ({
			gotv: get(data, `gotv.oneida`)
			// gotv: ['thing'] // get(data) // get(data, `projects.${'4sjDotrpFJSHFhoB69sf'}`)
		})), */
  withRouter,
  withNotifications,
  withStateHandlers(
    // Setup initial state
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    // Add state handlers as props
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),

  // withHandlers({
  // 	goToNew: ({ router }) => shiftId => {
  // 		router.push(`${DAILY_PATH}/${shiftId}`)
  // 	}
  // }),
  // Show loading spinner while project is loading
  // spinnerWhileLoading(['project']),
  withHandlers({
    editById: ({ router }) => (countyName, stationName, id) => {
      // console.log('clicking new thing...')
      router.push(`${DAILY_PATH}/${countyName}/${stationName}/edit/${id}`)
    },
    clickNew: ({ router }) => (countyName, stationName) => {
      // console.log('clicking new thing...')
      router.push(`${DAILY_PATH}/${countyName}/${stationName}/new`)
    },
    getDailyDataList: props => (territory, location) => {
      // console.log(territory)
      // console.log(location)
      const { firestore, showError } = props
      return (
        firestore
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` })
          .get({
            collection: `NC09/MCCREADY/DAILY/${territory.toUpperCase()}/${location.toUpperCase()}`
          })
      )
      // .get({ collection: `NC09/MCCREADY/DAILY` })

      /*	 .then(doc => {
          console.log(doc.docs)
          // console.log(doc.data())
          // console.log('data has been received')
          if (doc.exists) {
            const dbObj = doc.docs // doc.data()
            // console.log(doc)
            // console.log(dbObj[data.station][data.day])
              // if (dbObj[data.station] && dbObj[data.station][data.day]) {
              // 	return dbObj[data.station][data.day]
              // } else {
              // 	return undefined
              // } 
  
          //	return dbObj
  
            // return doc.data()
          } else {
            // doc.data() will be undefined in this case
             console.log("No such document!");
          }
  
        })
        .catch(err => {
          console.error('Error:', err) // eslint-disable-line no-console
          showError(err.message || 'Could not get shift')
          return Promise.reject(err)
        }) */
    }
  }),
  pure
)
