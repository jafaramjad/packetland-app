import React, { Component } from 'react'
import axios from 'axios'
import swal from 'sweetalert2'

import classes from './DailyEntry.scss'

import { TOTAL_PACKETS } from '../../../../../../constants/packets'
import { STATION_MAP } from '../../../../../../constants/places'

import FormControlLabel from '@material-ui/core/FormControlLabel'
import NativeSelect from '@material-ui/core/NativeSelect'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
// import TextField from 'material-ui/TextField'

import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'

import SaveIcon from '@material-ui/icons/Save'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

class DailyEntryEditor extends Component {
  constructor(props) {
    super(props)

    this.state = {
      expandedForm: true,
      expandedFeedback: true,
      form: {
        doorsknocked: 0,
        callsattempted: 0,
        votersidentified: 0,
        votersregistered: 0,
        shiftsrecruited: 0,
        shiftscompleted: 0,
        vbmforms: 0,
        digitalvols: 0
      }
    }
  }

  handleChangeExpansion = panel => (event, expandBool) => {
    this.setState({
      [panel]: expandBool
    })
  }

  clickSave = event => {
    console.log(event)
    console.log(this.state)
  }

  handleChange = name => event => {
    console.log(name)
    console.log(this.state)
    console.log(this.state.form[name])

    let tmpObj = {
      ...this.state.form,
      [name]: Number(event.target.value)
    }

    this.setState({
      form: tmpObj
    })
    console.log(this.state)
  }

  render() {
    return (
      <div>
        <h2>Daily Tracker</h2>

        <ExpansionPanel
          expanded={this.state.expandedForm}
          onChange={this.handleChangeExpansion('expandedForm')}>
          <ExpansionPanelSummary
            style={{ borderBottom: '1px solid #dcdcdc' }}
            expandIcon={<ExpandMoreIcon />}>
            <Typography variant="title">Numbers</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Grid container spacing={16}>
              <Grid item xs={6} sm={3}>
                <TextField
                  className={classes.textField}
                  label="Doors Knocked"
                  type="number"
                  fullWidth
                  margin="normal"
                  value={this.state.form.doorsknocked}
                  onChange={this.handleChange('doorsknocked')}
                  variant="outlined"
                  helperText="# of doors knocked"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Calls Attempted"
                  value={this.state.form.callsattempted}
                  onChange={this.handleChange('callsattempted')}
                  type="number"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of calls attempted"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Voters ID'd"
                  value={this.state.form.votersidentified}
                  onChange={this.handleChange('votersidentified')}
                  type="number"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of voters identified"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Voters Registered"
                  value={this.state.form.votersregistered}
                  onChange={this.handleChange('votersregistered')}
                  type="number"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of voters registered"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Shifts Recruited"
                  value={this.state.form.shiftsrecruited}
                  onChange={this.handleChange('shiftsrecruited')}
                  type="number"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of shifts recruited"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Shifts Completed"
                  value={this.state.form.shiftscompleted}
                  onChange={this.handleChange('shiftscompleted')}
                  type="number"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of shifts completed"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="VBM Forms"
                  value={this.state.form.vbmforms}
                  onChange={this.handleChange('vbmforms')}
                  type="number"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of VBM forms"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Digital Vols"
                  value={this.state.form.digitalvols}
                  onChange={this.handleChange('digitalvols')}
                  type="number"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of digital vols recruited"
                />
              </Grid>
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <ExpansionPanel
          expanded={this.state.expandedFeedback}
          onChange={this.handleChangeExpansion('expandedFeedback')}>
          <ExpansionPanelSummary
            style={{ borderBottom: '1px solid #dcdcdc' }}
            expandIcon={<ExpandMoreIcon />}>
            <Typography variant="title">Feedback</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <TextField
                  label="What Constituency outreach efforts did you have today?"
                  multiline
                  variant="outlined"
                  fullWidth
                  rows={4}
                  rowsMax={7}
                  margin="normal"
                  helperText="Mention any interaction you had with the people"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  label="What was a high for the day?"
                  type="text"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="Mention something good that happened"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  label="What was a low for the day?"
                  type="text"
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="Mention anything you didn't like"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="What was a problem you faced today?"
                  multiline
                  variant="outlined"
                  fullWidth
                  rows={4}
                  rowsMax={7}
                  margin="normal"
                  helperText="Mention any problems or issues you faced"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="What was the solution to that problem?"
                  multiline
                  variant="outlined"
                  fullWidth
                  rows={4}
                  rowsMax={7}
                  margin="normal"
                  helperText="Mention anything that helped you"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="Do you have any best practices to share with the group?"
                  multiline
                  variant="outlined"
                  fullWidth
                  rows={4}
                  rowsMax={7}
                  margin="normal"
                  helperText="Mention anything you think could help the group"
                />
              </Grid>
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <br />
        <Grid container spacing={16} justify="center" alignItems="center">
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              onClick={evt => {
                this.clickSave(evt)
              }}
              size="large">
              Save &nbsp;
              <SaveIcon />
            </Button>
          </Grid>
        </Grid>
        <br />
        <br />
        <br />
      </div>
    )
  }
}
export default DailyEntryEditor

/*

Metrics

Quantitative

1-Doors Knocked:Number
2-Calls Attempted:Number
3-Voters ID’d:Number
4-Voters Registered:Number
5-Shifts Recruited:Number
6-Shifts Completed:Number
7-VBM Forms Collected:Number
8-Digital volunteers recruited:Number


Qualitative

What Constituency outreach efforts did you have today?:Textarea
What was a high for the day?:Input
What was a low for the day?:Input
What was a problem you faced today?:Input
What was the solution to that problem?:Textarea
Do you have any best practices to share with the group?:Textarea


*/
