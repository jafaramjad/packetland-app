import DailyEntry from './DailyEntry'
import enhance from './DailyEntry.enhancer'

export default enhance(DailyEntry)
