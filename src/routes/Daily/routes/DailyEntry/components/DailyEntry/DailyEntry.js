import React, { Component, cloneElement } from 'react'

import classes from './DailyEntry.scss'

import DailyEntryEditor from './DailyEntryEditor'
import DailyEntryList from './DailyEntryList'

// import { TOTAL_PACKETS } from '../../../../../../constants/packets'
// import { STATION_MAP } from '../../../../../../constants/places'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

class DailyEntry extends Component {
  constructor(props) {
    super(props)
    this.state = {
      // mode: props.params.mode
    }
  }

  render() {
    return this.props.children ? (
      <div>
        {cloneElement(this.props.children, { vanEvents: this.state.vanEvents })}
      </div>
    ) : (
      <DailyEntryList
        params={this.props.params}
        clickNew={this.props.clickNew}
        editById={this.props.editById}
        getList={this.props.getDailyDataList}
      />
    )
    // return (<DailyEntryList params={this.props.params} clickNew={this.props.clickNew} />)
    // return this.state.mode === 'list' ? (<DailyEntryList params={this.props.params} clickNew={this.props.clickNew} />) : (<DailyEntryEditor />)
  }
}
export default DailyEntry
