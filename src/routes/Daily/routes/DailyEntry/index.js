// import { DAILY_PATH as path } from 'constants'

export default store => ({
  path: ':county/:city',
  // path: ':dailyEntry',
  /*  Async getComponent is only invoked when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure(
      [],
      require => {
        /*  Webpack - use require callback to define
          dependencies for bundling   */
        const DAILY_ENTRY = require('./components/DailyEntry').default

        /*  Return getComponent   */
        cb(null, DAILY_ENTRY)

        /* Webpack named bundle   */
      },
      'DailyEntry'
    )
  },
  getChildRoutes(partialNextState, cb) {
    require.ensure([], require => {
      const DailyForm = require('./routes/DailyForm').default

      cb(null, [DailyForm(store)])
    })
  }
})
