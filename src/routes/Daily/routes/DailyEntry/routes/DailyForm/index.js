export default store => ({
  path: ':mode(/:id)',
  // path: ':dailyEntry',
  /*  Async getComponent is only invoked when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure(
      [],
      require => {
        /*  Webpack - use require callback to define
          dependencies for bundling   */
        const DAILY_FORM = require('./components/DailyFormPage').default

        /*  Return getComponent   */
        cb(null, DAILY_FORM)

        /* Webpack named bundle   */
      },
      'DailyFormPage'
    )
  }
})
