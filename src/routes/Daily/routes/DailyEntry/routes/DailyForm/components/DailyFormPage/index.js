import DailyFormPage from './DailyFormPage'
import enhance from './DailyFormPage.enhancer'

export default enhance(DailyFormPage)
