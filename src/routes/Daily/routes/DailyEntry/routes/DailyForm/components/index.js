import DailyFormPage from './DailyFormPage/DailyFormPage'
import enhance from './DailyFormPage/DailyFormPage.enhancer'

export default enhance(DailyFormPage)
