import React, { Component } from 'react'
import axios from 'axios'
import swal from 'sweetalert2'

import classes from './DailyFormPage.scss'


import FormControlLabel from '@material-ui/core/FormControlLabel'
import NativeSelect from '@material-ui/core/NativeSelect'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'

import SaveIcon from '@material-ui/icons/Save'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'




// https://stackoverflow.com/questions/49330139/date-toisostring-but-local-time-instead-of-utc
function toISOLocal(d) {
  var z = n => (n < 10 ? '0' : '') + n;
  var off = d.getTimezoneOffset();
  var sign = off < 0 ? '+' : '-';
  off = Math.abs(off);

  return d.getFullYear() + '-' + z(d.getMonth() + 1) + '-' +
    z(d.getDate()) + 'T' + z(d.getHours()) + ':' + z(d.getMinutes()) +
    ':' + z(d.getSeconds()) + sign + z(off / 60 | 0) + z(off % 60);
}


class DailyFormPage extends Component {
  constructor(props) {
    super(props)

    // console.log(props)
    this.state = {
      cuid: null,
      county: '',
      city: '',
      expandedForm: true,
      expandedFeedback: true,

      form: {
        info: {
          active: true,
          currentDate: toISOLocal(new Date()).substr(0, 10),
          createdAt: '',
          createdByEmail: '',
          createdByName: '',
          createdBy: ''
        },
        numbers: {
          doorsKnocked: '',
          callAttempts: '',
          votersID: '',
          votersRegistered: '',
          shiftsRecruited: '',
          shiftsCompleted: '',
          vbmForms: '',
          digitalVols: ''
        },
        feedback: {
          outreach: '',
          dayHigh: '',
          dayLow: '',
          problems: '',
          solutions: '',
          bestPractices: ''
        }
      }
    }

  }

  componentDidMount() {
    // console.log(this.props)

    if (this.props.params.mode === 'edit') {
      let data

      this.props.getDailyDataById(this.props.params).then(doc => {
        // console.log(doc)
        // console.log(doc.data())
        // console.log(doc.docs[0].data())

        if (doc.exists) {
          data = doc.data()
          // console.log(doc.data())

          this.setState({
            cuid: this.props.params.id,
            form: { ...data },
            county: this.props.params.county,
            city: this.props.params.city
          })
        } else {
          // console.log('no doc exists...')
        }
      })
    } else {
      this.setState({
        county: this.props.params.county,
        city: this.props.params.city
      })
    }
  }

  handleChangeExpansion = panel => (event, expandBool) => {
    this.setState({
      [panel]: expandBool
    })
  }

  handleChangeNumbers = name => evt => {
    this.setState({
      form: {
        info: {
          ...this.state.form.info
        },
        numbers: {
          ...this.state.form.numbers,
          [name]: Number(evt.target.value)
        },
        feedback: {
          ...this.state.form.feedback
        }
      }
    })
  }

  handleChangeText = name => evt => {
    // console.log(evt.target.value)
    // console.log(this.state.feedback)
    // console.log(name)
    this.setState({
      form: {
        info: {
          ...this.state.form.info
        },
        numbers: {
          ...this.state.form.numbers
        },
        feedback: {
          ...this.state.form.feedback,
          [name]: evt.target.value
        }
      }
    })
  }

  handleChangeInfo = name => evt => {
    this.setState({
      form: {
        info: {
          ...this.state.form.info,
          [name]: evt.target.value
        },
        numbers: {
          ...this.state.form.numbers
        },
        feedback: {
          ...this.state.form.feedback
        }
      }
    })
  }

  saveForm = shiftIdx => evt => {
    evt.preventDefault()

    // console.log(evt)
    // console.log(this.state)

    if (this.props.params.mode === 'edit') {
      this.props.enterDailyData(
        this.state.form,
        'Updated data successfully',
        this.successCB,
        this.state.cuid
      )
    } else {
      this.props.enterDailyData(
        this.state.form,
        'Added new data successfully',
        this.successCB,
        this.state.cuid
      )
    }
  }

  // SUCCESS NOTIFICATION
  successCB = (msg, uniqueID) => {
    swal(msg)
    this.setState({
      cuid: uniqueID
    })
  }

  render() {
    const county =
      this.state.county.charAt(0).toUpperCase() + this.state.county.slice(1)
    const city =
      this.state.city.charAt(0).toUpperCase() + this.state.city.slice(1)

    return (
      <div className={classes.dailyContainer}>
        <Grid container spacing={8}>
          <Grid item xs={6} sm={9}>
            <h2>Daily Report</h2>
            <h4>
              {county} - {city}
            </h4>
          </Grid>

          <Grid item xs={6} sm={3}>
            <TextField
              id="date"
              label="Date"
              margin="normal"
              variant="outlined"
              type="date"
              style={{ background: '#fff' }}
              required
              fullWidth
              onChange={this.handleChangeInfo('currentDate')}
              value={this.state.form.info.currentDate}
              InputLabelProps={{
                shrink: true
              }}
            />
          </Grid>
        </Grid>

        <ExpansionPanel
          expanded={this.state.expandedForm}
          onChange={this.handleChangeExpansion('expandedForm')}>
          <ExpansionPanelSummary
            style={{ borderBottom: '1px solid #dcdcdc' }}
            expandIcon={<ExpandMoreIcon />}>
            <Typography variant="h6">Numbers</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Grid container spacing={16}>
              <Grid item xs={6} sm={3}>
                <TextField
                  label="Doors Knocked"
                  type="number"
                  fullWidth
                  onChange={this.handleChangeNumbers('doorsKnocked')}
                  value={this.state.form.numbers.doorsKnocked}
                  margin="normal"
                  variant="outlined"
                  helperText="# of doors knocked"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Calls Attempted"
                  type="number"
                  onChange={this.handleChangeNumbers('callAttempts')}
                  value={this.state.form.numbers.callAttempts}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of calls attempted"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Voters ID'd"
                  type="number"
                  onChange={this.handleChangeNumbers('votersID')}
                  value={this.state.form.numbers.votersID}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of voters identified"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Voters Registered"
                  type="number"
                  onChange={this.handleChangeNumbers('votersRegistered')}
                  value={this.state.form.numbers.votersRegistered}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of voters registered"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Shifts Recruited"
                  type="number"
                  onChange={this.handleChangeNumbers('shiftsRecruited')}
                  value={this.state.form.numbers.shiftsRecruited}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of shifts recruited"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Shifts Completed"
                  type="number"
                  onChange={this.handleChangeNumbers('shiftsCompleted')}
                  value={this.state.form.numbers.shiftsCompleted}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of shifts completed"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="VBM Forms"
                  type="number"
                  onChange={this.handleChangeNumbers('vbmForms')}
                  value={this.state.form.numbers.vbmForms}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of VBM forms"
                />
              </Grid>

              <Grid item xs={6} sm={3}>
                <TextField
                  label="Digital Vols"
                  type="number"
                  onChange={this.handleChangeNumbers('digitalVols')}
                  value={this.state.form.numbers.digitalVols}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="# of digital vols recruited"
                />
              </Grid>
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel
          expanded={this.state.expandedFeedback}
          onChange={this.handleChangeExpansion('expandedFeedback')}>
          <ExpansionPanelSummary
            style={{ borderBottom: '1px solid #dcdcdc' }}
            expandIcon={<ExpandMoreIcon />}>
            <Typography variant="h6">Feedback</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Grid container spacing={16}>
              <Grid item xs={12}>



                <TextField
                  label="What Constituency outreach efforts did you have today?"
                  multiline
                  variant="outlined"
                  onChange={this.handleChangeText('outreach')}
                  value={this.state.form.feedback.outreach}
                  fullWidth
                  rows={12}
                  margin="normal"
                  helperText="Mention any interaction you had with the people"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  label="What was a high for the day?"
                  type="text"
                  onChange={this.handleChangeText('dayHigh')}
                  value={this.state.form.feedback.dayHigh}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="Mention something good that happened"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  label="What was a low for the day?"
                  type="text"
                  onChange={this.handleChangeText('dayLow')}
                  value={this.state.form.feedback.dayLow}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                  helperText="Mention anything you didn't like"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="What was a problem you faced today?"
                  multiline
                  variant="outlined"
                  onChange={this.handleChangeText('problems')}
                  value={this.state.form.feedback.problems}
                  fullWidth
                  rows={12}

                  margin="normal"
                  helperText="Mention any problems or issues you faced"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="What was the solution to that problem?"
                  multiline
                  variant="outlined"
                  onChange={this.handleChangeText('solutions')}
                  value={this.state.form.feedback.solutions}
                  fullWidth
                  rows={12}

                  margin="normal"
                  helperText="Mention anything that helped you"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label="Do you have any best practices to share with the group?"
                  multiline
                  variant="outlined"
                  onChange={this.handleChangeText('bestPractices')}
                  value={this.state.form.feedback.bestPractices}
                  fullWidth
                  rows={12}
                  margin="normal"
                  helperText="Mention anything you think could help the group"
                />
              </Grid>
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <br />
        <Grid container spacing={16} justify="center" alignItems="center">
          <Grid item>
            <Button
              onClick={this.saveForm()}
              variant="contained"
              color="secondary"
              size="large">
              Save &nbsp; <SaveIcon />
            </Button>
          </Grid>
        </Grid>
        <br />
        <br />
        <br />
      </div>
    )
  }
}
export default DailyFormPage

/*

Metrics

Quantitative

1-Doors Knocked:Number
2-Calls Attempted:Number
3-Voters ID’d:Number
4-Voters Registered:Number
5-Shifts Recruited:Number
6-Shifts Completed:Number
7-VBM Forms Collected:Number
8-Digital volunteers recruited:Number


Qualitative

What Constituency outreach efforts did you have today?:Textarea
What was a high for the day?:Input
What was a low for the day?:Input
What was a problem you faced today?:Input
What was the solution to that problem?:Textarea
Do you have any best practices to share with the group?:Textarea


*/
