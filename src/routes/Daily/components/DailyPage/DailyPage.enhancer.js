import { compose } from 'redux'
import { connect } from 'react-redux'
import { DAILY_PATH } from 'constants'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { firestoreConnect } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'

export default compose(
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  // Map auth uid from state to props
  connect(({ firebase: { auth: { uid } } }) => ({ uid })),
  // Wait for uid to exist before going further
  spinnerWhileLoading(['uid']),
  // Create listeners based on current users UID
  firestoreConnect(({ params, uid }) => [
    // Listener for projects the current user created
    /*     {
          collection: 'projects',
          where: ['createdBy', '==', uid]
        } */
  ]),
  // Map projects from state to props
  /*  connect(({ firestore: { ordered } }) => ({
     projects: ordered.projects
   })), */
  // Show loading spinner while projects and collabProjects are loading
  // spinnerWhileLoading(['GOTV']),
  // Add props.router
  withRouter,
  // Add props.showError and props.showSuccess
  withNotifications,
  // Add state and state handlers as props
  withStateHandlers(
    // Setup initial state
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    // Add state handlers as props
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),
  // Add handlers as props
  withHandlers({
    getPlaces: props => newInstance => {
      const { firestore, uid, showError, showSuccess, toggleDialog } = props
      if (!uid) {
        return console.error('You must be logged in')
      }

      return (
        firestore
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` })
          .get({ collection: `NC09/MCCREADY/CONSTANTS`, doc: `PLACES` })
      )
    },
    /* 
  deleteProject: props => projectId => {
    const { firestore, showError, showSuccess } = props
    return firestore
      .delete({ collection: 'projects', doc: projectId })
      .then(() => showSuccess('Project deleted successfully'))
      .catch(err => {
        console.error('Error:', err) // eslint-disable-line no-console
        showError(err.message || 'Could not delete project')
        return Promise.reject(err)
      })
  }, */
    clickStation: ({ router }) => (countyName, stationName) => {
      router.push(`${DAILY_PATH}/${countyName}-${stationName}`)
    },
    clickDay: ({ router }) => (countyName, stationName, eventId) => {
      // console.log('clicking thing...')
      router.push(`${DAILY_PATH}/${countyName}/${stationName}`)
    }
  }),
  pure // shallow equals comparison on props (prevent unessesary re-renders)
)
