import React, { Component, cloneElement } from 'react'
import PropTypes from 'prop-types'
import { isEmpty } from 'react-redux-firebase'
import axios from 'axios'
import classes from './DailyPage.scss'
// import uuid from 'uuid/v1'
import Locations from './Locations'

import Grid from '@material-ui/core/Grid'
import LinearProgress from '@material-ui/core/LinearProgress'

import SwipeableViews from 'react-swipeable-views'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
// import TotalPackets from './TotalPackets'
import { vanEvents } from '../../../../constants/events'

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  )
}

class DailyPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      vanEvents: vanEvents,
      hasRole: '',
      dataEvents: [],
      showLoader: true,
      value: 0
    }

    // console.log(this.props)
  }

  componentDidMount() {
    // this.queryAPI()

    this.props.firebase
      .auth()
      .currentUser.getIdTokenResult()
      .then(idTokenResult => {
        // console.log(idTokenResult)
        let hasRole

        if (idTokenResult.claims.role) {
          hasRole = idTokenResult.claims.role[0]
        } else {
          if (idTokenResult.claims.admin === true) {
            hasRole = 'admin'
          } else {
            hasRole = false
          }
        }

        this.setState(
          {
            hasRole
          },
          () => {
            // this.queryAPI()
          }
        )

        // console.log(hasRole)
      })
      .catch(error => {
        console.log(error)
      })
  }

  /* // QUERY
	queryAPI = () => {

		const token = this.props.firebase.auth().currentUser.getIdToken()

		token.then(tok => {

			// console.log(tok)
			// FORMAT yyyy-MM-dd
			axios({
				method: 'get',
				headers: {
					'X-Requested-With': 'XMLHttpRequest',
					'Authorization': 'Bearer' + ' ' + tok
				},
				// url: `http://localhost:3000/van/events?before=2018-11-07&after=2018-11-02`
				url: `https://us-central1-packet-land.cloudfunctions.net/apiVan/events?before=2018-11-07&after=2018-11-02`
			}).then(resp => {
				console.info(resp)
				this.setState({
					vanEvents: resp.data.items
				})
			})
		}).catch(err => {
			console.log(err)
		})
	} */

  handleChange = (event, value) => {
    this.setState({ value })
  }

  handleChangeIndex = index => {
    this.setState({ value: index })
  }

  /* generateList = locations => {
		return locations.map(location => <Location key={uuid()} location={location} />)
	} */

  /* 	<SingleStation info={this.props.params} eventData={this.state.dataEvents} />
   */
  render() {
    return this.props.children ? (
      <div>
        {cloneElement(this.props.children, { vanEvents: this.state.vanEvents })}
      </div>
    ) : (
        <Grid container spacing={16} className={classes.gotvGrid}>
          <Grid item xs={12}>
            <Typography variant="h6">Daily</Typography>
          </Grid>
          <Grid item xs={12}>
            <Locations

              getPlaces={this.props.getPlaces}
              hasRole={this.state.hasRole}
              clickDay={this.props.clickDay}
              clickStation={this.props.clickStation}
            />
          </Grid>
        </Grid>
      )
  }
}

/* 	<AppBar position="static" color="default">
					<Tabs
						value={this.state.value}
						onChange={this.handleChange}
						indicatorColor="primary"
						textColor="primary"
						fullWidth>
						<Tab label="Locations" />
						<Tab label="Daily" />
						<Tab label="Total" />
					</Tabs>
				</AppBar>
				<SwipeableViews
					disabled
					className={classes.swipeViews}
					axis={'x'}
					index={this.state.value}
					onChangeIndex={this.handleChangeIndex}>
					<TabContainer dir={'ltr'}>
						<Locations
							key={uuid()}
							clickDay={this.props.clickDay}
							clickStation={this.props.clickStation}

						/>
					</TabContainer>
					<TabContainer dir={'ltr'}>Item Two</TabContainer>
					<TabContainer dir={'ltr'}>Item Three</TabContainer>
				</SwipeableViews > 
				*/

DailyPage.propTypes = {
  children: PropTypes.object, // from react-router
  clickDay: PropTypes.func, // from enhancer (withHandlers - router)
  clickStation: PropTypes.func,
  getPlaces: PropTypes.func
}

export default DailyPage
