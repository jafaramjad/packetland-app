import DailyPage from './DailyPage'
import enhance from './DailyPage.enhancer'

export default enhance(DailyPage)
