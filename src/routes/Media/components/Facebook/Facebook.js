import React from 'react'
import Posts from './Posts'
import uuid from 'uuid/v1'
import Grid from '@material-ui/core/Grid'
import classes from './Facebook.scss'

const Facebook = ({ posts }) => {
  const generateList = posts => {
    return posts.map(post => <Posts key={uuid()} post={post} />)
  }

  return (
    <Grid container spacing={40} className={classes.cardGrid}>
      {generateList(posts)}
    </Grid>
  )
}

export default Facebook
