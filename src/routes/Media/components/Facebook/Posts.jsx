import React from 'react';
import moment from 'moment';
import classes from './Facebook.scss';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LaunchIcon from '@material-ui/icons/Launch';


const Posts = ({ post }) => (

	<Grid item key={post.title} xs={12} md={10}>

		<Card>
			<CardContent>
				<Grid container>
					<Grid item xs={11}>
						<Typography variant="subheading">{post.date}</Typography>
					</Grid>
					<Grid item xs={1}>
						<Typography><a href={'https://www.facebook.com' + post.link} target="_blank" ><LaunchIcon className={classes.fbLink} />LINK</a></Typography>
					</Grid>
				</Grid>
				<br />
				<Typography variant="headline">
					<div dangerouslySetInnerHTML={{ __html: post.item }} />
				</Typography>

			</CardContent>



		</Card>
	</Grid>
);

export default Posts;
