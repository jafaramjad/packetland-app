import MediaPage from './MediaPage'
import enhance from './MediaPage.enhancer'

export default enhance(MediaPage)
