import React, { Component } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import classes from './MediaPage.scss'

import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import ListSubheader from '@material-ui/core/ListSubheader'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
import LinearProgress from '@material-ui/core/LinearProgress'
import Hidden from '@material-ui/core/Hidden'
import Icon from '@mdi/react'
import {
  mdiTwitter,
  mdiYoutube,
  mdiNewspaper,
  mdiVote,
  mdiFacebook
} from '@mdi/js'

import Tweets from '../Twitter/Tweets'
import YouTube from '../YouTube/YouTube'
import VoteInfo from '../ProPublica/VoteInfo'
import Articles from '../News/Articles'
import Facebook from '../Facebook/Facebook'

// const NODE_ENV = process.env.NODE_ENV || 'development';

class MediaPage extends Component {
  constructor() {
    super()
    this.state = {
      dataTwitter: [],
      dataYouTube: [],
      dataProPublica: [],
      dataNews: [],
      dataFacebook: [],
      tabIndex: 0,
      showLoader: true
    }
    // INIT
    this.queryTwitter('abrindisiny', 0)
  }

  // FACEBOOK
  queryFacebook(username, tab) {
    axios({
      method: 'get',
      url: `https://us-central1-packet-land.cloudfunctions.net/apiFacebook/posts?q=${username}`
    }).then(response => {
      this.setState({
        dataFacebook: response.data,
        tabIndex: tab,
        showLoader: false
      })
    })
  }

  // FACEBOOK
  queryFacebook2(username1, username2, tab) {
    let urlReq = `https://us-central1-packet-land.cloudfunctions.net/apiFacebook/posts?q=`

    axios
      .all([axios.get(urlReq + username1), axios.get(urlReq + username2)])
      .then(
        axios.spread((resp1, resp2) => {
          const totalResp = resp1.data.concat(resp2.data)
          const totalSort = totalResp.sort(function(a, b) {
            return (
              new Date(a.date.replace('at', '')).getTime() -
              new Date(b.date.replace('at', '')).getTime()
            )
          })
          const totalSortReversed = totalSort.reverse()

          console.log(resp1)
          console.log(resp2)

          this.setState({
            dataFacebook: totalSortReversed,
            tabIndex: tab,
            showLoader: false
          })
        })
      )
  }

  // TWITTER
  queryTwitter(username, tab) {
    axios({
      method: 'get',
      url: `https://us-central1-packet-land.cloudfunctions.net/apiTwitter/timeline?q=${username}`
      // url: `/twitter?q=${username}`
      // params: {
      // 	q: username
      // }
    }).then(response => {
      this.setState({
        dataTwitter: response.data,
        tabIndex: tab,
        showLoader: false
      })
    })
  }

  // TWITTER
  queryTwitter2(username1, username2, tab) {
    const urlReq =
      'https://us-central1-packet-land.cloudfunctions.net/apiTwitter/timeline?q='
    axios
      .all([axios.get(urlReq + username1), axios.get(urlReq + username2)])
      .then(
        axios.spread((resp1, resp2) => {
          const totalResp = resp1.data.concat(resp2.data)
          const totalSort = totalResp.sort(function(a, b) {
            return (
              new Date(a.created_at).getTime() -
              new Date(b.created_at).getTime()
            )
          })
          const totalSortReversed = totalSort.reverse()

          // console.log(resp1)
          // console.log(resp2)
          // console.log(totalResp);
          // console.log(totalSortReversed)

          this.setState({
            dataTwitter: totalSortReversed,
            tabIndex: tab,
            showLoader: false
          })
        })
      )
  }
  /* 	//YOUTUBE CHANNEL
		queryYouTubeChannelAll(keywords, tab) {
	
	
			const urlReq = 'https://us-central1-packet-land.cloudfunctions.net/apiYoutube/channel?q='
			axios.all([
				axios.get(urlReq + keywords[0]),
				axios.get(urlReq + keywords[1]),
				axios.get(urlReq + keywords[2])
			]).then(axios.spread((resp1, resp2, resp3) => {
	
				const totalResp1 = resp1.data.concat(resp2.data)
				const totalSort1 = totalResp1.sort(function (a, b) { return new Date(a.snippet.publishedAt).getTime() - new Date(b.snippet.publishedAt).getTime() });
	
				const totalResp2 = totalSort1.concat(resp3.data)
				const totalSort2 = totalResp2.sort(function (a, b) { return new Date(a.snippet.publishedAt).getTime() - new Date(b.snippet.publishedAt).getTime() });
	
				const totalSortReversed = totalSort2.reverse();
	
				// console.log(resp1)
				// console.log(resp2)
				// console.log(resp3)
				// console.log(totalResp);
				// console.log(totalSortReversed)
	
				this.setState({ dataYouTube: totalSortReversed, tabIndex: tab, showLoader: false });
	
			}))
	
		} 
	*/

  // YOUTUBE CHANNEL
  queryYouTubeChannelAll(keywords, tab) {
    const urlReq =
      'https://us-central1-packet-land.cloudfunctions.net/apiYoutube/channel?q='
    const urlReqAndQuery =
      'https://us-central1-packet-land.cloudfunctions.net/apiYoutube/channelAndQuery?q=NY-22&channelId='

    axios
      .all([
        axios.get(urlReq + keywords[0]),
        axios.get(urlReqAndQuery + keywords[1]),
        axios.get(urlReqAndQuery + keywords[2])
      ])
      .then(
        axios.spread((resp1, resp2, resp3) => {
          const totalResp1 = resp1.data.concat(resp2.data)
          const totalSort1 = totalResp1.sort(function(a, b) {
            return (
              new Date(a.snippet.publishedAt).getTime() -
              new Date(b.snippet.publishedAt).getTime()
            )
          })

          const totalResp2 = totalSort1.concat(resp3.data)
          const totalSort2 = totalResp2.sort(function(a, b) {
            return (
              new Date(a.snippet.publishedAt).getTime() -
              new Date(b.snippet.publishedAt).getTime()
            )
          })

          const totalSortReversed = totalSort2.reverse()

          // console.log(resp1)
          // console.log(resp2)
          // console.log(resp3)
          // console.log(totalResp);
          // console.log(totalSortReversed)

          this.setState({
            dataYouTube: totalSortReversed,
            tabIndex: tab,
            showLoader: false
          })
        })
      )
  }

  // YOUTUBE
  queryYouTube(keyword, tab) {
    axios({
      method: 'get',
      url: `https://us-central1-packet-land.cloudfunctions.net/apiYoutube/search?q=${keyword}`
    }).then(response => {
      this.setState({
        dataYouTube: response.data,
        tabIndex: tab,
        showLoader: false
      })
    })
  }

  // YOUTUBE CHANNEL
  queryYouTubeChannel(keyword, tab) {
    axios({
      method: 'get',
      url: `https://us-central1-packet-land.cloudfunctions.net/apiYoutube/channel?q=${keyword}`
    }).then(response => {
      this.setState({
        dataYouTube: response.data,
        tabIndex: tab,
        showLoader: false
      })
    })
  }

  // YOUTUBE USER
  queryYouTubeUser(keyword, tab) {
    axios({
      method: 'get',
      url: `https://us-central1-packet-land.cloudfunctions.net/apiYoutube/user?q=${keyword}`
    }).then(response => {
      this.setState({
        dataYouTube: response.data,
        tabIndex: tab,
        showLoader: false
      })
    })
  }
  // NEWS
  queryNews(keyword, tab) {
    axios({
      method: 'get',
      url: `https://us-central1-packet-land.cloudfunctions.net/apiNews/search?q=${keyword}`
    }).then(response => {
      // console.log(response.data)

      /**
       * FIND DUPLICATE ARTICLES
       */
      const arr1 = response.data
      let arr2 = response.data
      let arr3 = []

      arr1.forEach((item1, idx1) => {
        arr2.forEach((item2, idx2) => {
          if (item2.title === item1.title) {
            // console.log('duplicate item at: ' + idx1)
            arr3.push(idx1)
          }
        })
      })

      // console.log(arr3)

      /**
       * IDENTIFY DUPLICATES
       */
      var sorted_arr = arr3.slice().sort()

      var results = []
      for (var i = 0; i < sorted_arr.length - 1; i++) {
        if (sorted_arr[i + 1] == sorted_arr[i]) {
          results.push(sorted_arr[i])
        }
      }
      results = results.sort((a, b) => b - a) // DESC
      // ASC
      // (a, b) => a - b

      // console.log(results)

      /**
       * CREATE SINGLE DUPLICATE IDENTIFIERS
       */
      let removeDupes = [...new Set(results)]
      // console.log(removeDupes)

      /**
       * REMOVE ITEMS FROM ACTUAL DATA
       */
      let finalData = response.data
      removeDupes.map(item => {
        finalData.splice(item, 1)
      })

      // console.log(finalData)

      this.setState({ dataNews: finalData, tabIndex: tab, showLoader: false })
    })
  }
  // PROPUBLICA
  queryProPublica(id, tab) {
    axios({
      method: 'get',
      url: `https://us-central1-packet-land.cloudfunctions.net/apiProPub/vote?q=${id}`
    }).then(response => {
      this.setState({
        dataProPublica: response.data,
        tabIndex: tab,
        showLoader: false
      })
    })
  }

  // SELECT TAB
  selectTab(tabIndex) {
    this.setState({ showLoader: true })

    // TWITTER
    if (tabIndex === 0) {
      this.queryTwitter('abrindisiny', tabIndex)
    }
    // FACEBOOK
    if (tabIndex === 1) {
      this.queryFacebook('BrindisiForCongress', tabIndex)
    }

    // YOUTUBE
    if (tabIndex === 2) {
      this.queryYouTubeChannelAll(
        [
          'UCv94lOGHXuIyYyIRejVGJqw',
          'UCqkbu0bMGNn7gyFVx1kdjsg',
          'UCCHxPFSJVmj6uhg9Kc_mNjg'
        ],
        tabIndex
      )
    }

    // NEWS
    if (tabIndex === 3) {
      this.queryNews('Anthony Brindisi', tabIndex)
    }

    /// ///////////////////////////////////////////////////////

    // TWITTER
    if (tabIndex === 4) {
      // this.queryTwitter('reptenney', tabIndex);
      this.queryTwitter2('reptenney', 'claudiatenney', tabIndex)
    }

    // FACEBOOK
    if (tabIndex === 5) {
      this.queryFacebook2('RepClaudiaTenney', 'ClaudiaForCongress', tabIndex)
    }

    // YOUTUBE
    if (tabIndex === 6) {
      // this.queryYouTubeChannel('UCOQKVZSiiz9tkwVXYFuyICw', tabIndex);
      this.queryYouTubeChannelAll(
        [
          'UCOQKVZSiiz9tkwVXYFuyICw',
          'UC8cYjOWq1B2OZhF4PjT-JhA',
          'UClJKQAD1YiqXYZHwbkRnNTw'
        ],
        tabIndex
      )
    }

    // NEWS
    if (tabIndex === 7) {
      this.queryNews('Claudia Tenney', tabIndex)
    }

    // PROPUBLICA
    if (tabIndex === 8) {
      this.queryProPublica('T000478', tabIndex)
    }
  }

  render() {
    return (
      <div className={classes.container}>
        <Tabs
          selectedIndex={this.state.tabIndex}
          onSelect={tabIndex => this.selectTab(tabIndex)}>
          <Grid container spacing={40}>
            <Grid item xs={2} className={classes.tabNav}>
              <TabList>
                <List
                  component="nav"
                  subheader={
                    <Hidden smDown>
                      <ListSubheader component="div">
                        Media Aggregator
                      </ListSubheader>
                    </Hidden>
                  }>
                  <Tab>
                    <ListItem button>
                      <Icon path={mdiTwitter} size={1} color="blue" />
                      <Hidden smDown>
                        <ListItemText primary="Twitter" />
                      </Hidden>
                    </ListItem>
                  </Tab>

                  <Tab>
                    <ListItem button>
                      <Icon path={mdiFacebook} size={1} color="blue" />
                      <Hidden smDown>
                        <ListItemText primary="Facebook" />
                      </Hidden>
                    </ListItem>
                  </Tab>

                  <Tab>
                    <ListItem button>
                      <Icon path={mdiYoutube} size={1} color="blue" />
                      <Hidden smDown>
                        <ListItemText primary="YouTube" />
                      </Hidden>
                    </ListItem>
                  </Tab>

                  <Tab>
                    <ListItem button>
                      <Icon path={mdiNewspaper} size={1} color="blue" />
                      <Hidden smDown>
                        <ListItemText primary="News" />
                      </Hidden>
                    </ListItem>
                  </Tab>

                  <Divider />

                  <Tab>
                    <ListItem button>
                      <Icon path={mdiTwitter} size={1} color="red" />
                      <Hidden smDown>
                        <ListItemText primary="Twitter" />
                      </Hidden>
                    </ListItem>
                  </Tab>

                  <Tab>
                    <ListItem button>
                      <Icon path={mdiFacebook} size={1} color="red" />
                      <Hidden smDown>
                        <ListItemText primary="Facebook" />
                      </Hidden>
                    </ListItem>
                  </Tab>

                  <Tab>
                    <ListItem button>
                      <Icon path={mdiYoutube} size={1} color="red" />
                      <Hidden smDown>
                        <ListItemText primary="YouTube" />
                      </Hidden>
                    </ListItem>
                  </Tab>

                  <Tab>
                    <ListItem button>
                      <Icon path={mdiNewspaper} size={1} color="red" />
                      <Hidden smDown>
                        <ListItemText primary="News" />
                      </Hidden>
                    </ListItem>
                  </Tab>

                  <Tab>
                    <ListItem button>
                      <Icon path={mdiVote} size={1} color="red" />
                      <Hidden smDown>
                        <ListItemText primary="ProPublica" />
                      </Hidden>
                    </ListItem>
                  </Tab>

                  <Divider />
                </List>
              </TabList>
            </Grid>

            <Grid item xs={10} className={classes.tabPanels}>
              {this.state.showLoader ? (
                <LinearProgress
                  color="secondary"
                  className={classes.progress}
                  size={50}
                />
              ) : null}

              <TabPanel>
                <h2>Brindisi Twitter</h2>
                <Tweets tweets={this.state.dataTwitter} />
              </TabPanel>

              <TabPanel>
                <h2>Brindisi Facebook - @BrindisiForCongress</h2>
                <Facebook posts={this.state.dataFacebook} />
              </TabPanel>

              <TabPanel>
                <h2>YouTube - Brindisi + HouseMajorityPac + EDF Action</h2>
                <YouTube videos={this.state.dataYouTube} />
              </TabPanel>

              <TabPanel>
                <h2>Brindisi News</h2>
                <Articles articles={this.state.dataNews} />
              </TabPanel>

              <TabPanel>
                <h2>Twitter - @RepTenney + @claudiatenney</h2>
                <Tweets tweets={this.state.dataTwitter} />
              </TabPanel>

              <TabPanel>
                <h2>Facebook - @RepClaudiaTenney + @ClaudiaForCongress</h2>
                <Facebook posts={this.state.dataFacebook} />
              </TabPanel>

              <TabPanel>
                <h2>YouTube - Tenney + CLFSuperPAC + NRCC</h2>
                <YouTube videos={this.state.dataYouTube} />
              </TabPanel>

              <TabPanel>
                <h2>Tenney News</h2>
                <Articles articles={this.state.dataNews} />
              </TabPanel>

              <TabPanel>
                <h2>Tenney ProPublica</h2>
                <VoteInfo votes={this.state.dataProPublica} />
              </TabPanel>
            </Grid>
          </Grid>
        </Tabs>
      </div>
    )
  }
}

MediaPage.propTypes = {
  children: PropTypes.object // from react-router
}

export default MediaPage
