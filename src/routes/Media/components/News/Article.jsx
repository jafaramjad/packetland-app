import React from 'react';
import moment from 'moment';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';
import classes from './Article.scss';

const Article = ({ article }) => (
	/*(
	<li className="article list-group-item">
		<div className="row">
			<div className="col-4">
				<a href={article.url} target="_blank">
					<img className="article-img" alt="Article Img" width="500px" src={article.urlToImage} />
				</a>
				<p className="text-ellipsis"> {article.source.name} </p>
				<p> {moment(article.publishedAt).format('dddd, MMMM Do')} </p>
			</div>
			<div className="col-8">
				<p>
					<a href={article.url} target="_blank">
						{article.title}
					</a>
				</p>
				<p>{article.description}</p>
			</div>
		</div>
	</li>
					<img className="article-img" alt="Article Img" width="500px" src={article.urlToImage} />

);*/
	/* 
	<Grid container spacing={40} className={classes.cardGrid}>
		{featuredPosts.map((post) => (
			<Grid item key={post.title} xs={12} md={6}>
				<Card className={classes.card}>
					<div className={classes.cardDetails}>
						<CardContent>
							<Typography variant="headline">{post.title}</Typography>
							<Typography variant="subheading" color="textSecondary">
								{post.date}
							</Typography>
							<Typography variant="subheading" paragraph>
								{post.description}
							</Typography>
							<Typography variant="subheading" color="primary">
								Continue reading...
							</Typography>
						</CardContent>
					</div>
					<Hidden xsDown>
						<CardMedia
							className={classes.cardMedia}
							style={{ height: 0, paddingTop: '56.25%' }}
							image="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22288%22%20height%3D%22225%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20288%20225%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_164edaf95ee%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_164edaf95ee%22%3E%3Crect%20width%3D%22288%22%20height%3D%22225%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2296.32500076293945%22%20y%3D%22118.8%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" // eslint-disable-line max-len
							title="Image title"
						/>
					</Hidden>
				</Card>
			</Grid>
		))}
	</Grid>
*/

	<Grid item key={article.title} xs={12} md={10}>
		<Card className={classes.card}>
			<div className={article.urlToImage ? classes.cardDetails : classes.cardDetailsNoPic}>
				<CardContent>
					<Typography variant="headline">
						<a href={article.url} target="_blank">{article.title}</a>
					</Typography>
					<Typography variant="subheading" color="textSecondary">
						{moment(article.publishedAt).format('dddd, MMMM Do')}
					</Typography>
					<Typography variant="subheading" paragraph>
						{article.description}
					</Typography>
					<Typography variant="subheading" color="primary">
						{article.source.name}
					</Typography>
				</CardContent>
			</div>

			<Hidden xsDown>
				{article.urlToImage ? <CardMedia className={classes.cardMedia} image={article.urlToImage} /> : ''}
			</Hidden>
		</Card>
	</Grid>
);

export default Article;
