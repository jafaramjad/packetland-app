import React from 'react'
import uuid from 'uuid/v1'
import Grid from '@material-ui/core/Grid'
import Article from './Article'
import classes from './Article.scss'

const Articles = ({ articles }) => {
  const generateList = articles => {
    return articles.map(article => <Article key={uuid()} article={article} />)
  }

  return (
    <Grid container spacing={40} className={classes.cardGrid}>
      {generateList(articles)}
    </Grid>
  )
}

export default Articles
