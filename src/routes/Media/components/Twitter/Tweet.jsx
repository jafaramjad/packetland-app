import React from 'react';
import moment from 'moment';
//  import _ from 'lodash';
import Grid from '@material-ui/core/Grid';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import RepeatIcon from '@material-ui/icons/Repeat';
import LinkIcon from '@material-ui/icons/Link';
import classes from './Twitter.scss';

const Tweet = ({ tweet }) => {
	/*
	const specifyTweetType = function(text, quote, reply) {
		if (text.slice(0, 2) === 'RT') {
			return 'tweet-retweet';
		} else if (quote) {
			return 'tweet-quote';
		} else if (reply && _.get(tweet, 'in_reply_to_screen_name') === tweet.user.screen_name) {
			return 'tweet-reply-self';
		} else if (reply) {
			return 'tweet-reply';
		}
		return 'tweet';
	};

	const tweetLinkMaker = function(username, tweetId) {
		return tweetId
			? `https://www.twitter.com/${username}/status/${tweetId}`
			: `https://www.twitter.com/${username}`;
	};

	const addInteractionTag = function(tweet) {
		const { text, is_quote_status: quote, in_reply_to_screen_name: reply } = tweet;
		let action, link, name, tweetUrl;
		if (
			text.slice(0, 2) === 'RT' ||
			quote ||
			(reply && _.get(tweet, 'in_reply_to_screen_name') !== tweet.user.screen_name)
		) {
			if (text.slice(0, 2) === 'RT') {
				action = 'Retweeted';
				name = text.match(/(@)\w+/)[0].slice(1);
				link = tweetLinkMaker(name);
				tweetUrl = tweetLinkMaker(name, _.get(tweet, 'retweeted_status.id_str'));
			} else if (quote) {
				action = 'Quoted';
				name = _.get(tweet, 'quoted_status.user.screen_name');
				link = tweetLinkMaker(name);
				tweetUrl = tweetLinkMaker(name, _.get(tweet, 'quoted_status.id_str'));
			} else if (reply && _.get(tweet, 'in_reply_to_screen_name') !== tweet.user.screen_name) {
				action = 'Mentioned';
				name = _.get(tweet, 'in_reply_to_screen_name');
				link = tweetLinkMaker(name);
			}

			if (tweetUrl)
				return (
					<p>
						{action}{' '}
						<a href={link} target="_blank">
							{'@' + name}
						</a>
						's{' '}
						<a href={tweetUrl} target="_blank">
							tweet
						</a>
					</p>
				);
			else
				return (
					<p>
						{action}{' '}
						<a href={link} target="_blank">
							{'@' + name}
						</a>
					</p>
				);
		}
	};

	const addInteractionUser = function(tweet) {
		const { text, is_quote_status: quote, in_reply_to_screen_name: reply } = tweet;
		let user, userLink, userImg;
		if (
			text.slice(0, 2) === 'RT' ||
			quote ||
			(reply && _.get(tweet, 'in_reply_to_screen_name') !== tweet.user.screen_name)
		) {
			if (text.slice(0, 2) === 'RT') {
				user = _.get(tweet, 'retweeted_status.user', {});
				userLink = tweetLinkMaker(user.screen_name);
				userImg = user.profile_image_url_https;
			} else if (quote) {
				user = _.get(tweet, 'quoted_status.user', {});
				userLink = tweetLinkMaker(user.screen_name);
				userImg = user.profile_image_url_https;
			} else if (reply && _.get(tweet, 'in_reply_to_screen_name') !== tweet.user.screen_name) {
				user = _.get(tweet, 'entities', {});
				userLink = tweetLinkMaker(tweet.in_reply_to_status_id_str);
				userImg = tweet.in_reply_to_status_id_str;
			}
			return (
				<a href={userLink} target="_blank">
					<img src={userImg} alt="" />
				</a>
			);
		}
	};
	*/

	/*
	return (
		<li
			className={`article list-group-item ${specifyTweetType(
				tweet.text,
				tweet.is_quote_status,
				tweet.in_reply_to_screen_name
			)}`}
		>
			<div className="row">
				<div className="col-2">
					<a href={tweetLinkMaker(tweet.user.id_str, tweet.id_str)} target="_blank">
						<img alt="Tweet Img" src={tweet.user.profile_image_url} />
					</a>
					{addInteractionUser(tweet)}
				</div>
				<div className="col-10">
					{addInteractionTag(tweet)}
					<p> {tweet.text} </p>
					<div className="d-flex justify-content-around">
						<p className="m-0 text-danger">
							{' '}
							<i className="fas fa-heart" /> {tweet.favorite_count}{' '}
						</p>
						<p className="m-0 text-success">
							{' '}
							<i className="fas fa-retweet" /> {tweet.retweet_count}{' '}
						</p>
						<p className="m-0 "> {moment(tweet.created_at).format('dddd, MMMM Do')} </p>
						<p className="m-0 "> {moment(tweet.created_at).format('h:mm:ssa')} </p>
					</div>
				</div>
			</div>
		</li>
	);
	*/
	return (
		<Grid item key={tweet.created_at} xs={12} md={10}>
			<Card>
				<CardHeader
					avatar={
						<Avatar aria-label="Recipe">
							<img alt="Tweet Avatar" src={tweet.user.profile_image_url.replace('http', 'https')} />
						</Avatar>
					}
					action={
						<IconButton>
							<a href={`https://www.twitter.com/${tweet.user.id_str}/status/${tweet.id_str}`} target='_blank'>
								<LinkIcon />
							</a>
						</IconButton>
					}
					title={'@' + tweet.user.screen_name}
					subheader={moment(tweet.created_at, 'dd MMM DD HH:mm:ss ZZ YYYY', 'en').format('ddd MMM Do, h:mm a')}
				/>
				<CardContent>
					<Typography className={classes.tweetText} component="p">
						{tweet.text}
					</Typography>
				</CardContent>
				<CardActions disableActionSpacing>
					<IconButton aria-label="Favorite Count">
						<FavoriteIcon />
						{tweet.favorite_count}
					</IconButton>
					<IconButton aria-label="Retweets">
						<RepeatIcon />
						{tweet.retweet_count}
					</IconButton>
				</CardActions>
			</Card>
			<br />
		</Grid>
	);
};

export default Tweet;
