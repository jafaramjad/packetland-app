import React from 'react'
import Tweet from './Tweet'
import uuid from 'uuid/v1'
import classes from './Twitter.scss'

const Tweets = ({ tweets }) => {
  const generateList = function(tweets) {
    return tweets.map(tweet => <Tweet key={uuid()} tweet={tweet} />)
  }

  return (
    <div className={classes.container}>
      <div>{generateList(tweets)}</div>
    </div>
  )
}

export default Tweets
