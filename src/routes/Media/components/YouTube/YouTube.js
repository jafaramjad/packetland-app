import React, { Component } from 'react'
import moment from 'moment'

import Video from './Video'
import uuid from 'uuid/v1'
import List from '@material-ui/core/List'
import classes from './YouTube.scss'
import Grid from '@material-ui/core/Grid'

export default class YouTube extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedId: props.videos[0].id.videoId,
      videoTitle: props.videos[0].snippet.title,
      videoDate: props.videos[0].snippet.publishedAt,
      videoChannel: props.videos[0].snippet.channelTitle,
      videoDesc: props.videos[0].snippet.description
    }

    // console.log(props);
  }

  handleVideoListClick = videoInfo => {
    console.log(videoInfo)
    this.setState({
      selectedId: videoInfo.id.videoId,
      videoTitle: videoInfo.snippet.title,
      videoDate: videoInfo.snippet.publishedAt,
      videoDesc: videoInfo.snippet.description,
      videoChannel: videoInfo.snippet.channelTitle
    })
  }

  generateList = videos => {
    return videos.map(video => (
      <Video key={uuid()} video={video} click={this.handleVideoListClick} />
    ))
  }

  render() {
    const { videos } = this.props

    return (
      <Grid container spacing={40}>
        <Grid item xs={8} className={classes.videoPlayerContainer}>
          <iframe
            className={classes.videoIframePlayer}
            src={`https://www.youtube.com/embed/${this.state.selectedId}`}
            frameBorder="0"
            allow="autoplay; encrypted-media"
            allowFullScreen
          />
          <br />
          <h2>{this.state.videoTitle}</h2>
          <h4>{moment(this.state.videoDate).format('MMM Do, YYYY')}</h4>
          <h6>{this.state.videoChannel}</h6>
          <p>{this.state.videoDesc}</p>
        </Grid>

        <Grid item xs={4}>
          <List className={classes.videoList}>{this.generateList(videos)}</List>
        </Grid>
      </Grid>
    )

    /*
		return (
			<div>
				<div>
					<iframe
						src={`https://www.youtube.com/embed/${this.state.selectedId || videos[0].id.videoId}`}
						frameBorder="0"
						width="560"
						height="315"
						allow="autoplay; encrypted-media"
						allowFullScreen
					/>
				</div>

				<div>
					<ul>{this.generateList(videos)}</ul>
				</div>
			</div>
		); */
  }
}

// export default YouTube;
