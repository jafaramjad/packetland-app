import { compose } from 'redux'
import { connect } from 'react-redux'
import { LIST_PATH } from 'constants'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { firestoreConnect } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'

export default compose(
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  // Map auth uid from state to props
  connect(({ firebase: { auth: { uid } } }) => ({ uid })),
  // Wait for uid to exist before going further
  spinnerWhileLoading(['uid']),
  // Create listeners based on current users UID
  firestoreConnect(({ params, uid }) => [
    // Listener for projects the current user created
    // {
    //   collection: 'projects',
    //   where: ['createdBy', '==', uid]
    // }
  ]),
  // Map projects from state to props
  // connect(({ firestore: { ordered } }) => ({
  //   projects: ordered.projects
  // })),
  // Show loading spinner while projects and collabProjects are loading
  // spinnerWhileLoading(['projects']),
  // Add props.router
  withRouter,
  // Add props.showError and props.showSuccess
  withNotifications,
  // Add state and state handlers as props
  withStateHandlers(
    // Setup initial state
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    // Add state handlers as props
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),
  // Add handlers as props
  withHandlers({
    addTestData: props => (newData, successText) => {
      const { firestore, uid, showError, showSuccess, toggleDialog } = props
      // console.log(props)
      // console.log(newData)

      // const locationName = newData.info.locationName
      // const stationName = newData.info.stationName
      // const dayName = newData.info.dayName

      if (!uid) {
        return console.error('You must be logged in to create a project')
      }

      // firestore.collection('governor').doc('ky_beshear').collection('canvass').doc('adair')
      // .set({ thing: 'huh' })

      let batch = firestore.batch()

      let doc0 = firestore.collection('ky_gov_beshear').doc('gotv')
      let doc1 = firestore.collection('ky_gov_beshear').doc('canvass')
      let doc2 = firestore.collection('ky_gov_beshear').doc('locations')
      let doc3 = firestore.collection('ky_gov_beshear').doc('logs')
      let doc4 = firestore.collection('ky_gov_beshear').doc('users')
      let doc5 = firestore.collection('ky_gov_beshear').doc('volunteers')

      batch.set(doc0, {})
      batch.set(doc1, {})
      batch.set(doc2, {})
      batch.set(doc3, {})
      batch.set(doc4, {})
      batch.set(doc5, {})

      // Commit the batch
      batch.commit().then(() => {
        console.log('finished batch commit')

        firestore.set(
          { collection: `_orgs/`, doc: 'ky_gov_beshear' },
          {
            campaign: 'beshear',
            candidate: 'andy beshear',
            label: 'ky_gov_beshear',
            type: 'gov'
          },
          { merge: true }
        )
      })

      /*
      firestore.set({ collection: `kentucky`, doc: 'gov_beshear' }, {}, { merge: true })
        .then(() => {
          // firestore.set({ collection: `kentucky/gov_beshear/canvass`, doc: 'demo' }, {}, { merge: true })
          // firestore.set({ collection: `kentucky/gov_beshear/locations`, doc: 'demo' }, {}, { merge: true })
          // firestore.set({ collection: `kentucky/gov_beshear/logs`, doc: 'demo' }, {}, { merge: true })
          // firestore.set({ collection: `kentucky/gov_beshear/volunteers`, doc: 'demo' }, {}, { merge: true })
          // firestore.set({ collection: `kentucky/gov_beshear/users`, doc: 'demo' }, {}, { merge: true })

          firestore.set({ collection: `kentucky/`, doc: 'gov_beshear' }, {
            canvass: {},
            locations: {},
            logs: {},
            volunteers: {},
            users: {}
          }, { merge: true })

          firestore.set({ collection: `_orgs/`, doc: 'kentucky' }, {
            gov: {
              campaign: 'beshear',
              candidate: 'andy beshear',
              type: 'gov'
            }
          }, { merge: true })


        }) */

      /*   firestore.set({ collection: `governor`, doc: 'ky_beshear' }, {}, { merge: true })
          .then(() => {
            firestore.set({ collection: `governor/ky_beshear/canvass`, doc: 'demo' }, {}, { merge: true })
            firestore.set({ collection: `governor/ky_beshear/locations`, doc: 'demo' }, {}, { merge: true })
            firestore.set({ collection: `governor/ky_beshear/logs`, doc: 'demo' }, {}, { merge: true })
            firestore.set({ collection: `governor/ky_beshear/volunteers`, doc: 'demo' }, {}, { merge: true })
            firestore.set({ collection: `governor/ky_beshear/users`, doc: 'demo' }, {}, { merge: true })
  
  
          }) */
      // firestore.set({ collection: `governor/ky_beshear/locations`, doc: 'adair' }, {}, { merge: true })
      /*    .then(() => {
           toggleDialog()
           // showSuccess(successText)
         })
         .catch(err => {
           console.error('Error:', err) // eslint-disable-line no-console
           showError(err.message || 'Could not add data')
           return Promise.reject(err)
         }) */
    },

    getAllOrgs: props => data => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props

      let list = []

      return (
        firestore
          .collection('_orgs')
          .get()
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` }) // .get({ collection: `gotv/oneida/utica`, doc: `day1` })
          // .get({ collection: `_orgs/`, doc: `` })
          .then(query => {
            // console.log('query data has been retrieved')
            console.log(query.docs[1])
            if (query.docs.length > 0) {
              query.docs.forEach(doc => {
                list.push(doc.data())
              })

              // const dbObj = query.docs.data()
              // console.log(query)
              // console.log(dbObj)
              // console.log(dbObj[data.station][data.day])
              /* 	if (dbObj[data.station] && dbObj[data.station][data.day]) {
                return { 0: dbObj[data.station][data.day], 1: dbObj[data.station] }
              } else {
                showError('No data exists for this selection')
  
                return undefined
              } */
              // console.log("Document data:", doc.data());
              // return doc.data()
              console.log(list)
              return list
            } else {
              // doc.data() will be undefined in this case
              showError('No data exists for this selection')
              // console.log("No such document!");
              return undefined
            }
          })
          .catch(err => {
            console.error('Error:', err) // eslint-disable-line no-console
            showError(err.message || 'Could not get shift')
            return Promise.reject(err)
          })
      )
    },
    getSampleData: props => data => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props

      let list = []

      return (
        firestore
          // .collection('ky_gov_beshear/')
          // .get()
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` }) // .get({ collection: `gotv/oneida/utica`, doc: `day1` })
          .get({ collection: `ky_gov_beshear/gotv/counties`, doc: `adair` })
          .then(query => {
            // console.log('query data has been retrieved')
            console.log(query)
            if (query.exists) {
              // console.log("Document data:", doc.data());
              return query.data()
            } else {
              // doc.data() will be undefined in this case
              showError('No data exists for this selection')
              // console.log("No such document!");
              return undefined
            }
          })
          .catch(err => {
            console.error('Error:', err) // eslint-disable-line no-console
            showError(err.message || 'Could not get shift')
            return Promise.reject(err)
          })
      )
    }
  }),

  pure // shallow equals comparison on props (prevent unessesary re-renders)
)
