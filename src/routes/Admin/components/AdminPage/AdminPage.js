import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import classes from './AdminPage.scss'

import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import LinearProgress from '@material-ui/core/LinearProgress'

class AdminPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showLoader: false,
      campaignName: '',
      twitterId: '',
      followerList: [],
      usersList: [],
      selectStation: 'disabledselect',
      open: false,
      isAdmin: true
    }
  }

  componentDidMount() {
    const orgsList = this.props.getAllOrgs()
    const gotvSample = this.props.getSampleData()
    console.log(orgsList)

    gotvSample.then(res => {
      console.log(res.outpost1)
    })

    this.preQueryCheck(this.queryGetList)
  }

  preQueryCheck = query => {
    this.props.firebase
      .auth()
      .currentUser.getIdTokenResult()
      .then(idTokenResult => {
        let isAdmin

        console.log(idTokenResult)

        if (idTokenResult.claims.admin === true) {
          isAdmin = true
        } else {
          isAdmin = true // FALSE
        }

        query(isAdmin)
      })
      .catch(error => {
        console.log(error)
      })
  }

  // GET QUERY
  queryGetList = isAdmin => {
    if (isAdmin) {
      const token = this.props.firebase.auth().currentUser.getIdToken()

      token
        .then(tok => {
          axios({
            method: 'get',
            headers: {
              'X-Requested-With': 'XMLHttpRequest',
              Authorization: 'Bearer' + ' ' + tok
            },
            url: `https://us-central1-packet-land.cloudfunctions.net/apiUsers/users/list`
          }).then(resp => {
            console.info(resp)
            this.setState({
              showLoader: false,
              isAdmin: true,
              usersList: resp.data.usersList
            })
          })
        })
        .catch(err => {
          // console.log(err)
        })
    } else {
      this.setState({
        isAdmin: false
      })
    }
  }

  // INIT ADMIN QUERY
  queryInitAdmin = isAdmin => {
    //	if (isAdmin) {
    const token = this.props.firebase.auth().currentUser.getIdToken()

    token
      .then(tok => {
        // const email = this.state.editEmail
        // const role = this.state.selectStation

        axios({
          method: 'post',
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            Authorization: 'Bearer' + ' ' + tok
          },
          url: `https://us-central1-packet-land.cloudfunctions.net/apiUsers/users/initadmin`
        }).then(resp => {
          console.info(resp)
          // this.preQueryCheck(this.queryGetList)
          // this.setState({
          // 	selectStation: 'disabledselect',
          // 	editEmail: '',
          // 	open: false
          // })
        })
      })
      .catch(err => {
        console.log(err)
      })

    // } else {
    // 	console.log('error not an admin...')
    // }
  }

  handleChange = evt => {
    // console.log(evt.target.value)
    this.setState({
      screenName: evt.target.value
    })
  }

  clickBtn = evt => {
    console.log(evt)
    this.setState(
      {
        showLoader: true
      },
      () => {
        // this.queryTwitterName(this.state.screenName)
        // this.queryVanPeople()
        this.queryInitAdmin(false)
      }
    )
  }

  clickNewCampaignBtn = evt => {
    console.log(evt)
    this.setState(
      {
        showLoader: true
      },
      () => {
        // this.queryTwitterName(this.state.screenName)
        // this.queryVanPeople()
        this.props.addTestData()
      }
    )
  }

  render() {
    return (
      <Paper className={classes.scoutPaper}>
        {this.state.showLoader ? (
          <LinearProgress
            color="secondary"
            className={classes.progress}
            size={50}
          />
        ) : null}

        {this.state.isAdmin === true ? (
          <Grid
            container
            className={classes.scoutGrid}
            spacing={8}
            direction="row"
            alignItems="center"
            justify="center">
            <Grid item xs={12}>
              <Typography variant="title">Admin</Typography>
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                color="primary"
                onClick={this.clickBtn}>
                ACTIVATE ADMIN
              </Button>
              <br />

              <TextField
                label="Campaign Name"
                className={classes.textField}
                value={this.state.campaignName}
                onChange={this.handleChange}
                margin="normal"
              />

              <Button
                variant="contained"
                color="primary"
                onClick={this.clickNewCampaignBtn}>
                CREATE CAMPAIGN
              </Button>
            </Grid>
            <Grid item>
              <table>
                <tbody>
                  {this.state.followerList.map((item, idx) => {
                    return (
                      <tr key={`follower-${idx}`}>
                        <td>{idx + 1}</td>
                        <td>{item.location}</td>
                        <td>{item.name}</td>
                        <td>@{item.screen_name}</td>
                      </tr>
                    )
                  })}
                </tbody>
              </table>
            </Grid>
          </Grid>
        ) : this.state.isAdmin === null ? (
          <div>Checking credentials...</div>
        ) : (
          <div>Not Authorized...</div>
        )}
      </Paper>
    )
  }
}

AdminPage.propTypes = {
  children: PropTypes.object // from react-router
}

export default AdminPage
