/**
 * THIS FILE IS NOT IN USE
 */
import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import SaveIcon from '@material-ui/icons/Save'
import { locations, stations } from '../../../../constants/places'
import classes from './DailyPage.scss'

export default class TotalPackets extends Component {
	handleClickSave = val => {
		console.log(val)
	}

	render() {
		return (
			<div>
				<h2>Total Packets</h2>
				<table>
					<tbody>
						{locations.map((item, idx) => {
							return (
								<tr key={idx}>
									<td className={classes.tableTD}>
										<b>{item}</b>
									</td>
									<td>
										<table>
											{stations[item].map(stationName => {
												return (
													<tr>
														<td>
															<input type="text" size="4" />
														</td>
														<td className={classes.subTableTD}>
															{stationName}
														</td>
													</tr>
												)
											})}
										</table>
									</td>
								</tr>
							)
						})}
					</tbody>
				</table>
				<br />
				<Button variant="contained" color="secondary" onClick={this.handleClickSave} size="large">
					Save &nbsp;
					<SaveIcon />
				</Button>
			</div>
		)
	}
}
