import { PEOPLE_PATH as path } from 'constants'

export default store => ({
  path,
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const PeoplePage = require('./components/PeoplePage').default
        // const API = require('./routes/Project').default;

        /*  Return getComponent   */
        cb(null, PeoplePage)

        /* Webpack named bundle   */
      },
      'Users'
    )
  } /* ,
	getChildRoutes(partialNextState, cb) {
		require.ensure([], (require) => {
	
			//const TWITTER_API = require('./routes/Project').default;
			const APIName = require('./routes/APIName').default;

			
			cb(null, [ APIName(store) ]);
		});
	} */
})
