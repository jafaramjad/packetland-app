import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import { isEmpty } from 'react-redux-firebase'
import classes from './PeoplePage.scss'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'
import NativeSelect from '@material-ui/core/NativeSelect'
import LinearProgress from '@material-ui/core/LinearProgress'
import moment from 'moment'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Avatar from '@material-ui/core/Avatar'


// import { stations } from '../../../../constants/places'


class PeoplePage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dialogAddPeople: false,
      textAddPeople: '',
      showLoader: true,
      editEmail: '',
      usersList: [],
      selectStation: 'disabledselect',
      selectOrg: 'disabledselect',
      selectCamp: 'disabledselect',

      dialogContent: '',
      dialogCurrentName: '',
      open: false,
      openOrg: false,
      isAdmin: null,
      isDirector: null,
      isOrg: null,
      isCamp: null,
      stations: {}
    }
  }

  componentDidMount() {
    this.preQueryCheck(this.queryGetList)
  }

  preQueryCheck = query => {
    this.props.firebase
      .auth()
      .currentUser.getIdTokenResult()
      .then(idTokenResult => {
        let isClaims

        // console.log(idTokenResult)

        if (idTokenResult.claims.role !== null && idTokenResult.claims.org !== null && idTokenResult.claims.camp !== null || idTokenResult.claims.admin) {
          isClaims = idTokenResult.claims
        } else {
          isClaims = false
        }

        query(isClaims)
      })
      .catch(error => {
        console.log(error)
      })
  }

  // GET QUERY
  queryGetList = isClaims => {
    if (isClaims.role && isClaims.role.includes('director') || isClaims.admin) {
      const token = this.props.firebase.auth().currentUser.getIdToken()

      token
        .then(tok => {
          axios({
            method: 'get',
            headers: {
              'X-Requested-With': 'XMLHttpRequest',
              Authorization: 'Bearer' + ' ' + tok
            },
            url: `https://us-central1-packet-land.cloudfunctions.net/apiUsers/users/listByClaims?org=${isClaims.org[0]}&camp=${isClaims.camp[0]}` //&role=${isClaims.role[0]}
          }).then(resp => {
            console.info(resp)
            this.setState({
              showLoader: false,
              isAdmin: isClaims.admin,
              isDirector: isClaims.role ? true : false,
              isOrg: isClaims.org[0],
              isCamp: isClaims.camp[0],
              usersList: resp.data.usersList
            }, () => {
              this.loadPlaces()
            })
          })
        })
        .catch(err => {
          // console.log(err)
        })


    } else {
      this.setState({
        isAdmin: false,
        isDirector: false,
        isOrg: false,
        isCamp: false,
      })
    }
  }

  loadPlaces = () => {
    this.props.getPlaces().then(doc => {
      // console.log(doc)
      // console.log(doc.data())
      const data = doc.data()
      this.setState({
        stations: data.stations
      })
    })
  }

  // GET QUERY
  queryPostClaim = (isClaims) => {
    if (isClaims && isClaims.role && isClaims.role.includes('director') || isClaims.admin) {
      const token = this.props.firebase.auth().currentUser.getIdToken()

      token
        .then(tok => {
          const email = this.state.editEmail
          const claim = this.state.dialogCurrentName
          let val

          switch (claim) {
            case 'role':
              val = this.state.selectStation
              break
            case 'org':
              val = this.state.selectOrg
              break
            case 'camp':
              val = this.state.selectCamp
              break
            default:
              val = ''
              break
          }

          // console.log(claim)
          // console.log(email)
          // console.log(val)

          if (val === '') {
            return false
          }

          axios({
            method: 'post',
            headers: {
              'X-Requested-With': 'XMLHttpRequest',
              Authorization: 'Bearer' + ' ' + tok
            },
            url: `https://us-central1-packet-land.cloudfunctions.net/apiUsers/users/claims?email=${email}&claim=${claim}&val=${val}`
          }).then(resp => {
            // console.info(resp)

            this.setState({
              selectStation: 'disabledselect',
              selectOrg: 'disabledselect',
              selectCamp: 'disabledselect',
              editEmail: '',
              open: false
            }, () => {
              this.preQueryCheck(this.queryGetList)
            })
          })


        })
        .catch(err => {
          console.log(err)
        })
    } else {
      console.log('error not an admin...')
    }
  }


  // GET QUERY
  queryAddPeople = (isClaims) => {
    if (isClaims && isClaims.role && isClaims.role.includes('director') || isClaims.admin) {
      const token = this.props.firebase.auth().currentUser.getIdToken()

      console.log('have the token...')
      token
        .then(tok => {
          const email = this.state.editEmail
          const claim = this.state.dialogCurrentName
          let val = this.state.textAddPeople

          // switch (claim) {
          //   case 'role':
          //     val = this.state.selectStation
          //     break
          //   case 'org':
          //     val = this.state.selectOrg
          //     break
          //   case 'camp':
          //     val = this.state.selectCamp
          //     break
          //   default:
          //     val = ''
          //     break
          // }

          // console.log(claim)
          // console.log(email)
          // console.log(val)

          if (val === '') {
            return false
          }

          axios({
            method: 'post',
            headers: {
              'X-Requested-With': 'XMLHttpRequest',
              Authorization: 'Bearer' + ' ' + tok
            },
            url: `https://us-central1-packet-land.cloudfunctions.net/apiUsers/users/people/new?email=${val}&role=${'staff'}`
          }).then(resp => {
            // console.info(resp)


            this.setState({
              dialogAddPeople: false,
              textAddPeople: ''
            }, () => {
              this.preQueryCheck(this.queryGetList)
            })

            // this.setState({
            //   selectStation: 'disabledselect',
            //   selectOrg: 'disabledselect',
            //   selectCamp: 'disabledselect',
            //   editEmail: '',
            //   open: false
            // }, () => {
            //   this.preQueryCheck(this.queryGetList)
            // })
          })


        })
        .catch(err => {
          console.log(err)
        })
    } else {
      console.log('error not an admin...')
    }
  }


  handleOpen = (email, claimDialog, claimName) => evt => {

    // console.log(claimDialog)
    // console.log(claimName)

    this.setState({
      editEmail: email,
      open: true,
      dialogContent: claimDialog,
      dialogCurrentName: claimName
    })
  }


  handleClose = btnPick => evt => {
    // console.log(btnPick)
    // console.log(this.state.selectStation)
    // console.log(this.state.editEmail)

    if (btnPick === 'save') {
      // console.log(this.state)
      // console.log(this.state.dialogCurrentName)


      this.preQueryCheck(this.queryPostClaim)

      // this.setState({ open: false })
    } else {
      this.setState({
        open: false,
        selectOrg: 'disabledselect',
        selectStation: 'disabledselect',
        selectCamp: 'disabledselect',
        editEmail: ''
      })
    }
  }

  onSelectChange = selectName => evt => {
    const pick = evt.target.value
    // console.log(pick)
    // console.log(selectName)

    this.setState({
      [selectName]: pick
    })
  }

  claimsRoles = () => {
    return (
      <NativeSelect
        onChange={this.onSelectChange('selectStation')}
        value={this.state.selectStation}>
        <option value="disabledselect" disabled>
          Please Select Option
    </option>
        <optgroup label="Administrative">
          <option value="director">Director</option>
          <option value="staff">Staff</option>
          <option value="remove">Remove Roles</option>
        </optgroup>
        {Object.entries(this.state.stations).map((item, idx) => {
          return (
            <optgroup key={`optgroup-${idx}`} label={item[0]}>
              {item[1].map((name, idx2) => {
                const valFormat = name
                  .replace(' ', '_')
                  .replace('(', '')
                  .replace(')', '')
                  .toLowerCase()
                return (
                  <option key={`station-${idx2}`} value={item[0].toLowerCase() + '|' + valFormat}>
                    {name}
                  </option>
                )
              })}
            </optgroup>
          )
        })}
      </NativeSelect>
    )
  }

  closeDialogAddPeople = (clickType) => evt => {
    console.log(evt)

    if (clickType === 'save') {
      this.preQueryCheck(this.queryAddPeople)
    } else {
      this.setState({
        dialogAddPeople: false,
        textAddPeople: ''
      })
    }






  }

  openDialogAddPeople = () => evt => {
    console.log(evt)
    this.setState({ dialogAddPeople: true })
  }
  updateTextAddPeople = () => evt => {
    console.log(evt)
    this.setState({ textAddPeople: evt.target.value })
  }

  /*   claimsOrg = () => {
      return (
        <NativeSelect
          onChange={this.onSelectChange('selectOrg')}
          value={this.state.selectOrg}>
          <option value="disabledselect" disabled>
            Please Select Org
        </option>
          <optgroup label="Congressional">
            <option value="NC09">NC-09</option>
            <option value="NY22">NY-22</option>
          </optgroup>
          <optgroup label="Admin">
            <option value="DEMO">Demo</option>
            <option value="remove">Remove Org</option>
          </optgroup>
        </NativeSelect>
      )
    } */
  /* 
    claimsCamp = () => {
      return (
        <NativeSelect
          onChange={this.onSelectChange('selectCamp')}
          value={this.state.selectCamp}>
          <option value="disabledselect" disabled>
            Please Select Campaign
        </option>
          <optgroup label="NC-09">
            <option value="MCCREADY">McCready</option>
          </optgroup>
          <optgroup label="NY-22">
            <option value="BRINDISI">Brindisi</option>
          </optgroup>
          <optgroup label="DEMO">
            <option value="OBAMA">Obama</option>
            <option value="CARTER">Carter</option>
            <option value="KENNEDY">Kennedy</option>
          </optgroup>
          <optgroup label="ADMIN">
            <option value="remove">Remove Camp</option>
          </optgroup>
        </NativeSelect>
      )
    } */

  render() {
    return (
      <Paper className={classes.userList} style={{ overflow: 'auto' }}>
        {this.state.showLoader ? (
          <LinearProgress
            color="secondary"
            className={classes.progress}
            size={50}
          />
        ) : null}

        {this.state.isAdmin === true || this.state.isDirector === true ? (
          <div>
            <div>
              <Grid container spacing={16} alignItems="stretch">

                <Grid item xs={12} sm={9}>
                  <h2>People</h2>
                </Grid>

                <Grid item xs={12} sm={3}>

                  <Button variant="outlined" color="primary" className={classes.btnAddPeople} onClick={this.openDialogAddPeople()}>
                    Add New People
                  </Button>

                </Grid>
              </Grid>
            </div>

            <table className={classes.userTable}>
              <thead>
                <tr>
                  <th />
                  <th></th>
                  <th>Name</th>
                  <th>Role</th>
                  <th>Email</th>
                  <th>Last Login</th>
                </tr>
              </thead>
              <tbody>
                {this.state.usersList.map((item, idx) => {
                  const claims = item.customClaims

                  // console.log(claims)

                  let role = claims ? Object.entries(claims) : ''

                  let currentRole = claims && claims.role ? claims.role : '-' // role[0] ? role[0][0] === 'admin' ? role[0][0] : role[0][1] : '-'
                  let currentCamp = claims && claims.camp ? claims.camp : '-' // role[0] ? role[0][0] === 'admin' ? role[0][0] : role[0][1] : '-'
                  let currentOrg = claims && claims.org ? claims.org : '-' // role[0] ? role[0][0] === 'admin' ? role[0][0] : role[0][1] : '-'


                  return (
                    <tr key={`users-${idx}`}>
                      <td>{idx + 1}</td>

                      <td>
                        <Avatar src={item.photoURL} className={classes.userAvatar} />
                        {/* <img src={item.photoURL} className={classes.userAvatar} /> */}
                      </td>

                      <td>
                        {item.displayName}

                      </td>



                      <td>
                        {role[0] && role[0][0] === 'admin' ? (
                          <Button disabled>Admin</Button>
                        ) : (
                            <Button
                              color="primary"
                              onClick={this.handleOpen(item.email, this.claimsRoles, 'role')}>
                              {currentRole}
                            </Button>
                          )}
                      </td>
                      <td>{item.email}</td>
                      <td>
                        {item.metadata.lastSignInTime
                          ? moment(item.metadata.lastSignInTime).format(
                            'MMM Do h:mm a'
                          )
                          : '-'}
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
            <br />

            <Dialog open={this.state.open} onClose={this.handleClose}>
              <DialogTitle>
                <span>Edit - </span>
                <span>{this.state.editEmail}</span>
              </DialogTitle>
              <DialogContent>
                {this.state.dialogContent ? this.state.dialogContent() : null}
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose('cancel')} color="primary">
                  Cancel
                </Button>
                <Button onClick={this.handleClose('save')} color="primary">
                  Save
                </Button>
              </DialogActions>
            </Dialog>


            <Dialog
              open={this.state.dialogAddPeople}
              onClose={this.closeDialogAddPeople('cancel')}
            >
              <DialogTitle>Add New People</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  Please enter a gmail related email.
            </DialogContentText>
                <TextField
                  autoFocus
                  value={this.state.textAddPeople}
                  onChange={this.updateTextAddPeople()}
                  label="Email Address"
                  type="email"
                  fullWidth
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.closeDialogAddPeople('cancel')} color="primary">
                  Cancel
            </Button>
                <Button onClick={this.closeDialogAddPeople('save')} color="primary">
                  Save
            </Button>
              </DialogActions>
            </Dialog>


          </div>
        ) : this.state.isDirector === null && this.state.isAdmin === null ? (
          <div>Checking credentials...</div>
        ) : (
              <div>Not Authorized...</div>
            )}
      </Paper>
    )
  }
}



export default PeoplePage

