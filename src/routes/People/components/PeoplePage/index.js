import PeoplePage from './PeoplePage'
import enhance from './PeoplePage.enhancer'

export default enhance(PeoplePage)
