import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import { isEmpty } from 'react-redux-firebase'
import classes from './UsersPage.scss'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'
import NativeSelect from '@material-ui/core/NativeSelect'
import LinearProgress from '@material-ui/core/LinearProgress'
import moment from 'moment'
import Paper from '@material-ui/core/Paper'

import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'


// import { stations } from '../../../../constants/places'


class PeoplePage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            showLoader: true,
            editEmail: '',
            usersList: [],
            selectStation: 'disabledselect',
            selectOrg: '',
            open: false,
            openOrg: false,
            isAdmin: null,
            stations: {}
        }
    }

    componentDidMount() {
        this.preQueryCheck(this.queryGetList)
    }

    preQueryCheck = query => {
        this.props.firebase
            .auth()
            .currentUser.getIdTokenResult()
            .then(idTokenResult => {
                let isAdmin

                console.log(idTokenResult)

                if (idTokenResult.claims.admin === true) {
                    isAdmin = true
                } else {
                    isAdmin = false
                }

                query(isAdmin)
            })
            .catch(error => {
                console.log(error)
            })
    }

    // GET QUERY
    queryGetList = isAdmin => {
        if (isAdmin) {
            const token = this.props.firebase.auth().currentUser.getIdToken()

            token
                .then(tok => {
                    axios({
                        method: 'get',
                        headers: {
                            'X-Requested-With': 'XMLHttpRequest',
                            Authorization: 'Bearer' + ' ' + tok
                        },
                        url: `https://us-central1-packet-land.cloudfunctions.net/apiUsers/users/list`
                    }).then(resp => {
                        console.info(resp)
                        this.setState({
                            showLoader: false,
                            isAdmin: true,
                            usersList: resp.data.usersList
                        }, () => {
                            this.loadPlaces()
                        })
                    })
                })
                .catch(err => {
                    // console.log(err)
                })
        } else {
            this.setState({
                isAdmin: false
            })
        }
    }

    loadPlaces = () => {
        this.props.getPlaces()
            .then(doc => {
                console.log(doc)
                console.log(doc.data())
                const data = doc.data()
                this.setState({
                    stations: data.stations
                })
            })
    }

    // GET QUERY
    queryPostRoles = isAdmin => {
        if (isAdmin) {
            const token = this.props.firebase.auth().currentUser.getIdToken()

            token
                .then(tok => {
                    const email = this.state.editEmail
                    const role = this.state.selectStation

                    axios({
                        method: 'post',
                        headers: {
                            'X-Requested-With': 'XMLHttpRequest',
                            Authorization: 'Bearer' + ' ' + tok
                        },
                        url: `https://us-central1-packet-land.cloudfunctions.net/apiUsers/users/roles?email=${email}&role=${role}`
                    }).then(resp => {
                        // console.info(resp)
                        this.preQueryCheck(this.queryGetList)
                        this.setState({
                            selectStation: 'disabledselect',
                            editEmail: '',
                            open: false
                        })
                    })
                })
                .catch(err => {
                    console.log(err)
                })
        } else {
            console.log('error not an admin...')
        }
    }

    // GET QUERY
    queryPostOrg = isAdmin => {
        if (isAdmin) {
            const token = this.props.firebase.auth().currentUser.getIdToken()

            token
                .then(tok => {
                    const email = this.state.editEmail
                    const org = this.state.selectOrg

                    axios({
                        method: 'post',
                        headers: {
                            'X-Requested-With': 'XMLHttpRequest',
                            Authorization: 'Bearer' + ' ' + tok
                        },
                        url: `https://us-central1-packet-land.cloudfunctions.net/apiUsers/users/org?email=${email}&org=${org}`
                    }).then(resp => {
                        // console.info(resp)
                        this.preQueryCheck(this.queryGetList)
                        this.setState({
                            selectStation: 'disabledselect',
                            editEmail: '',
                            open: false
                        })
                    })
                })
                .catch(err => {
                    console.log(err)
                })
        } else {
            console.log('error not an admin...')
        }
    }



    handleOpen = email => evt => {
        this.setState({
            editEmail: email,
            open: true
        })
    }

    handleOpenOrg = email => evt => {
        this.setState({
            editEmail: email,
            openOrg: true
        })
    }

    handleClose = btnPick => evt => {
        // console.log(btnPick)
        // console.log(this.state.selectStation)
        // console.log(this.state.editEmail)

        if (btnPick === 'save') {
            this.preQueryCheck(this.queryPostRoles)

            // this.setState({ open: false })
        } else {
            this.setState({
                open: false,
                selectStation: 'disabledselect',
                editEmail: ''
            })
        }
    }

    handleCloseOrg = btnPick => evt => {
        // console.log(btnPick)
        // console.log(this.state.selectStation)
        // console.log(this.state.editEmail)

        if (btnPick === 'save') {
            this.preQueryCheck(this.queryPostRoles)

            // this.setState({ open: false })
        } else {
            this.setState({
                openOrg: false,
                selectOrg: 'disabledselect',
                editEmail: ''
            })
        }
    }

    onSelectChange = evt => {
        const pick = evt.target.value
        // console.log(pick)
        this.setState({
            selectStation: pick
        })
    }

    onSelectChangeOrg = evt => {
        const pick = evt.target.value
        // console.log(pick)
        this.setState({
            selectOrg: pick
        })
    }

    render() {
        return (
            <Paper className={classes.userList} style={{ overflow: 'auto' }}>
                {this.state.showLoader ? (
                    <LinearProgress
                        color="secondary"
                        className={classes.progress}
                        size={50}
                    />
                ) : null}

                {this.state.isAdmin === true ? (
                    <div>
                        <h2>USERS</h2>
                        <table className={classes.userTable}>
                            <thead>
                                <tr>
                                    <th />
                                    <th>Org</th>
                                    <th>Campaign</th>
                                    <th>Role</th>
                                    <th>Email</th>
                                    <th>Last Login</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.usersList.map((item, idx) => {
                                    const claims = item.customClaims
                                    let role = claims ? Object.entries(claims) : ''
                                    let currentRole = role[0] ? role[0][0] === 'admin' ? role[0][0] : role[0][1] : '-'
                                    return (
                                        <tr key={`users-${idx}`}>
                                            <td>{idx + 1}</td>

                                            <td>

                                                <Button
                                                    color="primary"
                                                    onClick={this.handleOpenOrg(item.email)}>
                                                    {currentRole}
                                                </Button>

                                            </td>

                                            <td>
                                                MCCREADY
                      </td>

                                            <td>
                                                {role[0] && role[0][0] === 'admin' ? (
                                                    <Button disabled>Admin</Button>
                                                ) : (
                                                        <Button
                                                            color="primary"
                                                            onClick={this.handleOpen(item.email)}>
                                                            {currentRole}
                                                        </Button>
                                                    )}
                                            </td>
                                            <td>{item.email}</td>
                                            <td>
                                                {item.metadata.lastSignInTime
                                                    ? moment(item.metadata.lastSignInTime).format(
                                                        'MMM Do h:mm a'
                                                    )
                                                    : '-'}
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                        <br />

                        <Dialog open={this.state.openOrg} onClose={this.handleCloseOrg}>
                            <DialogTitle>Set Org Access</DialogTitle>
                            <DialogContent>
                                <DialogContentText>{this.state.editEmail}</DialogContentText>
                                <NativeSelect
                                    onChange={this.onSelectChangeOrg}
                                    value={this.state.selectOrg}>
                                    <option value="disabledselect" disabled>
                                        Please Select Org
                  </option>
                                    <optgroup label="Congressional">
                                        <option value="NC09">NC-09</option>
                                        <option value="NY22">NY-22</option>
                                    </optgroup>
                                    <optgroup label="Admin">
                                        <option value="DEMO">Demo</option>
                                        <option value="remove">Remove Roles</option>
                                    </optgroup>
                                </NativeSelect>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={this.handleCloseOrg('cancel')} color="primary">
                                    Cancel
                </Button>
                                <Button onClick={this.handleCloseOrg('save')} color="primary">
                                    Save
                </Button>
                            </DialogActions>
                        </Dialog>



                        <Dialog open={this.state.open} onClose={this.handleClose}>
                            <DialogTitle>Set Staging Location Access</DialogTitle>
                            <DialogContent>
                                <DialogContentText>{this.state.editEmail}</DialogContentText>
                                <NativeSelect
                                    onChange={this.onSelectChange}
                                    value={this.state.selectStation}>
                                    <option value="disabledselect" disabled>
                                        Please Select Option
                  </option>
                                    <optgroup label="Administrative">
                                        <option value="director">Director</option>
                                        <option value="staff">Staff</option>
                                        <option value="remove">Remove Roles</option>
                                    </optgroup>
                                    {Object.entries(this.state.stations).map((item, idx) => {
                                        return (
                                            <optgroup key={`optgroup-${idx}`} label={item[0]}>
                                                {item[1].map((name, idx2) => {
                                                    const valFormat = name
                                                        .replace(' ', '_')
                                                        .replace('(', '')
                                                        .replace(')', '')
                                                        .toLowerCase()
                                                    return (
                                                        <option key={`station-${idx2}`} value={item[0].toLowerCase() + '|' + valFormat}>
                                                            {name}
                                                        </option>
                                                    )
                                                })}
                                            </optgroup>
                                        )
                                    })}
                                </NativeSelect>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={this.handleClose('cancel')} color="primary">
                                    Cancel
                </Button>
                                <Button onClick={this.handleClose('save')} color="primary">
                                    Save
                </Button>
                            </DialogActions>
                        </Dialog>
                    </div>
                ) : this.state.isAdmin === null ? (
                    <div>Checking credentials...</div>
                ) : (
                            <div>Not Authorized...</div>
                        )}
            </Paper>
        )
    }
}

// UsersPage.propTypes = {
// 	children: PropTypes.object // from react-router
// }

export default PeoplePage

// <td>{item.displayName ? item.displayName : ''}</td>

// <td>{role[0] && role[0][0] === 'admin' ? <Button disabled>{item.email}</Button> : <Button color="primary" onClick={this.handleOpen(item.email)}>{item.email}</Button>}</td>
