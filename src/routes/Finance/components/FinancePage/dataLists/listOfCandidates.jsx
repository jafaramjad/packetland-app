export default [
    { committeeId: "C00632828", committeeName: "CLAUDIA TENNEY FOR CONGRESS" },
    { committeeId: "C00561183", committeeName: "TENNEY FOR CONGRESS" },
    { committeeId: "C00638718", committeeName: "TENNEY VICTORY FUND" },
    { committeeId: "C00682070", committeeName: "TENNEY NY VICTORY FUND" },
    { committeeId: "C00648725", committeeName: "BRINDISI FOR CONGRESS" },
    { committeeId: "C00659789", committeeName: "BRINDISI VICTORY FUND" },
    { committeeId: "C00610642", committeeName: "KIM MYERS FOR CONGRESS" },
    { committeeId: "C00451005", committeeName: "RICHARD HANNA FOR CONGRESS COMMITTEE" },
]