import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Downshift from 'downshift'
import deburr from 'lodash/deburr'
import classes from './FinancePage.scss'
import listOfCandidates from './dataLists/listOfCandidates'

import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import InputAdornment from '@material-ui/core/InputAdornment'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Popper from '@material-ui/core/Popper'
import Chip from '@material-ui/core/Chip'

import keycode from 'keycode'

const API_KEY = 'vZLLEk3hXiiAdBKfyP6g3qv8bhbnZm9NWV2eL9co'

const commID = listOfCandidates

function renderInput(inputProps) {
  const { InputProps, classes, ref, ...other } = inputProps

  return (
    <TextField
      InputProps={{
        inputRef: ref,
        classes: {
          root: classes.inputRoot
        },
        ...InputProps
      }}
      {...other}
    />
  )
}

function renderSuggestion({
  suggestion,
  index,
  itemProps,
  highlightedIndex,
  selectedItem
}) {
  const isHighlighted = highlightedIndex === index
  const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.committeeName}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400
      }}>
      {suggestion.committeeName}
    </MenuItem>
  )
}
// renderSuggestion.propTypes = {
// 	highlightedIndex: PropTypes.number,
// 	index: PropTypes.number,
// 	itemProps: PropTypes.object,
// 	selectedItem: PropTypes.string,
// 	suggestion: PropTypes.shape({ label: PropTypes.string }).isRequired,
// };

function getSuggestions(value) {
  const inputValue = deburr(value.trim()).toLowerCase()
  const inputLength = inputValue.length
  let count = 0

  return inputLength === 0
    ? []
    : commID.filter(suggestion => {
        const keep =
          count < 5 &&
          suggestion.committeeName.slice(0, inputLength).toLowerCase() ===
            inputValue

        if (keep) {
          count += 1
        }

        return keep
      })
}

class FinancePage extends Component {
  constructor() {
    super()

    this.state = {
      inputValue: '',
      selectedItem: [],
      results: '',
      loading: false,
      year: '',
      params: {
        firstName: '',
        lastName: '',
        contributor_city: '',
        contributor_zip: '',
        contributor_state: '',
        contributor_occupation: '',
        contributor_employer: '',
        two_year_transaction_period: '',
        // min_date: '',
        // max_date: '',
        min_amount: '200',
        committee_id: ''
      },
      searchTerm: '',
      searchTermRes: '',
      showModal: false
    }
  }

  getResultsFromFEC = async queries => {
    const baseURL = `https://api.open.fec.gov/v1/schedules/schedule_a/?api_key=${API_KEY}&per_page=100&sort=contribution_receipt_date&is_individual=true`
    this.setState({ loading: true })
    try {
      var res = await fetch(`${baseURL + queries}`)
      var json = await res.json()
      this.setState({
        results: json.results,
        loading: false
      })
    } catch (error) {
      console.log(error)
    }
  }

  newSearch = () => {
    this.setState({ results: '' })
  }

  /* 	handleOpenModal = () => {
			this.setState({ showModal: true });
		}
	
		handleCloseModal = () => {
			this.setState({ showModal: false });
		} */

  onKeyPress(event) {
    if (event.which === 13) {
      event.preventDefault()
    }
  }

  onChange = e => {
    if (e.target.name === 'searchTerm' || e.target.name === 'searchTermRes') {
      this.setState({ [e.target.name]: e.target.value })
    } else {
      this.setState({
        params: { ...this.state.params, [e.target.name]: e.target.value }
      })
    }
  }

  buildUrl = e => {
    e.preventDefault()
    const { params } = this.state
    var queryString = ''
    var combineName = []
    for (let i in params) {
      if (i === 'firstName' || i === 'lastName') {
        if (params[i]) {
          combineName.push(params[i])
        }
      } else {
        if (params[i]) {
          queryString += `&${i}=${params[i]}`
        }
      }
    }
    if (combineName.length > 0) {
      queryString += `&contributor_name=${combineName.join('%20')}`
    }
    this.props.getFEC(queryString)
  }

  setCommitteeID = e => {
    this.setState({
      params: { ...this.state.params, committee_id: e.currentTarget.dataset.id }
    })
  }

  /* 	searchCommittee = (e) => {
			e.preventDefault()
			const result = listOfCandidates.filter(com => {
				return com.committeeName.toLowerCase().includes(this.state.searchTerm.toLowerCase())
			})
			this.setState({ searchTermRes: result });
		} */
  /* 
		handleChange = event => {
			this.setState({ [event.target.name]: event.target.value });
		}; */

  handleChange = name => event => {
    this.setState({ [name]: event.target.value })
  }

  /**
   * DOWNSHIFT FUNC
   */

  handleKeyDown = event => {
    const { inputValue, selectedItem } = this.state
    if (
      selectedItem.length &&
      !inputValue.length &&
      keycode(event) === 'backspace'
    ) {
      this.setState({
        selectedItem: selectedItem.slice(0, selectedItem.length - 1)
      })
    }
  }

  handleInputChange = event => {
    this.setState({ inputValue: event.target.value })
  }

  handleChangeDownshift = item => {
    let { selectedItem } = this.state

    if (selectedItem.indexOf(item) === -1) {
      selectedItem = [...selectedItem, item]
    }

    this.setState({
      inputValue: '',
      selectedItem
    })
  }

  handleDelete = item => () => {
    this.setState(state => {
      const selectedItem = [...state.selectedItem]
      selectedItem.splice(selectedItem.indexOf(item), 1)
      return { selectedItem }
    })
  }

  render() {
    const { params, inputValue, selectedItem } = this.state

    if (this.state.results) {
      return null // <Redirect to='./fecResults'></Redirect>
    }
    if (this.state.loading) {
      return (
        <h1 className="text-center">
          Loading
          <p className="loader" />
        </h1>
      )
    }
    let searchTermList = this.state.searchTermRes
      ? this.state.searchTermRes.map(name => (
          <li
            className="committeeList"
            data-dismiss="modal"
            key={name.committeeName}
            onClick={this.setCommitteeID}
            data-id={name.committeeId}>
            {name.committeeName}
          </li>
        ))
      : ''

    return (
      <div className={classes.container}>
        <h2>Donor Research</h2>

        <form
          onSubmit={this.buildUrl}
          onKeyPress={this.onKeyPress}
          className="card-body">
          <ExpansionPanel defaultExpanded>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>SEARCH</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={24}>
                <Grid item xs={12} md={2}>
                  <TextField
                    select
                    label="Year"
                    value={this.state.year}
                    onChange={this.handleChange('year')}
                    className={classes.selectYear}
                    SelectProps={{
                      native: false,
                      MenuProps: {
                        className: classes.textField
                      }
                    }}>
                    <MenuItem value={2018}>2018</MenuItem>
                    <MenuItem value={2016}>2016</MenuItem>
                    <MenuItem value={2014}>2014</MenuItem>
                    <MenuItem value={2012}>2012</MenuItem>
                  </TextField>
                </Grid>

                <Grid item xs={12} md={4}>
                  <FormControl fullWidth>
                    <InputLabel htmlFor="minimum-amount">
                      Minimum Amount
                    </InputLabel>
                    <Input
                      label="Minimum Amount"
                      className={classes.textField}
                      value={this.state.params.min_amount}
                      onChange={this.handleChange('params.min_amount')}
                      startAdornment={
                        <InputAdornment position="start">$</InputAdornment>
                      }
                    />
                  </FormControl>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Downshift
                    id="downshift-multiple"
                    inputValue={inputValue}
                    onChange={this.handleChangeDownshift}
                    selectedItem={selectedItem}>
                    {({
                      getInputProps,
                      getItemProps,
                      isOpen,
                      inputValue: inputValue2,
                      selectedItem: selectedItem2,
                      highlightedIndex
                    }) => (
                      <div>
                        {renderInput({
                          fullWidth: true,
                          classes,
                          InputProps: getInputProps({
                            startAdornment: selectedItem.map(item => (
                              <Chip
                                key={item}
                                tabIndex={-1}
                                label={item}
                                className={classes.chip}
                                onDelete={this.handleDelete(item)}
                              />
                            )),
                            onChange: this.handleInputChange,
                            onKeyDown: this.handleKeyDown,
                            placeholder: 'Search by typing name'
                          }),
                          label: 'Committee ID'
                        })}
                        {isOpen ? (
                          <Paper className={classes.paper} square>
                            {getSuggestions(inputValue2).map(
                              (suggestion, index) =>
                                renderSuggestion({
                                  suggestion,
                                  index,
                                  itemProps: getItemProps({
                                    item: suggestion.committeeId
                                  }),
                                  highlightedIndex,
                                  selectedItem: selectedItem2
                                })
                            )}
                          </Paper>
                        ) : null}
                      </div>
                    )}
                  </Downshift>
                  {/* 
									<FormControl fullWidth className={classes.margin}>
										<InputLabel htmlFor="adornment-amount">Search for Committee ID</InputLabel>
										<Input

											className={classes.textField}
											value={this.state.params.searchTerm}
											onChange={this.onChange}

										/>
									</FormControl> */}
                </Grid>

                {/* 
								<Grid item xs={12} md={6}>
									<FormControl fullWidth className={classes.margin}>
										<InputLabel htmlFor="adornment-amount">Committee ID</InputLabel>
										<Input
											label="Committee ID"
											className={classes.textField}
											value={this.state.params.committee_id}
											onChange={this.onChange}
											startAdornment={<InputAdornment position="start">#</InputAdornment>}
										/>
									</FormControl>
								</Grid> */}
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>

          <br />

          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>ADVANCED</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={8}>
                <Grid item xs={12} md={6}>
                  <TextField
                    label="First Name"
                    className={classes.textField}
                    // value={this.state.params.firstName}
                    onChange={this.handleChange('params.firstName')}
                    margin="normal"
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    label="Last Name"
                    className={classes.textField}
                    // value={this.state.params.lastName}
                    onChange={this.handleChange('params.lastName')}
                    margin="normal"
                  />
                </Grid>

                <Grid item xs={12} md={4}>
                  <TextField
                    label="City"
                    className={classes.textField}
                    value={this.state.params.contributor_city}
                    onChange={this.handleChange('params.contributor_city')}
                    margin="normal"
                  />
                </Grid>

                <Grid item xs={12} md={4}>
                  <TextField
                    label="State"
                    className={classes.textField}
                    value={this.state.params.contributor_state}
                    onChange={this.handleChange('params.contributor_state')}
                    margin="normal"
                  />
                </Grid>

                <Grid item xs={12} md={4}>
                  <TextField
                    label="Zip"
                    className={classes.textField}
                    value={this.state.params.contributor_zip}
                    onChange={this.handleChange('params.contributor_zip')}
                    margin="normal"
                  />
                </Grid>

                <Grid item xs={12} md={6}>
                  <TextField
                    label="Occupation"
                    className={classes.textField}
                    value={this.state.params.contributor_occupation}
                    onChange={this.handleChange(
                      'params.contributor_occupation'
                    )}
                    margin="normal"
                  />
                </Grid>

                <Grid item xs={12} md={6}>
                  <TextField
                    label="Employer"
                    className={classes.textField}
                    value={this.state.params.contributor_employer}
                    onChange={this.handleChange('params.contributor_employer')}
                    margin="normal"
                  />
                </Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <br />
          <br />

          <Button variant="contained" color="primary">
            Search
          </Button>

          {/*
					<label>First name</label>
					<input type="text" className="form-control" placeholder="First name" name="firstName" onChange={this.onChange} value={params.firstName} />

					<label>Last name</label>
					<input type="text" className="form-control" placeholder="Last name" name="lastName" onChange={this.onChange} value={params.lastName} />

					<label>City</label>
					<input type="text" className="form-control" placeholder="City" name="contributor_city" onChange={this.onChange} value={params.contributor_city} />


					<label>State</label>
					<input type="text" className="form-control" placeholder="State" name="contributor_state" onChange={this.onChange} value={params.contributor_state} />


					<label>Zip</label>
					<input type="text" className="form-control" placeholder="Zip" name="contributor_zip" onChange={this.onChange} value={params.contributor_zip} />


					<label>Occupation</label>
					<input type="text" className="form-control" placeholder="Occupation" name="contributor_occupation" onChange={this.onChange} value={params.contributor_occupation} />

					<label>Employer</label>
					<input type="text" className="form-control" placeholder="Employer" name="contributor_employer" onChange={this.onChange} value={params.contributor_employer} />



					<label>Minimum Amount</label>
					<input type="text" className="form-control" placeholder="Minumum Amount" name="min_amount" onChange={this.onChange} value={params.min_amount} />



					<label>Committee ID</label>
					<input type="text" className="form-control" placeholder="Committee ID" name="committee_id" onChange={this.onChange} value={params.committee_id} />

					<label>Year</label>
					<select className="form-control" placeholder="Year" name="two_year_transaction_period" onChange={this.onChange} value={params.two_year_transaction_period}>
						<option value=""></option>
						<option value="2018">2018</option>
						<option value="2016">2016</option>
						<option value="2014">2014</option>
						<option value="2012">2012</option>
					</select>

*/}
        </form>
      </div>
    )
  }
}

FinancePage.propTypes = {
  children: PropTypes.object // from react-router
}

export default FinancePage
