import React, { Component } from 'react';
import ChartData from './chart';
import ReactTable from "react-table";
import 'react-table/react-table.css';

class FecResults extends Component {

  render(){
    //const tableRows = this.props.appState.results ? this.props.appState.results.map(contribution => ChartData(contribution)) : null;

    const results = this.props.appState.results;
    console.log(results);
    
    //const data = this.props.appState.results;
   
    //const data = [this.props.appState.results.map((item,idx) =>  {item[idx]})];

    console.log(typeof(results));

    // const data = [{
    //   contributor_first_name: 'Tanner',
    //   contributor_last_name: 'Linsley',
    //   contributor_state: 'NY',
    //   contributor_zip: '13413'
    // }];

    let data;

    if(typeof(results) === 'object') {
      console.log('its an object woot!');
      data = results.map(item => item);
    }
    
    //const data = [results.forEach(item => item)];

/*
    const data = [
        {
          "candidate_office_state": null,
          "candidate_middle_name": null,
          "conduit_committee_street2": null,
          "contribution_receipt_amount": 225,
          "increased_limit": null,
          "entity_type": "IND",
          "candidate_last_name": null,
          "unused_contbr_id": "C00632133",
          "donor_committee_name": null,
          "back_reference_schedule_name": "SA11AI",
          "contributor_middle_name": null,
          "memo_code": null,
          "contributor_aggregate_ytd": 225,
          "memo_code_full": null,
          "election_type_full": null,
          "original_sub_id": null,
          "contributor_suffix": null,
          "pdf_url": "http://docquery.fec.gov/cgi-bin/fecimg/?201806149113696878",
          "link_id": 4061520181570725400,
          "contributor_state": "NY",
          "conduit_committee_name": null,
          "receipt_type_desc": "EARMARKED CONTRIBUTION",
          "contributor": {
            "street_1": "700 13TH STREET NW, SUITE 600",
            "cycle": 2018,
            "committee_id": "C00632133",
            "organization_type": null,
            "treasurer_name": "TODRAS-WHITEHILL, ETHAN",
            "state": "DC",
            "designation": "U",
            "street_2": null,
            "city": "WASHINGTON",
            "candidate_ids": [],
            "designation_full": "Unauthorized",
            "organization_type_full": null,
            "state_full": "District Of Columbia",
            "party": null,
            "cycles": [
              2018
            ],
            "party_full": null,
            "zip": "20005",
            "committee_type_full": "PAC with Non-Contribution Account - Nonqualified",
            "name": "SWING LEFT",
            "filing_frequency": "M",
            "committee_type": "V"
          },
          "national_committee_nonfederal_account": null,
          "contributor_last_name": "RYDER",
          "contributor_street_2": null,
          "entity_type_desc": "INDIVIDUAL",
          "line_number": "11AI",
          "memoed_subtotal": false,
          "sub_id": "4061520181570732523",
          "contributor_employer": "ROCHESTER INSTITUTE OF TECHNOLOGY",
          "committee_id": "C00648725",
          "image_number": "201806149113696878",
          "back_reference_transaction_id": "VTR1MDPH415E",
          "candidate_office_district": null,
          "schedule_type": "SA",
          "conduit_committee_id": null,
          "amendment_indicator_desc": "ADD",
          "candidate_office": null,
          "fec_election_type_desc": "PRIMARY",
          "contributor_first_name": "DAVID C",
          "two_year_transaction_period": 2018,
          "is_individual": true,
          "committee_name": null,
          "filing_form": "F3",
          "candidate_name": null,
          "load_date": "2018-06-16T01:31:23.587000+00:00",
          "contributor_name": "RYDER, DAVID C",
          "report_type": "12P",
          "contribution_receipt_date": "2017-04-28T00:00:00",
          "conduit_committee_state": null,
          "contributor_city": "ROCHESTER",
          "line_number_label": "Contributions From Individuals/Persons Other Than Political Committees",
          "candidate_first_name": null,
          "contributor_occupation": "PROFESSOR",
          "contributor_street_1": "48 EASTLAND AVE",
          "conduit_committee_zip": null,
          "receipt_type": "15E",
          "file_number": 1238040,
          "report_year": 2018,
          "candidate_prefix": null,
          "conduit_committee_street1": null,
          "fec_election_year": "2018",
          "transaction_id": "VTR1MDPH415",
          "schedule_type_full": "ITEMIZED RECEIPTS",
          "candidate_id": null,
          "receipt_type_full": null,
          "amendment_indicator": "A",
          "committee": {
            "street_1": "PO BOX 165",
            "cycle": 2018,
            "committee_id": "C00648725",
            "organization_type": null,
            "treasurer_name": "MAY, JENNIFER",
            "state": "NY",
            "designation": "P",
            "street_2": null,
            "city": "UTICA",
            "candidate_ids": [
              "H8NY22151"
            ],
            "designation_full": "Principal campaign committee",
            "organization_type_full": null,
            "state_full": "New York",
            "party": "DEM",
            "cycles": [
              2018
            ],
            "party_full": "DEMOCRATIC PARTY",
            "zip": "13503",
            "committee_type_full": "House",
            "name": "BRINDISI FOR CONGRESS",
            "filing_frequency": "Q",
            "committee_type": "H"
          },
          "contributor_prefix": null,
          "conduit_committee_city": null,
          "candidate_office_state_full": null,
          "contributor_zip": "146181030",
          "candidate_office_full": null,
          "candidate_suffix": null,
          "election_type": "P2018",
          "memo_text": "* EARMARKED CONTRIBUTION: SEE BELOW",
          "contributor_id": "C00632133"
        },
        {
          "candidate_office_state": null,
          "candidate_middle_name": null,
          "conduit_committee_street2": null,
          "contribution_receipt_amount": 225,
          "increased_limit": null,
          "entity_type": "IND",
          "candidate_last_name": null,
          "unused_contbr_id": "C00632133",
          "donor_committee_name": null,
          "back_reference_schedule_name": "SA11AI",
          "contributor_middle_name": null,
          "memo_code": null,
          "contributor_aggregate_ytd": 225,
          "memo_code_full": null,
          "election_type_full": null,
          "original_sub_id": null,
          "contributor_suffix": null,
          "pdf_url": "http://docquery.fec.gov/cgi-bin/fecimg/?201806149113696800",
          "link_id": 4061520181570725400,
          "contributor_state": "NY",
          "conduit_committee_name": null,
          "receipt_type_desc": "EARMARKED CONTRIBUTION",
          "contributor": {
            "street_1": "700 13TH STREET NW, SUITE 600",
            "cycle": 2018,
            "committee_id": "C00632133",
            "organization_type": null,
            "treasurer_name": "TODRAS-WHITEHILL, ETHAN",
            "state": "DC",
            "designation": "U",
            "street_2": null,
            "city": "WASHINGTON",
            "candidate_ids": [],
            "designation_full": "Unauthorized",
            "organization_type_full": null,
            "state_full": "District Of Columbia",
            "party": null,
            "cycles": [
              2018
            ],
            "party_full": null,
            "zip": "20005",
            "committee_type_full": "PAC with Non-Contribution Account - Nonqualified",
            "name": "SWING LEFT",
            "filing_frequency": "M",
            "committee_type": "V"
          },
          "national_committee_nonfederal_account": null,
          "contributor_last_name": "MURRAY",
          "contributor_street_2": null,
          "entity_type_desc": "INDIVIDUAL",
          "line_number": "11AI",
          "memoed_subtotal": false,
          "sub_id": "4061520181570732288",
          "contributor_employer": "GOOGLE",
          "committee_id": "C00648725",
          "image_number": "201806149113696800",
          "back_reference_transaction_id": "VTR1MDQH295E",
          "candidate_office_district": null,
          "schedule_type": "SA",
          "conduit_committee_id": null,
          "amendment_indicator_desc": "ADD",
          "candidate_office": null,
          "fec_election_type_desc": "PRIMARY",
          "contributor_first_name": "COLIN",
          "two_year_transaction_period": 2018,
          "is_individual": true,
          "committee_name": null,
          "filing_form": "F3",
          "candidate_name": null,
          "load_date": "2018-06-16T01:30:53.729000+00:00",
          "contributor_name": "MURRAY, COLIN",
          "report_type": "12P",
          "contribution_receipt_date": "2017-05-08T00:00:00",
          "conduit_committee_state": null,
          "contributor_city": "ASTORIA",
          "line_number_label": "Contributions From Individuals/Persons Other Than Political Committees",
          "candidate_first_name": null,
          "contributor_occupation": "SOFTWARE ENGINEER",
          "contributor_street_1": "2516 30TH DR",
          "conduit_committee_zip": null,
          "receipt_type": "15E",
          "file_number": 1238040,
          "report_year": 2018,
          "candidate_prefix": null,
          "conduit_committee_street1": null,
          "fec_election_year": "2018",
          "transaction_id": "VTR1MDQH295",
          "schedule_type_full": "ITEMIZED RECEIPTS",
          "candidate_id": null,
          "receipt_type_full": null,
          "amendment_indicator": "A",
          "committee": {
            "street_1": "PO BOX 165",
            "cycle": 2018,
            "committee_id": "C00648725",
            "organization_type": null,
            "treasurer_name": "MAY, JENNIFER",
            "state": "NY",
            "designation": "P",
            "street_2": null,
            "city": "UTICA",
            "candidate_ids": [
              "H8NY22151"
            ],
            "designation_full": "Principal campaign committee",
            "organization_type_full": null,
            "state_full": "New York",
            "party": "DEM",
            "cycles": [
              2018
            ],
            "party_full": "DEMOCRATIC PARTY",
            "zip": "13503",
            "committee_type_full": "House",
            "name": "BRINDISI FOR CONGRESS",
            "filing_frequency": "Q",
            "committee_type": "H"
          },
          "contributor_prefix": null,
          "conduit_committee_city": null,
          "candidate_office_state_full": null,
          "contributor_zip": "111022714",
          "candidate_office_full": null,
          "candidate_suffix": null,
          "election_type": "P2018",
          "memo_text": "* EARMARKED CONTRIBUTION: SEE BELOW",
          "contributor_id": "C00632133"
        }
    ];
  */

    /*
    const columns = [
      {
      Header: 'Name',
      accessor: 'name' // String-based value accessors!
    }, {
      Header: 'Age',
      accessor: 'age',
      Cell: props => <span className='number'>{props.value}</span> // Custom cell components!
    }, {
      id: 'friendName', // Required because our accessor is not a string
      Header: 'Friend Name',
      accessor: d => d.friend.name // Custom value accessors!
    }, {
      Header: props => <span>Friend Age</span>, // Custom header components!
      accessor: 'friend.age'
    }];
  */

 const columns = [
  {
    Header: 'First Name',
    accessor: 'contributor_first_name'
  },
  {
    Header: 'Last Name',
    accessor: 'contributor_last_name',
  },
  {
    Header: 'State',
    accessor: 'contributor_state',
  },
  {
    Header: 'City',
    accessor: 'contributor_city',
  },

  {
    Header: 'Zip',
    accessor: 'contributor_zip'
  },
  {
    Header: 'YTD',
    accessor: 'contributor_aggregate_ytd',
  },
  {
    Header: 'Amount',
    accessor: 'contribution_receipt_amount',
  },
  {
    Header: 'Committee Name',
    accessor: 'committee.name',
  },

  {
    Header: 'Committe State',
    accessor: 'committee.state'
  },
  {
    Header: 'Memo Text',
    accessor: 'memo_text',
  },
  {
    Header: 'Date',
    accessor: 'contribution_receipt_date',
  },
  {
    Header: 'Contributor Occupation',
    accessor: 'contributor_occupation',
  },
  {
    Header: 'Contributor Employer',
    accessor: 'contributor_employer'
  },
  {
    Header: 'PDF URL',
    accessor: 'pdf_url',
  }
];
  

    return(
      <main className="container">
      <h2 className="text-center homeMargin">Contributor</h2>
      <div className="table-responsive">

          <ReactTable
    data={data}
    columns={columns}
  />

      {/*
        <table className="table table-striped">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>State</th>
              <th>City</th>

              <th>Zip</th>
              <th>YTD</th>
              <th>Amount</th>
              <th>Committee Name</th>

              <th>Committee State</th>
              <th>Memo Text</th>
              <th>Date</th>
              <th>Contributor Occupation</th>

              <th>Contributor Employer</th>
              <th>PDF URL</th>
            </tr>
          </thead>
          <tbody>
            {tableRows}
          </tbody>
        </table>
        */}
        </div>
      </main >
    )
   }
 }

export default FecResults;
