import React, { Component } from 'react'
import { Link } from 'react-router'
import Typography from '@material-ui/core/Typography'
import { paths } from 'constants'
import classes from './HomePage.scss'
import ListAltIcon from '@material-ui/icons/ListAlt'
import HowToVoteIcon from '@material-ui/icons/HowToVoteOutlined'
import AssignmentIcon from '@material-ui/icons/AssignmentTurnedInOutlined'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import PollIcon from '@material-ui/icons/PollOutlined'
import PeopleIcon from '@material-ui/icons/PeopleOutlined'
import SuperVisorAccountIcon from '@material-ui/icons/SupervisorAccountOutlined'

// import EventAvailableIcon from '@material-ui/icons/EventAvailableOutlined'
import DirectionsWalkIcon from '@material-ui/icons/DirectionsWalkOutlined'
import Grid from '@material-ui/core/Grid'

import HomeNoAuth from './HomeNoAuth'
import HomeNoOrg from './HomeNoOrg'

// const authWrapperUrl = 'https://github.com/mjrussell/redux-auth-wrapper'
// const reactRouterUrl = 'https://github.com/ReactTraining/react-router'

/* export const Home = () => { */
// console.log(profileEmail)

class Home extends Component {
  constructor(props) {
    super(props)
    // console.log(this.props)

    this.state = {
      viewDashboard: props.accessDash,
      hasRole: props.accessRole
    }
  }

  /* 	componentWillMount() {
			// console.log(this.state.userAuthorized)
	
			this.state.userAuthorized ? null : this.props.handleLogout()
		}
	 */
  componentDidUpdate = prevProps => {
    // console.log(this.props)
    if (this.props.accessDash !== prevProps.accessDash) {
      this.setState({
        viewDashboard: this.props.accessDash
      })
    }

    if (this.props.accessRole !== prevProps.accessRole) {
      this.setState({
        hasRole: this.props.accessRole
      })
    }
  }

  componentDidMount() {
    /* this.setState({
		
		}) */
  }

  render() {
    const { authExists, profileEmail, accessSite } = this.props

    return (
      <Grid container className={classes.container}>
        {authExists && accessSite ? (
          <Grid
            container
            spacing={40}
            direction="row"
            alignItems="center"
            justify="center">
            {this.state.hasRole !== 'admin' &&
              this.state.hasRole === null ? (
                <HomeNoOrg />
              ) : null}

            {this.state.hasRole === 'admin' ||
              this.state.hasRole !== null ? (
                <Grid item xs={6} sm={3}>
                  <Typography
                    variant="h6"
                    color="inherit"
                    key={'daily'}
                    className={classes.menuIcon}
                    component={Link}
                    to={'/daily'}>
                    <AssignmentIcon fontSize="large" />
                    <br />
                    DAILY
                </Typography>
                </Grid>
              ) : null}
            {/* 
            {this.state.hasRole === 'admin' || this.state.hasRole === 'staff' ? (
              <Grid item xs={6} sm={3}>
                <Typography
                  variant="title"
                  color="inherit"
                  key={'gotv'}
                  className={classes.menuIcon}
                  component={Link}
                  to={'/gotv'}>
                  <HowToVoteIcon fontSize="large" />
                  <br />
                  GOTV
							</Typography>
              </Grid>
            ) : null} */}

            {/*   {this.state.hasRole === 'admin' || this.state.hasRole === 'staff' ? (
              <Grid item xs={6} sm={3}>
                <Typography
                  variant="title"
                  color="inherit"
                  key={'gotv'}
                  className={classes.menuIcon}
                  component={Link}
                  to={'/vols'}>
                  <DirectionsWalkIcon fontSize="large" />
                  <br />
                  VOLUNTEERS
								</Typography>
              </Grid>
            ) : null} */}

            {this.state.hasRole === 'admin' || this.state.hasRole === 'director' ? (
              <Grid item xs={6} sm={3}>
                <Typography
                  variant="h6"
                  color="inherit"
                  key={'dashboard'}
                  className={classes.menuIcon}
                  component={Link}
                  to={'/dashboard'}>
                  <PollIcon fontSize="large" />
                  <br />
                  DASHBOARD
								</Typography>
              </Grid>
            ) : null}


            {this.state.hasRole === 'admin' || this.state.hasRole === 'director' ? (
              <Grid item xs={6} sm={3}>
                <Typography
                  variant="h6"
                  color="inherit"
                  key={'dashboard'}
                  className={classes.menuIcon}
                  component={Link}
                  to={'/people'}>
                  <SuperVisorAccountIcon fontSize="large" />
                  <br />
                  PEOPLE
								</Typography>
              </Grid>
            ) : null}

            {/* 
            {this.state.hasRole === 'admin' || this.state.hasRole === 'staff' ? (
              <Grid item xs={6} sm={3}>
                <Typography
                  variant="title"
                  color="inherit"
                  key={'users'}
                  className={classes.menuIcon}
                  component={Link}
                  to={'/users'}>
                  <PeopleIcon fontSize="large" />
                  <br />
                  USERS
							</Typography>
              </Grid>

            ) : null} */}

            {/*  {this.state.hasRole === 'admin' || this.state.hasRole === 'staff' ? (
              <Grid item xs={6} sm={3}>
                <Typography
                  variant="title"
                  color="inherit"
                  key={'users'}
                  className={classes.menuIcon}
                  component={Link}
                  to={'/users'}>
                  <PeopleIcon fontSize="large" />
                  <br />
                  SETTINGS
							</Typography>
              </Grid>

            ) : null} */}
          </Grid>
        ) : (
            <HomeNoAuth />
          )}
      </Grid>
    )
  }
}

export default Home

/*

		<Grid item xs={6} sm={3}>
							<Typography
								variant="title"
								color="inherit"
								key={'media'}
								className={classes.menuIcon}
								component={Link}
								to={'/media'}>
								<ListAltIcon fontSize="large" />
								<br />
								MEDIA
							</Typography>
						</Grid>

						<Grid item xs={6} sm={3}>
							<Typography
								variant="title"
								color="inherit"
								key={'finance'}
								className={classes.menuIcon}
								component={Link}
								to={'/finance'}>
								<AttachMoneyIcon fontSize="large" />
								<br />
								DONORS
							</Typography>
						</Grid>

						*/
