import GOTVPage from './GOTVPage'
import enhance from './GOTVPage.enhancer'

export default enhance(GOTVPage)
