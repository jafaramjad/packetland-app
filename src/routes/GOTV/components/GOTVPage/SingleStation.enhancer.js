/* import { compose } from 'redux'
import { connect } from 'react-redux'
import { GOTV_PATH } from 'constants'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { firestoreConnect } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'

export default compose(
    // redirect to /login if user is not logged in
    // UserIsAuthenticated,
    // Map auth uid from state to props
    connect(({ firebase: { auth: { uid } } }) => ({ uid })),
    // Wait for uid to exist before going further
    spinnerWhileLoading(['uid']),

    // Add props.router
    withRouter,

    withStateHandlers(
        // Setup initial state
        ({ initialDialogOpen = false }) => ({
            newDialogOpen: initialDialogOpen
        }),
        // Add state handlers as props
        {
            toggleDialog: ({ newDialogOpen }) => () => ({
                newDialogOpen: !newDialogOpen
            })
        }
    ),
    // Add handlers as props
    withHandlers({

        clickDay: ({ router }) => (countyName, stationName, eventId) => {
            // console.log('clicking thing...')
            router.push(`${GOTV_PATH}/${countyName}-${stationName}/${eventId}`)
        }

    }),
    pure // shallow equals comparison on props (prevent unessesary re-renders)
)
 */
