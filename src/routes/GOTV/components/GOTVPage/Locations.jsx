import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classes from './GOTVPage.scss'
import Stations from './Stations'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline'
import Collapse from '@material-ui/core/Collapse'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'
import ExpandMore from '@material-ui/icons/ExpandMore'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'


import { stations, locations } from '../../../../constants/places'
import { Typography } from '@material-ui/core'

// const locations = ["Oneida", "Herkimer", "Oswego", "Madison", "Chenango", "Cortland", "Broome", "Tioga"]

/* const stations = {
	Oneida: ["Utica", "New Hartford", "Whitestown", "Rome", "Trenton", "Camden"],
	Oswego: ["Mexico", "Central Square"],
	Herkimer: ["Ilion", "Little Falls"],
	Madison: ["Cazenovia", "Oneida", "Hamilton (phones)", "Hamilton (doors)"],
	Chenango: ["Norwich"],
	Cortland: ["Cortland"],
	Broome: ["Binghamton", "Endicott", "Whitney Point", "Windsor"],
	Tioga: ["Owego"],
	"Out of District": ["Out of District"]
} */

class Locations extends Component {
	constructor(props) {
		super(props)
		this.state = {
			open: {
				0: false,
				1: false,
				2: false,
				3: false,
				4: false,
				5: false,
				6: false,
				7: false,
				8: false
			}
		}

		// console.log(this.props)
	}

	handleClick = idx => {
		const newOpen = [...this.state.open]
		newOpen[idx] = !newOpen[idx]

		this.setState({
			open: newOpen
		})
	}
	convertEpoch(epoch) {
		let d = new Date(0)
		return d.setUTCSeconds(epoch)
	}

	render() {
		if (this.props.hasRole === 'admin' || this.props.hasRole === 'staff') {
			return (
				<List className={classes.listCounties} component="ul">
					{locations.map((item, idx) => (
						<div key={idx}>
							<ListItem
								divider
								button
								onClick={() => {
									this.handleClick(idx)
								}}>
								<ListItemIcon>
									{this.state.open[idx] ? <RemoveCircleOutlineIcon /> : <AddCircleOutlineIcon />}
								</ListItemIcon>
								<ListItemText
									inset
									primary={item + " County"}
								/>
							</ListItem>
							<Collapse in={this.state.open[idx]} timeout="auto" unmountOnExit>
								<Stations
									location={item}
									stationList={stations[item]}
									// item={item}
									county={item}
									clickDay={this.props.clickDay}
									clickStation={this.props.clickStation}
								/>
							</Collapse>
						</div>
					))}
				</List>
			)
		} else if (this.props.hasRole === false) {
			return (
				<Grid container justify="center" alignItems="center">
					<Grid item className={classes.centerText}>
						<Typography variant='title'>Your staging location has not been set</Typography>
						<Typography variant='subheading'>Please contact your Field Organizer to have them set one for you.</Typography>
					</Grid>
				</Grid>
			)
		} else {
			const stagingLocation = this.props.hasRole.toUpperCase().replace('_', ' ')
			let county
			let stationLinkName

			Object.entries(stations).map(item => {
				// console.log(item)
				return item[1].filter(station => {
					const stationName = station.replace(' ', '_').replace('(', '').replace(')', '').toLowerCase()
					if (stationName === this.props.hasRole) {
						county = item[0].toUpperCase()
						stationLinkName = station.replace(' ', '_')
					}
				})
				// return item === this.props.hasRole
			})


			return (
				<Grid container justify="center" alignItems="center">
					<Grid item className={classes.centerText}>
						<Typography variant='title'>Your Staging Location</Typography>
						<Typography variant='subheading'>Click the button below to continue</Typography>
						<br />
						<Button
							variant="contained"
							color="primary"
							onClick={() => {
								this.props.clickStation(county.toLowerCase(), stationLinkName.toLowerCase())
							}}>
							{county}-{stagingLocation}
						</Button>
					</Grid>
				</Grid>
			)
		}
	}
}

Locations.propTypes = {
	locations: PropTypes.array, // from react-router
	clickDay: PropTypes.func, // from enhancer (withHandlers - router)
	clickStation: PropTypes.func
}

export default Locations
