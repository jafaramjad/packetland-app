import React, { Component } from 'react'
import classes from './GOTVPage.scss'
import moment from 'moment'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline'

import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse'

import Grid from '@material-ui/core/Grid'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import SaveIcon from '@material-ui/icons/Save'
import InputAdornment from '@material-ui/core/InputAdornment'



class TimeSlots extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: {
                0: false,
                1: false,
                2: false,
                3: false,
                4: false
            },
            packetsOut: {
                0: '',
                1: '',
                2: '',
                3: ''
            },
            packetsIn: {
                0: '',
                1: '',
                2: '',
                3: ''
            },
            volunteers: {
                0: '',
                1: '',
                2: '',
                3: ''
            },
            callers: {
                0: '',
                1: '',
                2: '',
                3: ''
            }
        }
    }
    /* 
        handleChange = (name, idx) => event => {
            console.log(name + '  ' + idx)
            console.log(event)
            this.setState({
                [[name][idx]]: event.target.value
            })
        } */
    handleClick = idx => {
        const newOpen = [...this.state.open]
        newOpen[idx] = !newOpen[idx]

        this.setState({
            open: newOpen
        })
    }

    handleChangeOut = (name, idx) => evt => {
        let packetsOut = this.state.packetsOut
        packetsOut[idx] = evt.target.value
        this.setState({ packetsOut })
        // console.log(packetsOut)
    }

    handleChangeIn = (name, idx) => evt => {
        let packetsIn = this.state.packetsIn
        packetsIn[idx] = evt.target.value
        this.setState({ packetsIn })
        // console.log(packetsIn)
    }

    handleChangeVolunteers = (name, idx) => evt => {
        let volunteers = this.state.volunteers
        volunteers[idx] = evt.target.value
        this.setState({ volunteers })
        // console.log(volunteers)
    }

    handleChangeCallers = (name, idx) => evt => {
        let callers = this.state.callers
        callers[idx] = evt.target.value
        this.setState({ callers })
        // console.log(callers)
    }

    handleClickSave = (info) => evt => {

        const packOut = this.state.packetsOut
        const packIn = this.state.packetsIn
        const vols = this.state.volunteers
        const callers = this.state.callers

        // console.log(info)
        // console.log(packOut)
        // console.log(packIn)
        // console.log(vols)
        // console.log(callers)

        let sendToDB = {
            //[dayName]: {
            "packetsOut": packOut,
            "packetsIn": packIn,
            "volunteers": vols,
            "callers": callers,
            "info": this.props.info
            //}
        }

        // console.log(sendToDB)
        // console.log(this.props.addShiftData)
        this.props.addShiftData(sendToDB)
        // this.props.firebase.push('gotv', sendToDB)


    }

    render() {

        return (
            <List component="ul">
                {this.props.item.map((shift, idx) => (
                    <div key={idx}>
                        <ListItem
                            divider
                            button
                            onClick={() => {
                                this.handleClick(idx)
                            }}>
                            <ListItemIcon>
                                {this.state.open[idx] ? <RemoveCircleOutlineIcon /> : <AddCircleOutlineIcon />}
                            </ListItemIcon>
                            <ListItemText
                                inset
                                primary={moment.unix(shift.start_date).format('ha')}
                            />
                        </ListItem>
                        <Collapse in={this.state.open[idx]} timeout="auto" unmountOnExit>
                            <Grid container>
                                <TextField
                                    label="Packets Out"
                                    type="number"
                                    fullWidth
                                    className={classes.textField}
                                    value={this.state.packetsOut[idx]}
                                    onChange={this.handleChangeOut('packetsOut', idx)}
                                    margin="normal"
                                    InputProps={{
                                        startAdornment: <InputAdornment style={{ marginRight: '2px' }} position="start">#</InputAdornment>,
                                    }}
                                />

                                <TextField
                                    label="Packets In"
                                    type="number"
                                    fullWidth
                                    className={classes.textField}
                                    value={this.state.packetsIn[idx]}
                                    onChange={this.handleChangeIn('packetsIn', idx)}
                                    margin="normal"
                                    InputProps={{
                                        startAdornment: <InputAdornment style={{ marginRight: '2px' }} position="start">#</InputAdornment>,
                                    }}
                                />

                                <TextField
                                    label="Volunteers"
                                    type="number"
                                    fullWidth
                                    className={classes.textField}
                                    value={this.state.volunteers[idx]}
                                    onChange={this.handleChangeVolunteers('volunteers', idx)}
                                    margin="normal"
                                    InputProps={{
                                        startAdornment: <InputAdornment style={{ marginRight: '2px' }} position="start">#</InputAdornment>,
                                    }}
                                />
                                <TextField
                                    label="Callers"
                                    type="number"
                                    fullWidth
                                    className={classes.textField}
                                    value={this.state.callers[idx]}
                                    onChange={this.handleChangeCallers('callers', idx)}
                                    margin="normal"
                                    InputProps={{
                                        startAdornment: <InputAdornment style={{ marginRight: '2px' }} position="start">#</InputAdornment>,
                                    }}
                                />
                            </Grid>
                            <br />
                            <br />
                            <Grid container justify="center" alignItems="center">

                                <Grid item>
                                    <Button variant="contained" color="secondary" onClick={this.handleClickSave(this.props.info)} size="large">
                                        Save &nbsp;<SaveIcon />
                                    </Button>
                                </Grid>
                            </Grid>
                        </Collapse>
                    </div>
                ))}
            </List>

        )

        /*
        return (

            <Paper className={classes.paperTable}>
                <Table className={classes.shiftTable}>
                    <TableHead>
                        <TableRow>
                            <TableCell padding={'none'}></TableCell>
                            <TableCell padding={'none'}>OUT</TableCell>
                            <TableCell padding={'none'}>IN</TableCell>
                            <TableCell padding={'none'}>VOLS</TableCell>
                            <TableCell padding={'none'}>CALLERS</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.item.map((shift, idx) => {
                            return (
                                <TableRow key={shift.start_date}>
                                    <TableCell component="th" scope="row" padding={'none'}>
                                        <b>{moment.unix(shift.start_date).format('ha')}</b>
                                    </TableCell>
                                    <TableCell padding={'none'}>
                                        <TextField
                                            label=""
                                            type="number"
                                            style={{ width: '55px' }}
                                            className={classes.textField}
                                            value={this.state.packetsOut[idx]}
                                            onChange={this.handleChangeOut('packetsOut', idx)}
                                            margin="normal"
                                            InputProps={{
                                                startAdornment: <InputAdornment style={{ marginRight: '2px' }} position="start">#</InputAdornment>,
                                            }}
                                        /></TableCell>
                                    <TableCell padding={'none'}>
                                        <TextField
                                            label=""
                                            type="number"
                                            style={{ width: '55px' }}
                                            className={classes.textField}
                                            value={this.state.packetsIn[idx]}
                                            onChange={this.handleChangeIn('packetsIn', idx)}
                                            margin="normal"
                                            InputProps={{
                                                startAdornment: <InputAdornment style={{ marginRight: '2px' }} position="start">#</InputAdornment>,
                                            }}
                                        />
                                    </TableCell>
                                    <TableCell padding={'none'}>
                                        <TextField
                                            label=""
                                            type="number"
                                            style={{ width: '55px' }}
                                            className={classes.textField}
                                            value={this.state.volunteers[idx]}
                                            onChange={this.handleChangeVolunteers('volunteers', idx)}
                                            margin="normal"
                                            InputProps={{
                                                startAdornment: <InputAdornment style={{ marginRight: '2px' }} position="start">#</InputAdornment>,
                                            }}
                                        />
                                    </TableCell>
                                    <TableCell padding={'none'}>
                                        <TextField
                                            label=""
                                            type="number"
                                            style={{ width: '55px' }}
                                            className={classes.textField}
                                            value={this.state.callers[idx]}
                                            onChange={this.handleChangeCallers('callers', idx)}
                                            margin="normal"
                                            InputProps={{
                                                startAdornment: <InputAdornment style={{ marginRight: '2px' }} position="start">#</InputAdornment>,
                                            }}
                                        />
                                    </TableCell>
                                </TableRow>

                            );
                        })}
                    </TableBody>
                </Table>
                <br />
                <br />
                <Grid container justify="center" alignItems="center">

                    <Grid item>
                        <Button variant="contained" color="secondary" onClick={this.handleClickSave(this.props.info)} size="large">
                            Save &nbsp;<SaveIcon />
                        </Button>
                    </Grid>
                </Grid>
                <br />


            </Paper>

        )*/
    }

}

export default TimeSlots
