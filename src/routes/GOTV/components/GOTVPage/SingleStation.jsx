import React, { Component } from 'react'
import enhance from './SingleStation.enhancer'
import PropTypes from 'prop-types'
import classes from './GOTVPage.scss'
// import moment from 'moment'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
// import TextField from '@material-ui/core/TextField'
// import Card from '@material-ui/core/Card'
// import CardActions from '@material-ui/core/CardActions'
// import CardContent from '@material-ui/core/CardContent'
// import Button from '@material-ui/core/Button'
// import axios from 'axios'
// import SwipeableViews from 'react-swipeable-views'
// import AppBar from '@material-ui/core/AppBar'
// import Tabs from '@material-ui/core/Tabs'
// import Tab from '@material-ui/core/Tab'
// import TimeSlots from './TimeSlots'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'

// function TabContainer(props) {
//     return (
//         <Typography component="div" style={{ padding: 8 * 3 }}>
//             {props.children}
//         </Typography>
//     );
// }

const DAYS = ['NOV 3', 'NOV 4', 'NOV 5', 'NOV 6']
/* const DAY1 = 'NOV 3'
const DAY2 = 'NOV 4'
const DAY3 = 'NOV 5'
const DAY4 = 'NOV 6'
 */

/* function TabContainer({ children, dir }) {
    return (
        <Typography component="div" dir={dir}>
            {children}
        </Typography>
    )
}
 */
class SingleStation extends Component {
    constructor(props) {
        super(props)



        this.state = {
            // packetsOut: '',
            value: 0,
            currentDay: DAYS[0]
        }



    }


    handleClickDay = (value) => {


        let setDay

        switch (value) {
            case 0:
                setDay = DAYS[0]
                break
            case 1:
                setDay = DAYS[1]
                break
            case 2:
                setDay = DAYS[2]
                break
            case 3:
                setDay = DAYS[3]
                break
            default:
                setDay = DAYS[0]
        }

        this.setState({
            value: value,
            currentDay: setDay
        })

        this.props.clickDay('oneida', 'utica', setDay)
        console.log(this.state)
    }

    /*     handleChangeIndex = index => {
            let setDay
            switch (index) {
                case 0:
                    setDay = DAY1
                    break
                case 1:
                    setDay = DAY2
                    break
                case 2:
                    setDay = DAY3
                    break
                case 3:
                    setDay = DAY4
                    break
                default:
                    setDay = DAY1
            }
            this.setState({
                value: index,
                currentDay: setDay
            })
        } */


    // handleChange = (event, value) => {
    //     this.setState({ value });
    // };


    render() {

        return (
            <List component="ul">
                {DAYS.map((day, idx) => (
                    <div key={idx}>
                        <ListItem
                            divider
                            button
                            onClick={() => {
                                this.handleClickDay(idx)
                            }}>
                            <ListItemText
                                inset
                                primary={day}
                            />
                        </ListItem>
                    </div>
                ))}
            </List>
        )

        /*

        const eventData = this.props.eventData
        // const info = this.props.info.gotvShift.split("-")
        const info = this.props.info.gotvDay.split("-")
        const locationName = info[0]
        const stationName = info[1].replace("_", " ")
        const eventId = info[2]

        const theEvent = eventData.filter(evt => {
            if (evt.id === Number(eventId)) {
                return evt
            }
        })
        console.log(theEvent)

        let day1TimeSlots = []
        let day2TimeSlots = []
        let day3TimeSlots = []
        let day4TimeSlots = []

        theEvent[0].timeslots.map((items, idx) => {

            if (idx <= 3) {
                day1TimeSlots.push(items)
            }

            if (idx >= 4 && idx <= 7) {
                day2TimeSlots.push(items)
            }

            if (idx >= 8 && idx <= 11) {
                day3TimeSlots.push(items)
            }

            if (idx >= 12 && idx <= 15) {
                day4TimeSlots.push(items)
            }
        })

        let passInfo = {
            "locationName": locationName,
            "stationName": stationName,
            "dayNum": this.state.value,
            "dateName": this.state.currentDay,
            "dayName": 'day' + String(this.state.value + 1)
        }

        */

        // const { value } = this.state;

        /*    <h2>{stationName}</h2>
        <TimeSlots item={theEvent[0]} /> */
        /*
        return (
            <div>
                <AppBar position="static" color="default">
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        fullWidth>
                        <Tab label={DAY1} />
                        <Tab label={DAY2} />
                        <Tab label={DAY3} />
                        <Tab label={DAY4} />

                    </Tabs>
                </AppBar>
                <Grid container className={classes.stationGrid}>

                    <div>
                        <Typography variant="title" gutterBottom>{this.state.currentDay}: {locationName.toUpperCase()} - {stationName.toUpperCase()}</Typography></div>

                    <SwipeableViews
                        className={classes.swipeViews}
                        axis={'x'}
                        index={this.state.value}
                        onChangeIndex={this.handleChangeIndex}>

                        <TabContainer dir={'ltr'}>
                            <TimeSlots item={day1TimeSlots} addShiftData={this.props.addShiftData} info={passInfo} currentDay={this.state.value} />
                        </TabContainer>

                        <TabContainer dir={'ltr'}>
                            <TimeSlots item={day2TimeSlots} addShiftData={this.props.addShiftData} info={passInfo} currentDay={this.state.value} />
                        </TabContainer>
                        <TabContainer dir={'ltr'}>
                            <TimeSlots item={day3TimeSlots} addShiftData={this.props.addShiftData} info={passInfo} currentDay={this.state.value} />
                        </TabContainer>
                        <TabContainer dir={'ltr'}>
                            <TimeSlots item={day4TimeSlots} addShiftData={this.props.addShiftData} info={passInfo} currentDay={this.state.value} />
                        </TabContainer>

                    </SwipeableViews>

                </Grid>
            </div>

        )*/
    }
}
SingleStation.propTypes = {
    // locations: PropTypes.array, // from react-router
    clickDay: PropTypes.func, // from enhancer (withHandlers - router)
    clickStation: PropTypes.func
}

export default SingleStation

// export default enhance(SingleStation)
