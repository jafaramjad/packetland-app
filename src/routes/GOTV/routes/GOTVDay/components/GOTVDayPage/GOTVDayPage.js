import React, { Component } from 'react'
import axios from 'axios'

import DayList from './DayList'

class GOTVDayPage extends Component {
  constructor(props) {
    super(props)

    // console.log(this.props)
    this.state = {
      vanEvents: this.props.vanEvents
    }
  }

  /* 	componentWillMount() {
			if (this.props.vanEvents === "") {
					this.queryAPI()
				} 
		}
	 */

  render() {
    return this.props.children ? (
      this.props.children
    ) : (
      <DayList
        vanEvents={this.props.vanEvents}
        location={this.props.params.gotvDay}
        clickDay={this.props.clickDay}
      />
    )
  }
}
export default GOTVDayPage
