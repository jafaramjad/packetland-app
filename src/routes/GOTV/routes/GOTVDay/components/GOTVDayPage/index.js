import GOTVDayPage from './GOTVDayPage'
import enhance from './GOTVDayPage.enhancer'

export default enhance(GOTVDayPage)
