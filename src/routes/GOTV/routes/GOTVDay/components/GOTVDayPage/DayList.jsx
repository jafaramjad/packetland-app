import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classes from './GOTVDayPage.scss'

import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { Typography } from '@material-ui/core'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'

const DAYS = ['NOV 3', 'NOV 4', 'NOV 5', 'NOV 6']
// const DAYS = ['OCT 20', 'OCT 21', 'OCT 27', 'OCT 28']

class DayList extends Component {
    constructor(props) {
        super(props)

        const location = this.props.location.split("-")
        const county = location[0]
        const station = location[1]

        this.state = {
            value: 0,
            currentDay: DAYS[0].date,
            county: county,
            station: station
        }


        // console.log(this.props)
    }

    componentDidUpdate(prevProps) {
        if (this.props.vanEvents !== prevProps.vanEvents) {
            // console.log('new props received')
            // console.log(this.props)
        }
    }


    handleClickDay = (value) => {


        let setDay

        switch (value) {
            case 0:
                setDay = DAYS[0]
                break
            case 1:
                setDay = DAYS[1]
                break
            case 2:
                setDay = DAYS[2]
                break
            case 3:
                setDay = DAYS[3]
                break
            default:
                setDay = DAYS[0]
        }

        setDay = setDay.replace(" ", "-").toLowerCase()

        this.setState({
            value: value,
            currentDay: setDay
        })


        // console.log(this.props.vanEvents[value])

        this.props.clickDay(this.state.county, this.state.station, setDay, this.props.vanEvents[value])
    }


    render() {

        return (
            <Grid container className={classes.dayList}>
                <Grid item xs={12}>
                    <Typography variant="title">
                        {this.state.county.toUpperCase()} > {this.state.station.replace("_", " ").toUpperCase()}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <List component="ul">
                        {DAYS.map((day, idx) => (
                            <ListItem
                                key={idx}
                                divider
                                button
                                onClick={() => {
                                    this.handleClickDay(idx)
                                }}>
                                <ListItemIcon><NavigateNextIcon /></ListItemIcon>

                                <ListItemText
                                    inset
                                    primary={day}
                                />
                            </ListItem>
                        ))}
                    </List>
                </Grid>
            </Grid>
        )
    }
}

DayList.propTypes = {
    clickDay: PropTypes.func, // from enhancer (withHandlers - router)
}

export default DayList
