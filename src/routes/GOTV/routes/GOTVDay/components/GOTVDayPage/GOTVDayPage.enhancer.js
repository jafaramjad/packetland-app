import { compose } from 'redux'
import { connect } from 'react-redux'
import { GOTV_PATH } from 'constants'
import { withHandlers, withStateHandlers, pure } from 'recompose'
import { firestoreConnect } from 'react-redux-firebase'
import { withNotifications } from 'modules/notification'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'

export default compose(
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  // Map auth uid from state to props
  connect(({ firebase: { auth: { uid } } }) => ({ uid })),
  // Wait for uid to exist before going further
  spinnerWhileLoading(['uid']),
  firestoreConnect(({ params, uid }) => [
    // Listener for projects the current user created
    /* 	{
				collection: 'projects',
				where: ['createdBy', '==', uid]
			} */
  ]),
  // Add props.router
  withRouter,

  withStateHandlers(
    // Setup initial state
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    // Add state handlers as props
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),
  // Add handlers as props
  withHandlers({
    clickDay: ({ router }) => (countyName, stationName, day, eventId) => {
      // console.log('clicking thing...')
      router.push(`${GOTV_PATH}/${countyName}-${stationName}/${day}-${eventId}`)
    }
  }),
  pure // shallow equals comparison on props (prevent unessesary re-renders)
)

/* import { compose } from 'redux'
import { connect } from 'react-redux'
import { get } from 'lodash'
import { withHandlers, withStateHandlers } from 'recompose'
import { GOTV_PATH } from 'constants'
import { withNotifications } from 'modules/notification'

import { firestoreConnect } from 'react-redux-firebase'
import { withRouter, spinnerWhileLoading } from 'utils/components'

import { UserIsAuthenticated } from 'utils/router'

export default compose(
	// redirect to /login if user is not logged in
	UserIsAuthenticated,
	connect(({ firebase: { auth: { uid } } }) => ({ uid })),
	// Create listeners based on current users UID
	firestoreConnect(({ params }) => [
		//   // Listener for projects the current user created
		{
			collection: 'gotv'
			// doc: '4sjDotrpFJSHFhoB69sf'
		}
	]),
	// // Map projects from state to props
	connect(({ firestore: { data } }, { params }) => ({
		project: get(data, `projects.${'4sjDotrpFJSHFhoB69sf'}`)
	})),
	withNotifications,
	withStateHandlers(
		// Setup initial state
		({ initialDialogOpen = false }) => ({
			newDialogOpen: initialDialogOpen
		}),
		// Add state handlers as props
		{
			toggleDialog: ({ newDialogOpen }) => () => ({
				newDialogOpen: !newDialogOpen
			})
		}
	),
	 	withRouter,
		withHandlers({
			goToShift: ({ router }) => shiftId => {
				router.push(`${GOTV_PATH}/${shiftId}`)
			}
		}), 
	// Show loading spinner while project is loading
	// spinnerWhileLoading(['project']),
	withHandlers({
		addShiftData: props => newData => {
			const { firestore, uid, showError, showSuccess, toggleDialog } = props
			// console.log(props)
			// console.log(newData)

			const locationName = newData.info.locationName
			const stationName = newData.info.stationName
			const dayName = newData.info.dayName

			if (!uid) {
				return console.error('You must be logged in to create a project')
			}
			return firestore
				.set(
					{ collection: `gotv/${locationName}/${stationName}`, doc: dayName },
					{
						...newData,
						createdBy: uid,
						createdAt: firestore.FieldValue.serverTimestamp()
					}
				)
				.then(() => {
					toggleDialog()
					showSuccess('Shift added successfully')
				})
				.catch(err => {
					console.error('Error:', err) // eslint-disable-line no-console
					showError(err.message || 'Could not add shift')
					return Promise.reject(err)
				})
		}
	}),
);
 */
