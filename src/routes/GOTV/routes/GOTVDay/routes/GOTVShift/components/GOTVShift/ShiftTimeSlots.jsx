import React, { Component } from 'react'
import classes from './GOTVShiftPage.scss'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import SaveIcon from '@material-ui/icons/Save'
import InputAdornment from '@material-ui/core/InputAdornment'

class ShiftTimeSlots extends Component {
	constructor(props) {
		super(props)
		this.state = {
			volsBegun: '',
			packetsOut: '',
			packetsIn: '',
			walkContacts: '',
			walkAttempts: '',
			volsHubdialer: '',
			volsPaper: ''
		}
		// console.log(props)
	}

	componentDidUpdate() {
		// console.log(props)
	}

	handleClick = idx => {
		const newOpen = [...this.state.open]
		newOpen[idx] = !newOpen[idx]

		this.setState({
			open: newOpen
		})
	}

	handleChangeVal = name => evt => {
		this.setState({ [name]: evt.target.value })
	}

	handleClickSave = shiftIdx => evt => {
		const volsBegun = this.state.volsBegun
		const packOut = this.state.packetsOut
		const packIn = this.state.packetsIn
		const walkContacts = this.state.walkContacts
		const walkAttempts = this.state.walkAttempts
		const volsHubdialer = this.state.volsHubdialer
		const volsPaper = this.state.volsPaper

		// console.log(shiftIdx)
		// console.log(packOut)
		// console.log(packIn)
		// console.log(walkContacts)
		// console.log(walkAttempts)
		// console.log(volsHubdialer)
		// console.log(volsPaper)

		let sendToDB = {
			"volsBegun": volsBegun,
			"packetsOut": packOut,
			"packetsIn": packIn,
			"walkContacts": walkContacts,
			"walkAttempts": walkAttempts,
			"volsHubdialer": volsHubdialer,
			"volsPaper": volsPaper
		}

		this.props.handleShiftUpdate(sendToDB)
	}

	componentDidUpdate(prevProps) {
		if (Object.keys(this.props.hasData).length !== Object.keys(prevProps.hasData).length) {
			// console.log('new props have been introduced...')
			// console.log(this.props.hasData)
			// console.log(this.props.hasData.packetsIn[this.props.tabIndex])
			const idx = this.props.tabIndex
			const newData = this.props.hasData
			this.setState({
				volsBegun: newData.volsBegun[idx],
				packetsOut: newData.packetsOut[idx],
				packetsIn: newData.packetsIn[idx],
				walkContacts: newData.walkContacts[idx],
				walkAttempts: newData.walkAttempts[idx],
				volsHubdialer: newData.volsHubdialer[idx],
				volsPaper: newData.volsPaper[idx]
			})
		}
	}

	render() {

		const isPhonesLocation = this.props.station === 'norwich_(phones)' || this.props.station === 'hamilton_(phones)'
		const isDoorsLocation = this.props.station === 'norwich_(doors)' || this.props.station === 'hamilton_(doors)'

		const electionDay = this.props.electionDay

		const disableCallField = this.props.shiftIndex === 4 || isDoorsLocation ? isDoorsLocation ? true : (electionDay ? false : true) : false
		// this.props.shiftIndex === 4 || isDoorsLocation ? electionDay ? false : true : false
		// this.props.shiftIndex === 4 || isDoorsLocation ? electionDay ? "outlined" : "filled" : "outlined"
		// this.props.shiftIndex === 4 || isDoorsLocation ? electionDay ? "# of Vols on hubdialer now" : "N/A for shift - leave blank" : "# of Vols on hubdialer now"
		return (
			<div className={classes.shiftTabContainer}>
				<Grid container spacing={16}>

					<Grid item xs={12} sm={4}>
						<TextField
							className={classes.textField}

							label="Volunteer Shifts Begun"
							type="number"
							fullWidth
							value={this.state.volsBegun}
							onChange={this.handleChangeVal('volsBegun', this.props.shiftIndex)}
							margin="normal"
							variant="outlined"
							helperText="# of Vols who have begun shift"
						/>
					</Grid>

					<Grid item xs={6} sm={4}>
						<TextField
							disabled={disableCallField}
							className={classes.textField}
							label="Hubdialer Vols"
							type="number"
							fullWidth
							value={this.state.volsHubdialer}
							onChange={this.handleChangeVal('volsHubdialer', this.props.shiftIndex)}
							margin="normal"
							variant={disableCallField ? "filled" : "outlined"}
							helperText={disableCallField ? "N/A for shift - leave blank" : "# of Vols on hubdialer now"}
						/>
					</Grid>
					<Grid item xs={6} sm={4}>
						<TextField
							disabled={disableCallField}
							label="Paper Vols"
							type="number"
							fullWidth
							value={this.state.volsPaper}
							onChange={this.handleChangeVal('volsPaper', this.props.shiftIndex)}
							margin="normal"
							variant={disableCallField ? "filled" : "outlined"}
							helperText={disableCallField ? "N/A for shift - leave blank" : "# of Vols calling paper lists now"}
						/>
					</Grid>

					<Grid item xs={6}>
						<TextField
							label="Packets Out"
							type="number"
							disabled={isPhonesLocation}
							fullWidth
							value={this.state.packetsOut}
							onChange={this.handleChangeVal('packetsOut', this.props.shiftIndex)}
							margin="normal"
							variant={isPhonesLocation ? "filled" : "outlined"}
							helperText="# of Walk packets OUT since last report"
						/>
					</Grid>

					<Grid item xs={6}>
						<TextField
							className={classes.textField}
							disabled={this.props.shiftIndex === 0 || isPhonesLocation}
							label="Packets In"
							type="number"
							fullWidth
							value={this.state.packetsIn}
							onChange={this.handleChangeVal('packetsIn', this.props.shiftIndex)}
							margin="normal"
							variant={this.props.shiftIndex === 0 || isPhonesLocation ? "filled" : "outlined"}
							helperText={this.props.shiftIndex === 0 ? "N/A for shift - leave blank" : "# of Walk packets IN since last report"}
						/>
					</Grid>

					<Grid item xs={6}>
						<TextField
							className={classes.textField}
							disabled={this.props.shiftIndex === 0 || isPhonesLocation}
							label="Walk Attempts"
							type="number"
							fullWidth
							value={this.state.walkAttempts}
							onChange={this.handleChangeVal('walkAttempts', this.props.shiftIndex)}
							margin="normal"
							variant={this.props.shiftIndex === 0 || isPhonesLocation ? "filled" : "outlined"}
							helperText={this.props.shiftIndex === 0 ? "N/A for shift - leave blank" : "Total new walk attempts since last report"}
						/>
					</Grid>

					<Grid item xs={6}>
						<TextField
							className={classes.textField}
							disabled={this.props.shiftIndex === 0 || isPhonesLocation}
							label="Walk Contacts"
							type="number"
							fullWidth
							value={this.state.walkContacts}
							onChange={this.handleChangeVal('walkContacts', this.props.shiftIndex)}
							margin="normal"
							variant={this.props.shiftIndex === 0 || isPhonesLocation ? "filled" : "outlined"}
							helperText={this.props.shiftIndex === 0 ? "N/A for shift - leave blank" : "Total new walk contacts since last report"}
						/>
					</Grid>




				</Grid>
				<br />

				<Grid container justify="center" alignItems="center">

					<Grid item>
						<Button variant="contained" color="secondary" onClick={this.handleClickSave(this.props.shiftIndex)} size="large">
							Save &nbsp;
							<SaveIcon />
						</Button>
					</Grid>
				</Grid>

			</div>
		)
	}
}

export default ShiftTimeSlots


/*
		<TextField
							className={classes.textField}
							disabled={this.props.shiftIndex === 4 ? electionDay ? false : true : false}
							label="Volunteer Shifts Begun"
							type="number"
							fullWidth
							value={this.state.volsBegun}
							onChange={this.handleChangeVal('volsBegun', this.props.shiftIndex)}
							margin="normal"
							variant={this.props.shiftIndex === 4 ? electionDay ? "outlined" : "filled" : "outlined"}
							helperText={this.props.shiftIndex === 4 ? electionDay ? "# of Vols who have begun shift" : "N/A for shift - leave blank" : "# of Vols who have begun shift"}
						/>
						*/