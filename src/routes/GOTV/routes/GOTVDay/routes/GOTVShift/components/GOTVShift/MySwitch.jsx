import React, { Component } from 'react'
import Switch from '@material-ui/core/Switch'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
	colorSwitchBase: {
		color: '#ccc',
		'&$colorChecked': {
			color: '#00ff00',
			'& + $colorBar': {
				backgroundColor: '#777'
			}
		}
	},
	colorBar: {},
	colorChecked: {}
})

class MySwitch extends Component {
	render() {
		const { classes } = this.props
		return (
			<Switch
				checked={this.props.isChecked}
				onChange={this.props.handleChange('switch')}
				classes={{
					switchBase: classes.colorSwitchBase,
					checked: classes.colorChecked,
					bar: classes.colorBar
				}}
			/>
		)
	}
}

export default withStyles(styles)(MySwitch)
