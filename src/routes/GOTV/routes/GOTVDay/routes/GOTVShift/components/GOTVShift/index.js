import GOTVShiftPage from './GOTVShiftPage'
import enhance from './GOTVShiftPage.enhancer'

export default enhance(GOTVShiftPage)
