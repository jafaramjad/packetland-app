import React, { Component } from 'react'
import axios from 'axios'
import swal from 'sweetalert2'

import classes from './GOTVShiftPage.scss'
import ShiftTimeSlots from './ShiftTimeSlots'
import ShiftVolunteers from './ShiftVolunteers'
import ShiftAllVols from './ShiftAllVols'

import MySwitch from './MySwitch'
import { TOTAL_PACKETS } from '../../../../../../../../constants/packets'
import { STATION_MAP } from '../../../../../../../../constants/places'

import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import SwipeableViews from 'react-swipeable-views'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import Modal from '@material-ui/core/Modal'
import LinearProgress from '@material-ui/core/LinearProgress'
import CircularProgress from '@material-ui/core/CircularProgress'

function TabContainer({ children, dir }) {
  return <div>{children}</div>
}
const ELECTION_DAY = 'NOV 6'
const DAYS = ['NOV 3', 'NOV 4', 'NOV 5', 'NOV 6']
// const ELECTION_DAY = 'OCT 28'

class GOTVShift extends Component {
  constructor(props) {
    super(props)

    // console.log(props)

    // const dateName = props.params.gotvShift.replace("-", " ").toUpperCase()
    const dayAndEventId = props.params.gotvShift.split('-')

    const dateName = String(
      dayAndEventId[0].toUpperCase() + ' ' + dayAndEventId[1]
    )

    const eventId = dayAndEventId[2]

    let dayName
    let dayNum
    switch (dateName) {
      case DAYS[0]:
        dayName = 'day1'
        dayNum = 0
        break
      case DAYS[1]:
        dayName = 'day2'
        dayNum = 1
        break
      case DAYS[2]:
        dayName = 'day3'
        dayNum = 2
        break
      case DAYS[3]:
        dayName = 'day4'
        dayNum = 3
        break
      default:
        break
    }
    /* 		switch (dateName) {
					case 'NOV 3':
						dayName = 'day1'
						dayNum = 0
						break
					case 'NOV 4':
						dayName = 'day2'
						dayNum = 1
						break
					case 'NOV 5':
						dayName = 'day3'
						dayNum = 2
						break
					case 'NOV 6':
						dayName = 'day4'
						dayNum = 3
						break
					default:
						break
				} */

    // const shiftCount = dateName === ELECTION_DAY ? { 0: '', 1: '', 2: '', 3: '', 4: '', 5: '' } : { 0: '', 1: '', 2: '', 3: '', 4: '' }
    const shiftCount = { 0: '', 1: '', 2: '', 3: '', 4: '' }

    let tabTime
    if (dateName === ELECTION_DAY) {
      tabTime = ['7AM', '9AM', '12PM', '3PM', '6PM']
    } else {
      tabTime = ['9AM', '12PM', '3PM', '6PM', '9PM']
    }

    // const location = this.props.params.gotvDay.replace("-", " ").toUpperCase()
    const location = this.props.params.gotvDay.split('-')
    const county = location[0]
    const station = location[1]

    this.state = {
      loaderModal: true,
      volStatusUpdates: '',
      volShiftSlots: {
        '7AM': [],
        '9AM': [],
        '12PM': [],
        '3PM': [],
        '6PM': [],
        '9PM': []
      },
      tabTime: tabTime,
      signupsAll: '',
      signups: '',
      eventId: eventId,
      shiftCount: shiftCount,
      expandedForm: true,
      expandedVols: false,
      expandedAllVols: false,
      dialogText: '',
      dialogOpen: false,
      switch: false,
      value: 0,
      queryResults: {},
      dateName: dateName,
      dayName: dayName,
      dayNum: dayNum,
      county: county,
      station: station,
      currentShift: 0,
      volsBegun: { ...shiftCount },
      packetsOut: { ...shiftCount },
      packetsIn: { ...shiftCount },
      walkContacts: { ...shiftCount },
      walkAttempts: { ...shiftCount },
      volsHubdialer: { ...shiftCount },
      volsPaper: { ...shiftCount }
      // volunteers: { ...shiftCount },
      // callers: { ...shiftCount }
    }
    // console.log(this.props)
  }

  componentWillMount() {
    const getQuery = {
      county: this.state.county,
      station: this.state.station,
      day: this.state.dayName
    }
    // console.log(TOTAL_PACKETS)
    // console.log(this.state)

    const queryData = this.props.getShiftData(getQuery)
    queryData.then(res => {
      // console.log(res)
      if (res !== undefined) {
        const queryRes = {
          volsBegun: res.volsBegun,
          packetsOut: res.packetsOut,
          packetsIn: res.packetsIn,
          walkContacts: res.walkContacts,
          walkAttempts: res.walkAttempts,
          volsHubdialer: res.volsHubdialer,
          volsPaper: res.volsPaper

          // volunteers: res.volunteers,
          // callers: res.callers
        }

        this.setState(
          {
            queryResults: queryRes,
            volsBegun: res.volsBegun,
            packetsOut: res.packetsOut,
            packetsIn: res.packetsIn,
            walkContacts: res.walkContacts,
            walkAttempts: res.walkAttempts,
            volsHubdialer: res.volsHubdialer,
            volsPaper: res.volsPaper,
            switch: res.info.stationOpen
            // volunteers: res.volunteers,
            // callers: res.callers
          },
          () => {
            // this.queryAPI()
          }
        )
      }
    })
  }

  componentDidMount() {
    this.queryAPI()
  }

  handleShiftUpdate = vals => {
    const shiftIdx = this.state.currentShift
    const totalPackets = TOTAL_PACKETS[this.state.county][this.state.station]
    // console.log(shiftIdx)
    // console.log(vals)

    let passInfo = {
      shiftIndex: shiftIdx,
      totalPackets: totalPackets,
      stationOpen: this.state.switch,
      locationName: this.state.county,
      stationName: this.state.station,
      dateName: this.state.dateName,
      dayNum: this.state.dayNum,
      dayName: this.state.dayName
    }
    let volsBegun = this.state.volsBegun
    let packetsOut = this.state.packetsOut
    let packetsIn = this.state.packetsIn
    let walkContacts = this.state.walkContacts
    let walkAttempts = this.state.walkAttempts
    let volsHubdialer = this.state.volsHubdialer
    let volsPaper = this.state.volsPaper

    // let volunteers = this.state.volunteers
    // let callers = this.state.callers
    volsBegun[shiftIdx] = vals['volsBegun']
    packetsOut[shiftIdx] = vals['packetsOut']
    packetsIn[shiftIdx] = vals['packetsIn']
    walkContacts[shiftIdx] = vals['walkContacts']
    walkAttempts[shiftIdx] = vals['walkAttempts']
    volsHubdialer[shiftIdx] = vals['volsHubdialer']
    volsPaper[shiftIdx] = vals['volsPaper']

    // volunteers[shiftIdx] = vals['volunteers']
    // callers[shiftIdx] = vals['callers']

    this.setState(
      {
        volsBegun,
        packetsOut,
        packetsIn,
        walkContacts,
        walkAttempts,
        volsHubdialer,
        volsPaper
      },
      () => {
        let sendToDB = {
          volsBegun: this.state.volsBegun,
          packetsOut: this.state.packetsOut,
          packetsIn: this.state.packetsIn,
          walkContacts: this.state.walkContacts,
          walkAttempts: this.state.walkAttempts,
          volsHubdialer: this.state.volsHubdialer,
          volsPaper: this.state.volsPaper,
          info: passInfo
        }

        // console.log(sendToDB)
        // console.log(this.props.addShiftData)
        // this.props.addShiftData(sendToDB, "Added new data successfully")
        this.props.addShiftData(
          sendToDB,
          'Added new data successfully',
          this.successCB
        )

        this.props.firebase.push('gotv', sendToDB)
      }
    )
  }

  handleChange = (event, idx) => {
    this.setState({ currentShift: idx })
  }

  handleChangeIndex = index => {
    this.setState({ currentShift: index })
  }

  handleSwitch = name => event => {
    this.handleOpenDialog(event.target.checked)

    // console.log(event.target.checked)
    /* 	this.setState(
				{
					switch: event.target.checked
				},
				() => {
					let passInfo = {
						"stationOpen": this.state.switch,
						"locationName": this.state.county,
						"stationName": this.state.station,
						"dayName": this.state.dayName
					}
	
					let sendToDB = {
						"info": passInfo
					}
	
					this.props.addShiftData(sendToDB)
					this.props.firebase.push('gotv', sendToDB)
				}
			) */
  }

  handleOpenDialog = switchEvt => {
    // console.log(switchEvt)

    const statusText = switchEvt ? 'open' : 'close'

    this.setState({
      dialogOpen: true,
      dialogText: statusText
    })
  }

  handleCloseDialog = decision => event => {
    // console.log(decision)
    // const shiftIdx = this.state.currentShift

    const dialogText = this.state.dialogText
    let newSwitchStatus
    // console.log(dialogText)

    if (decision === 'yes') {
      if (dialogText === 'open') {
        newSwitchStatus = true
      } else {
        newSwitchStatus = false
      }
      // console.log(newSwitchStatus)

      const totalPackets = TOTAL_PACKETS[this.state.county][this.state.station]

      this.setState(
        {
          switch: newSwitchStatus,
          dialogOpen: false
        },
        () => {
          const shiftIdx = newSwitchStatus ? 'open' : 'close'

          let passInfo = {
            shiftIndex: shiftIdx,
            totalPackets: totalPackets,
            stationOpen: newSwitchStatus,
            locationName: this.state.county,
            stationName: this.state.station,
            dateName: this.state.dateName,
            dayNum: this.state.dayNum,
            dayName: this.state.dayName
          }

          // const checkValsPacketOut = Object.values(this.state.packetsOut)
          // console.log(this.state.volsHubdialer)
          // console.log(this.state.packetsOut)
          // console.log(this.state.volsHubdialer[0])
          // console.log(this.state.packetsOut[0])
          // console.log(checkValsPacketOut)

          // const emptyDataSet = this.state.dateName === 'NOV 6' ? { 0: '', 1: '', 2: '', 3: '', 4: '', 5: '' } : { 0: '', 1: '', 2: '', 3: '', 4: '' }
          // const emptyDataSet = { 0: '', 1: '', 2: '', 3: '', 4: '' }

          let sendToDB

          let volsBegun = this.state.volsBegun
          let packetsOut = this.state.packetsOut
          let packetsIn = this.state.packetsIn
          let walkContacts = this.state.walkContacts
          let walkAttempts = this.state.walkAttempts
          let volsHubdialer = this.state.volsHubdialer
          let volsPaper = this.state.volsPaper

          // THIS IS THE PROBLEM
          // if (this.state.volsHubdialer[0] === "" && this.state.packetsOut[0] === "") {
          // console.log('no data yet for anything, station must just be opening...')
          sendToDB = {
            info: passInfo,
            volsBegun: { ...volsBegun },
            packetsOut: { ...packetsOut },
            packetsIn: { ...packetsIn },
            walkContacts: { ...walkContacts },
            walkAttempts: { ...walkAttempts },
            volsHubdialer: { ...volsHubdialer },
            volsPaper: { ...volsPaper }
          }

          // } else {
          // 	sendToDB = {
          // 		"info": passInfo
          // 	}
          // }

          // this.props.addShiftData(sendToDB, "Updated station status successfully")
          this.props.addShiftData(
            sendToDB,
            'Updated station status successfully',
            this.successCB
          )

          this.props.firebase.push('gotv', sendToDB)
        }
      )
    } else {
      this.setState({ dialogOpen: false })
    }
  }

  handleChangeExpansion = panel => (event, expandBool) => {
    this.setState(
      {
        [panel]: expandBool
      },
      () => {
        if (panel === 'expandedVols' || panel === 'expandedAllVols') {
          const getQuery = {
            county: this.state.county.toUpperCase(),
            station: this.state.station.toUpperCase(),
            day: this.state.dayName
          }

          const queryVolsData = this.props.getVolData(getQuery)

          queryVolsData.then(res => {
            // console.log(res)
            if (res !== undefined) {
              this.setState({
                // [panel]: expandBool,
                volStatusUpdates: res.status
              })
            }
          })
        }
      }
    )
  }

  // QUERY
  queryAPI = () => {
    const token = this.props.firebase.auth().currentUser.getIdToken()

    token
      .then(tok => {
        axios({
          method: 'get',
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            Authorization: 'Bearer' + ' ' + tok
          },
          // url: `http://localhost:3000/van/events?q=`
          url: `https://us-central1-packet-land.cloudfunctions.net/apiVan/signups?q=${
            this.state.eventId
          }`
        }).then(resp => {
          // console.info(resp.data.items)

          // console.log(STATION_MAP)
          const list = resp.data.items
          const county = this.state.county.toUpperCase()
          const station = this.state.station
            .replace(' ', '_')
            .replace('(', '')
            .replace(')', '')
            .toUpperCase()
          const stagingLocationName = STATION_MAP[county][station]
          // console.log(stagingLocationName)
          let signups = []
          list.map(item => {
            if (
              item.status.name === 'Scheduled' ||
              item.status.name === 'Confirmed' ||
              item.status.name === 'Completed'
            ) {
              if (
                item.role.name === 'Canvasser' ||
                item.role.name === 'Phonebanker'
              ) {
                item.location.name === stagingLocationName
                  ? signups.push(item)
                  : null
              }
            }
          })
          // console.log(signups)

          // SORT VOL SHIFTS

          let volShiftSlots = {
            '7AM': [],
            '9AM': [],
            '12PM': [],
            '3PM': [],
            '6PM': [],
            '9PM': []
          }

          signups.map(item => {
            const timeStart = item.startTimeOverride
            // console.log(timeStart)
            // yyyy-MM-ddTHH:mm:ssZ
            let timeFormat = new Date(timeStart)
            const formatOptions = {
              timeZone: 'America/New_York',
              hour: '2-digit',
              minute: '2-digit',
              hour12: true
            }
            // const timeFormat = moment(timeStart, "yyyy-MM-dd[T]HH:mm:ssZ").format('HH:mm a')
            // console.log(timeFormat)
            let finalTime = timeFormat
              .toLocaleTimeString('en-US', formatOptions)
              .replace(':00 ', '')

            // console.log(finalTime)
            switch (finalTime) {
              case '6AM':
                volShiftSlots['7AM'].push(item)
                break
              case '9AM':
                volShiftSlots['9AM'].push(item)
                break
              case '12PM':
                volShiftSlots['12PM'].push(item)
                break
              case '3PM':
                volShiftSlots['3PM'].push(item)
                break
              case '6PM':
                volShiftSlots['6PM'].push(item)
                break
              default:
                break
            }
            // console.log(volShiftSlots)
          })

          this.setState({
            loaderModal: false,
            signupsAll: resp.data.items,
            signups: signups,
            volShiftSlots: volShiftSlots
          })
        })
      })
      .catch(err => {
        this.setState({
          loaderModal: false
        })
        console.log(err)
      })
  }

  // SUCCESS NOTIFICATION
  successCB = msg => {
    swal(msg)
  }

  render() {
    // const day = this.props.params.gotvShift.replace("-", " ").toUpperCase()
    const day = this.state.dateName
    const station = this.state.station
    const location = this.props.params.gotvDay.replace('-', ', ').toUpperCase()
    // const tabCount = day === "NOV 6" ? [0, 1, 2, 3, 4, 5] : [0, 1, 2, 3, 4]
    const tabCount = [0, 1, 2, 3, 4]

    /* 	let tabTime
			if (day === ELECTION_DAY) {
				tabTime = ['7AM', '9AM', '12PM', '3PM', '6PM']
			} else {
				tabTime = ['9AM', '12PM', '3PM', '6PM', '9PM']
			} */

    // console.log(this.state.queryResults)

    return (
      <div>
        <AppBar position="static" color="default">
          <Tabs
            value={this.state.currentShift}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            fullWidth>
            {day === ELECTION_DAY ? <Tab label={'7AM'} /> : null}
            <Tab label={'9AM'} />
            <Tab label={'12PM'} />
            <Tab label={'3PM'} />
            <Tab label={'6PM'} />
            {day === ELECTION_DAY ? null : <Tab label={'9PM'} />}
          </Tabs>
        </AppBar>
        <Grid container className={classes.shiftGrid}>
          <Grid item xs={9} md={10}>
            {/* <Typography variant="title" gutterBottom>
							{day} - {tabTime[this.state.currentShift]}
						</Typography> */}
            <Typography variant="display1" gutterBottom>
              {day} -{' '}
              {station
                .toUpperCase()
                .replace('_', ' ') /* location.replace("_", " ") */}
            </Typography>
          </Grid>
          <Grid item xs={3} md={2}>
            <FormControlLabel
              labelPlacement={'end'}
              control={
                <MySwitch
                  isChecked={this.state.switch}
                  handleChange={this.handleSwitch}
                />
              }
              label={this.state.switch ? 'OPEN' : 'CLOSED'}
            />
          </Grid>
          <br />
          <br />
          {tabCount.map((item, idx) => {
            return (
              <div
                className={
                  this.state.currentShift === idx
                    ? classes.tabShow
                    : classes.tabHide
                }
                key={`tabshell-` + idx}>
                <TabContainer key={idx} dir={'ltr'}>
                  <ExpansionPanel
                    expanded={this.state.expandedForm}
                    onChange={this.handleChangeExpansion('expandedForm')}>
                    <ExpansionPanelSummary
                      style={{ borderBottom: '1px solid #dcdcdc' }}
                      expandIcon={<ExpandMoreIcon />}>
                      <Typography variant="title" className={classes.heading}>
                        {this.state.tabTime[this.state.currentShift]} - Report
                      </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <ShiftTimeSlots
                        // volunteerList={}
                        station={this.state.station}
                        electionDay={day === ELECTION_DAY}
                        shiftIndex={this.state.currentShift}
                        tabIndex={idx}
                        handleShiftUpdate={this.handleShiftUpdate}
                        hasData={this.state.queryResults}
                      />
                    </ExpansionPanelDetails>
                  </ExpansionPanel>

                  <ExpansionPanel
                    expanded={this.state.expandedVols}
                    onChange={this.handleChangeExpansion('expandedVols')}>
                    <ExpansionPanelSummary
                      style={{ borderBottom: '1px solid #dcdcdc' }}
                      expandIcon={<ExpandMoreIcon />}>
                      <Typography variant="title" className={classes.heading}>
                        {this.state.tabTime[this.state.currentShift]} -
                        Volunteers
                      </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      {/* VAN Event ID = {this.state.eventId} */}
                      <br />
                      <ShiftVolunteers
                        volStatusUpdates={this.state.volStatusUpdates}
                        getVolData={this.props.getVolData}
                        addVolData={this.props.addVolData}
                        location={location}
                        day={this.state.dayName}
                        firebase={this.props.firebase}
                        volunteers={
                          this.state.volShiftSlots[this.state.tabTime[idx]]
                        }
                        shiftTime={this.state.tabTime[idx]}
                      />
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                </TabContainer>
              </div>
            )
          })}

          <ExpansionPanel
            className={classes.allVolExpansion}
            expanded={this.state.expandedAllVols}
            onChange={this.handleChangeExpansion('expandedAllVols')}>
            <ExpansionPanelSummary
              style={{ borderBottom: '1px solid #dcdcdc' }}
              expandIcon={<ExpandMoreIcon />}>
              <Typography variant="title" className={classes.heading}>
                All Volunteers
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <ShiftAllVols
                volStatusUpdates={this.state.volStatusUpdates}
                getVolData={this.props.getVolData}
                addVolData={this.props.addVolData}
                location={location}
                day={this.state.dayName}
                firebase={this.props.firebase}
                volunteers={this.state.signups}
              />
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <br />
        <br />
        <br />

        <Dialog open={this.state.dialogOpen} onClose={this.handleCloseDialog}>
          <DialogTitle>
            {this.state.dialogText.charAt(0).toUpperCase() +
              this.state.dialogText.slice(1)}{' '}
            Staging Location
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Are you sure you want to{' '}
              <b>{this.state.dialogText.toUpperCase()}</b> this staging
              location?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCloseDialog('no')} color="primary">
              Cancel
            </Button>
            <Button
              onClick={this.handleCloseDialog('yes')}
              color="primary"
              autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>

        <Modal open={this.state.loaderModal}>
          <CircularProgress
            className={classes.progressCirc}
            size={50}
            thickness={7}
            color="secondary"
          />
        </Modal>
      </div>
    )
  }
}
export default GOTVShift
