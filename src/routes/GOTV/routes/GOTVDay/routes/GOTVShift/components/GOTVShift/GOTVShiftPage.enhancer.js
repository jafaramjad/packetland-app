import { compose } from 'redux'
import { connect } from 'react-redux'
import { get } from 'lodash'
import { withHandlers, withStateHandlers } from 'recompose'
import { GOTV_PATH } from 'constants'
import { withNotifications } from 'modules/notification'

import { firestoreConnect } from 'react-redux-firebase'
import { withRouter, spinnerWhileLoading } from 'utils/components'

import { UserIsAuthenticated } from 'utils/router'

export default compose(
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  connect(({ firebase: { auth: { uid } } }) => ({ uid })),
  // Create listeners based on current users UID
  firestoreConnect(({ params }) => [
    //   // Listener for projects the current user created
    /* 	{
				collection: 'gotv',
				doc: 'oneida'
			} */
  ]),
  // // Map projects from state to props
  /* 	connect(({ firestore: { data } }, { params }) => ({
			gotv: get(data, `gotv.oneida`)
			// gotv: ['thing'] // get(data) // get(data, `projects.${'4sjDotrpFJSHFhoB69sf'}`)
		})), */
  withNotifications,
  withStateHandlers(
    // Setup initial state
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    // Add state handlers as props
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  ),
  /* 	withRouter,
		withHandlers({
			goToShift: ({ router }) => shiftId => {
				router.push(`${GOTV_PATH}/${shiftId}`)
			}
		}), */
  // Show loading spinner while project is loading
  // spinnerWhileLoading(['project']),
  withHandlers({
    getShiftData: props => data => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props
      return (
        firestore
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` })
          .get({ collection: `gotv/`, doc: `${data.county}` })
          .then(doc => {
            // console.log(doc.data())
            // console.log('data has been received')
            if (doc.exists) {
              const dbObj = doc.data()
              // console.log(doc)
              // console.log(dbObj[data.station][data.day])
              if (dbObj[data.station] && dbObj[data.station][data.day]) {
                return dbObj[data.station][data.day]
              } else {
                return undefined
              }

              // return doc.data()
            } else {
              // doc.data() will be undefined in this case
              // console.log("No such document!");
            }
          })
          .catch(err => {
            console.error('Error:', err) // eslint-disable-line no-console
            showError(err.message || 'Could not get shift')
            return Promise.reject(err)
          })
      )
    },
    addShiftData: props => (newData, successText, successCB) => {
      const { firestore, uid, showError, showSuccess, toggleDialog } = props
      // console.log(props)
      // console.log(newData)

      const locationName = newData.info.locationName
      const stationName = newData.info.stationName
      const dayName = newData.info.dayName

      if (!uid) {
        return console.error('You must be logged in to create a project')
      }
      return firestore
        .set(
          // { collection: `gotv/${locationName}/${stationName}`, doc: dayName },
          { collection: `gotv/`, doc: locationName },
          {
            [stationName]: {
              [dayName]: {
                ...newData,
                createdBy: uid,
                createdAt: firestore.FieldValue.serverTimestamp(),
                shiftUpdate: {
                  [newData.info
                    .shiftIndex]: firestore.FieldValue.serverTimestamp()
                }
              }
            }
          },
          { merge: true }
        )
        .then(() => {
          // console.log(firestore.Timestamp.now())
          firestore
            .collection(`gotv_logs`)
            .doc(
              `${locationName}-${stationName}-${dayName}-${
                firestore.Timestamp.now().seconds
              }`
            )
            .set({
              [locationName]: {
                [stationName]: {
                  [dayName]: {
                    ...newData,
                    createdBy: uid,
                    createdAt: firestore.FieldValue.serverTimestamp(),
                    shiftUpdate: {
                      [newData.info
                        .shiftIndex]: firestore.FieldValue.serverTimestamp()
                    }
                  }
                }
              }
            })

          const msg = {
            title: 'Success!',
            text: successText,
            type: 'success',
            confirmButtonText: 'Ok'
          }
          successCB(msg)
          // toggleDialog()
          // showSuccess(successText)
        })
        .catch(err => {
          console.error('Error:', err) // eslint-disable-line no-console
          // showError(err.message || 'Could not add data')
          const msg = {
            title: 'Error!',
            html:
              'Data was <u>NOT</u> saved, this is not a drill and is a real error',
            type: 'error',
            showConfirmButton: false
          }
          // confirmButtonText: 'Got It'
          successCB(msg)
          return Promise.reject(err)
        })
    },
    addVolData: props => (newData, successText) => {
      const { firestore, uid, showError, showSuccess, toggleDialog } = props
      // console.log(props)
      // console.log(newData)

      const locationName = newData.info.locationName
      const stationName = newData.info.stationName
      const dayName = newData.info.dayName

      if (!uid) {
        return console.error('You must be logged in to create a project')
      }
      return firestore
        .set(
          // { collection: `gotv/${locationName}/${stationName}`, doc: dayName },
          { collection: `volunteers/`, doc: locationName },
          {
            [stationName]: {
              [dayName]: {
                ...newData,
                createdBy: uid,
                createdAt: firestore.FieldValue.serverTimestamp()
              }
            }
          },
          { merge: true }
        )
        .then(() => {
          toggleDialog()
          showSuccess(successText)
        })
        .catch(err => {
          console.error('Error:', err) // eslint-disable-line no-console
          showError(err.message || 'Could not add data')
          return Promise.reject(err)
        })
    },
    getVolData: props => data => {
      // console.log(data)
      // console.log('getting shift data')
      const { firestore, showError } = props
      return (
        firestore
          // .get({ collection: `gotv/${data.county}/${data.station}`, doc: `${data.day}` })
          .get({ collection: `volunteers/`, doc: `${data.county}` })
          .then(doc => {
            // console.log(doc.data())
            // console.log('data has been received')
            if (doc.exists) {
              const dbObj = doc.data()
              // console.log(doc)
              // console.log(dbObj[data.station][data.day])
              if (dbObj[data.station] && dbObj[data.station][data.day]) {
                return dbObj[data.station][data.day]
              } else {
                return undefined
              }

              // return doc.data()
            } else {
              // doc.data() will be undefined in this case
              // console.log("No such document!");
            }
          })
          .catch(err => {
            console.error('Error:', err) // eslint-disable-line no-console
            showError(err.message || 'Could not get vol data')
            return Promise.reject(err)
          })
      )
    }
  })
)
