import React, { Component } from 'react'
import axios from 'axios'
import classes from './GOTVShiftPage.scss'
import NativeSelect from '@material-ui/core/NativeSelect'

// const statusTypes = {
//     Scheduled: 1,
//     Confirmed: 11,
//     ConfTwice: 23
// }

class ShiftAllVols extends Component {

    constructor(props) {
        super(props)

        const locationStation = this.props.location.split(",")
        const location = locationStation[0]
        const station = locationStation[1].replace(" ", "")

        this.state = {
            selectStatus: {},
            location: location,
            station: station,
            day: props.day,
            allVols: []
        }

        // console.log(props)
    }


    componentWillUpdate(props) {
        // console.log(props)
        // console.log(props.volunteers)
    }

    componentDidUpdate(prevProps) {
        if (this.props.volunteers !== prevProps.volunteers) {
            // console.log(this.props)

            let selectStatus = {}
            this.props.volunteers.map(item => {

                selectStatus[item.eventSignupId] = item.status.statusId
            })

            this.setState({
                allVols: this.props.volunteers.reverse(),
                selectStatus: selectStatus
                // selectStatus: this.props.volStatusUpdates.status

            })
        } else if (this.props.volStatusUpdates !== prevProps.volStatusUpdates) {
            // console.log(this.props)
            // console.log(this.props.volStatusUpdates)

            let selectStatus = {}
            Object.entries(this.props.volStatusUpdates).map(item => {
                selectStatus[item[0]] = item[1]
            })

            this.setState({

                // selectStatus: selectStatus
                selectStatus: { ...selectStatus }

            })
        } else {

        }
    }

    handleChangeStatus = id => (evt) => {
        // console.log(id)
        // console.log(evt.target.value)
        const selectEl = document.getElementById('select-' + id)
        selectEl.value = evt.target.value

        this.setState({
            selectStatus: {
                [id]: evt.target.value
            }
        }, () => {
            this.postVolStatus()
        })
    }

    postVolStatus() {


        let passInfo = {
            "locationName": this.state.location,
            "stationName": this.state.station,
            "dayName": this.state.day,
        }

        const sendToDB = {
            "status": this.state.selectStatus,
            "info": passInfo
        }

        this.props.addVolData(sendToDB, "Updated volunteer status successfully")
        this.props.firebase.push('volunteers', sendToDB)


    }




    getPhone = id => (evt) => {

        // console.log(id)

        const vanId = id

        const token = this.props.firebase.auth().currentUser.getIdToken()

        token.then(tok => {

            axios({
                method: 'get',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Authorization': 'Bearer' + ' ' + tok
                },
                //url: `http://localhost:3000/van/events?q=`
                url: `https://us-central1-packet-land.cloudfunctions.net/apiVan/people?q=${vanId}`
            }).then(resp => {
                // console.info(resp)

                const phones = resp.data.phones
                let prefPhone

                phones.map(item => {
                    item.isPreferred ? prefPhone = item.phoneNumber : null
                })


                const divEl = document.getElementsByClassName('phone-' + vanId)

                // console.log(divEl)
                const divArr = [...divEl]

                divArr.map(item => {

                    prefPhone ? item.innerHTML = `<a href='tel:${prefPhone}'>${prefPhone.replace(/[^\d]+/g, '').replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3')}</a>` : item.innerHTML = `<div class=${classes.noPhone}>No Phone#</div>`

                })


            }).catch(err => {
                console.log(err)
            })
        })

    }




    render() {







        if (this.props.volunteers && this.props.volunteers.length > 0) {

            let previousVol

            let sortedVols = this.state.allVols // this.props.volunteers

            // sortedVols = sortedVols.reverse()
            sortedVols.sort(function (a, b) {

                const personA = a.person.lastName + a.person.firstName
                const personB = b.person.lastName + b.person.firstName

                return personA > personB ? 1 : personB > personA ? -1 : 0
            })

            return (
                <table className={classes.shiftVolsTable}>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Shift</th>
                            <th>Phone</th>
                            <th>Status</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <tbody>

                        {sortedVols.map((item, idx) => {

                            // FORMAT TIME
                            const timeStart = item.startTimeOverride
                            let timeFormat = new Date(timeStart)
                            const formatOptions = {
                                timeZone: 'America/New_York',
                                hour: '2-digit',
                                minute: '2-digit',
                                hour12: true
                            }
                            let finalTime = timeFormat.toLocaleTimeString('en-US', formatOptions).replace(":00 ", "")

                            let thisVol = item.person.lastName + item.person.firstName
                            let rowClass = previousVol === thisVol ? classes.samePerson : classes.diffPerson

                            previousVol = item.person.lastName + item.person.firstName


                            return (
                                <tr key={`vol-${idx}`} className={rowClass}>
                                    <td>{item.person.lastName}, {item.person.firstName}</td>
                                    <td>{finalTime}</td>
                                    <td className={classes.tdPhone}>
                                        <div className={`phone-${item.person.vanId}`} onClick={this.getPhone(item.person.vanId)}>Get Phone</div>
                                    </td>
                                    <td>
                                        <NativeSelect id={`select-${item.eventSignupId}`} onChange={this.handleChangeStatus(item.eventSignupId)} value={this.state.selectStatus[item.eventSignupId]}>
                                            <option value="1">Scheduled</option>
                                            <option value="11">Confirmed</option>
                                            <option value="23">Conf Twice</option>
                                            <option value="3">Canceled</option>
                                            <option value="6">No Show</option>
                                        </NativeSelect>

                                    </td>
                                    <td>{item.role.name}</td>
                                </tr>
                            )




                        }

                        )}

                    </tbody>
                </table>
            )
        } else {
            return (<div>No vols</div>)
        }




    }

}

export default ShiftAllVols


//  <td>{item.role.name}</td>
