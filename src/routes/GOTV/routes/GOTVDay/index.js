export default store => ({
  path: ':gotvDay',
  /*  Async getComponent is only invoked when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure(
      [],
      require => {
        /*  Webpack - use require callback to define
          dependencies for bundling   */
        const GOTV_DAY = require('./components/GOTVDayPage').default

        /*  Return getComponent   */
        cb(null, GOTV_DAY)

        /* Webpack named bundle   */
      },
      'GOTVDay'
    )
  },
  getChildRoutes(partialNextState, cb) {
    require.ensure([], require => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const GOTVShift = require('./routes/GOTVShift').default

      /*  Return getComponent   */
      cb(null, [GOTVShift(store)])
    })
  }
})
