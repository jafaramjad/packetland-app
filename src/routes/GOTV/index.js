import { GOTV_PATH as path } from 'constants'

export default store => ({
  path,
  getComponent(nextState, cb) {
    require.ensure(
      [],
      require => {
        const GOTVPage = require('./components/GOTVPage').default
        // const API = require('./routes/Project').default;

        /*  Return getComponent   */
        cb(null, GOTVPage)

        /* Webpack named bundle   */
      },
      'GOTV'
    )
  },
  getChildRoutes(partialNextState, cb) {
    require.ensure([], require => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      // const TWITTER_API = require('./routes/Project').default;
      const GOTVDay = require('./routes/GOTVDay').default

      /*  Return getComponent   */
      cb(null, [GOTVDay(store)])
    })
  }
})
