// export const LIST_PATH = '/projects'
export const ACCOUNT_PATH = '/account'
export const ADMIN_PATH = '/admin'
export const DAILY_PATH = '/daily'
export const CANVASS_PATH = '/canvass'
export const LOGIN_PATH = '/login'
export const FINANCE_PATH = '/finance'
export const MEDIA_PATH = '/media'
export const PEOPLE_PATH = '/people'

export const USERS_PATH = '/users'
export const SCOUT_PATH = '/scout'
export const VOLS_PATH = '/vols'
export const DASHBOARD_PATH = '/dashboard'
export const GOTV_PATH = '/gotv'
export const SIGNUP_PATH = '/signup'
export const ACCOUNT_FORM_NAME = 'account'
export const LOGIN_FORM_NAME = 'login'
export const SIGNUP_FORM_NAME = 'signup'
export const NEW_PROJECT_FORM_NAME = 'newProject'

export const formNames = {
  account: ACCOUNT_FORM_NAME,
  signup: SIGNUP_FORM_NAME,
  login: LOGIN_FORM_NAME
}

export const paths = {
  admin: ADMIN_PATH,
  dashboard: DASHBOARD_PATH,
  canvass: CANVASS_PATH,
  daily: DAILY_PATH,
  finance: FINANCE_PATH,
  people: PEOPLE_PATH,
  users: USERS_PATH,
  scout: SCOUT_PATH,
  // list: LIST_PATH,
  account: ACCOUNT_PATH,
  login: LOGIN_PATH,
  signup: SIGNUP_PATH,
  media: MEDIA_PATH,
  gotv: GOTV_PATH
}

export default { ...paths, ...formNames }
