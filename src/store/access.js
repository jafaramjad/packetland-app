// ------------------------------------
// Constants
// ------------------------------------
export const ACCESS_DASHBOARD = 'ACCESS_DASHBOARD'
export const ACCESS_SITE = 'ACCESS_SITE'
export const ACCESS_ROLE = 'ACCESS_ROLE'
export const RESET = 'RESET'

// ------------------------------------
// Actions
// ------------------------------------
export function accessReset(access) {
  return {
    type: RESET,
    payload: access
  }
}
export function accessDashboard(access) {
  return {
    type: ACCESS_DASHBOARD,
    payload: access
  }
}

export function accessSite(access) {
  return {
    type: ACCESS_SITE,
    payload: access
  }
}

export function accessRole(access) {
  return {
    type: ACCESS_ROLE,
    payload: access
  }
}

// ------------------------------------
// Specialized Action Creator
// ------------------------------------
/* export const updateLocation = ({ dispatch }) => {
  return nextLocation => dispatch(locationChange(nextLocation))
} */

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = false
export function accessDashboardReducer(state = initialState, action) {
  return action.type === ACCESS_DASHBOARD ? action.payload : state
}

export function accessSiteReducer(state = initialState, action) {
  return action.type === ACCESS_SITE ? action.payload : state
}

export function accessRoleReducer(state = initialState, action) {
  return action.type === ACCESS_ROLE ? action.payload : state
}

/* export function accessDashboardReducer(state = initialState, action) {
    switch (action.type) {
        case ACCESS_DASHBOARD:
            return action.payload
            break
        case RESET:
            return initialState
            break
        default:
            return state
    }
    // return action.type === ACCESS_DASHBOARD ? action.payload : state
}
 */
