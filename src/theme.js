export default {
  palette: {
    primary: {
      main: '#000'
    },
    secondary: {
      main: '#3366FF'
    }
  },
  typography: {
    useNextVariants: true
  }
}
