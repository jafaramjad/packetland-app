import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import AccountMenu from './AccountMenu'
import LoginMenu from './LoginMenu'
import { LIST_PATH } from 'constants'
import { MEDIA_PATH } from 'constants'
import HomeIcon from '@material-ui/icons/Home'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import { renderComponent } from 'recompose'
import Button from '@material-ui/core/Button'
import classes from './Navbar.scss'
import axios from 'axios'

// let currentLocation

class Navbar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentLocation: props.routerProps.location.pathname,
      routeListener: '',
      userAuthorized: ''
    }

    // console.log(props)
    // console.log(props.routerProps)
  }

  componentWillMount() {
    const token =
      this.props.firebase.auth().currentUser &&
      this.props.firebase.auth().currentUser.getIdTokenResult()
    if (token) {
      // console.log(token)

      // console.log(this.props.firebase.auth().currentUser.getIdToken())

      token.then(idTokenResult => {
        // console.log(idTokenResult)
        axios({
          method: 'get',
          headers: {
            // 'Access-Control-Allow-Origin': '*',
            'X-Requested-With': 'XMLHttpRequest',
            Authorization: 'Bearer' + ' ' + idTokenResult.token
          },
          url: `https://us-central1-packet-land.cloudfunctions.net/apiUserCheck/userverify?q=${
            this.props.auth.email
          }`
        }).then(response => {
          // console.log(idTokenResult)

          let hasRole

          if (idTokenResult.claims.role) {
            hasRole = idTokenResult.claims.role[0]
          } else {
            if (idTokenResult.claims.admin === true) {
              hasRole = 'admin'
            } else {
              hasRole = false
            }
          }

          /* this.props.firebase.auth().currentUser.getIdTokenResult()
					.then((idTokenResult) => {
		
			
						this.setState({
							hasRole
						}, () => {
							// this.queryAPI()
						})
		
						// console.log(hasRole)
		
					})
					.catch(error => {
						console.log(error)
					})
 */

          this.props.dispatch(
            this.props.accessDashAction(response.data.dashboard)
          )
          this.props.dispatch(this.props.accessSiteAction(response.data.access))
          this.props.dispatch(this.props.accessRoleAction(hasRole))

          // console.log(response)

          // this.props.accessDash(response.data.dashboard)

          this.setState(
            {
              hasRole: hasRole,
              userAuthorized: response.data.access
            },
            () => {
              // console.log(this.props.accessDash)
              // console.log(this.state.userAuthorized)
              this.state.userAuthorized ? true : this.props.handleLogout()
            }
          )
        })
      })
    }
  }

  componentDidMount() {
    // console.log('component mounted')

    const routeListener = this.props.routerProps.listen(thing => {
      // console.log(this.props)
      // console.log(thing.pathname)
      // console.log('listening to route changes...')

      this.setState({
        currentLocation: thing.pathname
      })
    })

    this.setState(
      {
        routeListener
      },
      () => {
        // console.log(this.state)
      }
    )
  }

  componentWillUnmount() {
    const { routeListener } = { ...this.state }
    routeListener()
  }

  /* 	listenToRouteChange = () => {
			let path = this.props.routerProps.listen(thing => {
				// console.log(this.props)
				// console.log(thing.pathname)
				console.log('listening to route changes...')
				return thing.pathname
				this.setState({
					currentLocation: thing.pathname
				})
			})
			return path
		} */

  render() {
    // const avatarUrl = this.props.avatarUrl
    // const displayName = this.props.displayName
    // const authExists = this.props.authExists
    // const goToAccount = this.props.goToAccount
    // const handleLogout = this.props.handleLogout
    // const closeAccountMenu = this.props.closeAccountMenu
    // const anchorEl = this.props.anchorEl
    // const handleMenu = this.props.handleMenu
    // const classes = this.props.classes
    // const router = this.props.router
    // const goHome = this.props.goHome

    const {
      avatarUrl,
      displayName,
      authExists,
      goToAccount,
      handleLogout,
      closeAccountMenu,
      anchorEl,
      handleMenu,
      router,
      goHome
    } = { ...this.props }

    return (
      <AppBar position="static">
        <Toolbar>
          {this.state.currentLocation !== '/' ? (
            <div className={classes.flex}>
              <Button
                size="small"
                onClick={() => {
                  router.goBack()
                }}
                className={classes.whiteTextFlex}>
                <ArrowBackIosIcon className={classes.whiteTextFlex} />{' '}
                <div className={classes.whiteTextFlex}>BACK</div>
              </Button>
            </div>
          ) : (
            <div className={classes.flex} />
          )}

          {authExists ? (
            <AccountMenu
              avatarUrl={avatarUrl}
              displayName={displayName}
              onLogoutClick={handleLogout}
              goToAccount={goToAccount}
              closeAccountMenu={closeAccountMenu}
              handleMenu={handleMenu}
              anchorEl={anchorEl}
              goHome={goHome}
            />
          ) : (
            <LoginMenu />
          )}
        </Toolbar>
      </AppBar>
    )
  }
}

Navbar.propTypes = {
  displayName: PropTypes.string, // from enhancer (flattenProps - profile)
  avatarUrl: PropTypes.string, // from enhancer (flattenProps - profile)
  authExists: PropTypes.bool, // from enhancer (withProps - auth)
  goToAccount: PropTypes.func.isRequired, // from enhancer (withHandlers - router)
  handleLogout: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  closeAccountMenu: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  handleMenu: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  anchorEl: PropTypes.object, // from enhancer (withStateHandlers - handleMenu)
  classes: PropTypes.object, // from enhancer (withStyles)
  routerProps: PropTypes.object, // my custom addition
  goHome: PropTypes.func
}

export default Navbar
