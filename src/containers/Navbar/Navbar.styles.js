export default theme => ({
  flex: {
    flexGrow: 1
  },
  whiteTextFlex: {
    color: '#FFF',
    flexGrow: 1
  }
})
