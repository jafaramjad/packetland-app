import { connect } from 'react-redux'
import {
  withHandlers,
  compose,
  withProps,
  flattenProp,
  withStateHandlers
} from 'recompose'
import { withStyles } from '@material-ui/core'
import { withFirebase, isEmpty, isLoaded } from 'react-redux-firebase'
import { ACCOUNT_PATH } from 'constants'
import { withRouter, spinnerWhileLoading } from 'utils/components'
import styles from './Navbar.styles'
import { accessDashboard, accessSite, accessRole } from '../../store/access'

export default compose(
  connect((store /* { firebase: { auth, profile } } */) => {
    // console.log(store)

    return {
      accessDashAction: accessDashboard,
      accessSiteAction: accessSite,
      accessRoleAction: accessRole,
      accessDash: store.accessDash,
      auth: store.firebase.auth,
      profile: store.firebase.profile
    }
  }),
  // Wait for auth to be loaded before going further
  spinnerWhileLoading(['profile']),
  withStateHandlers(
    ({ accountMenuOpenInitially = false }) => ({
      accountMenuOpen: accountMenuOpenInitially,
      anchorEl: null
    }),
    {
      closeAccountMenu: ({ accountMenuOpen }) => () => ({
        anchorEl: null
      }),
      handleMenu: () => event => ({
        anchorEl: event.target
      })
    }
  ),
  // Add props.router (used in handlers)
  withRouter,
  // Add props.firebase (used in handlers)
  withFirebase,
  // Handlers
  withHandlers({
    handleLogout: props => () => {
      props.firebase.logout()
      props.router.push('/')
      props.closeAccountMenu()
    },
    goToAccount: props => () => {
      props.router.push(ACCOUNT_PATH)
      props.closeAccountMenu()
    },
    goHome: props => () => {
      props.router.push('/')
      props.closeAccountMenu()
    },
    getPlaces: props => newInstance => {
      const { firestore, uid } = props
      if (!uid) {
        return false // console.error('You must be logged in')
      }

      return firestore.get({
        collection: `NC09/MCCREADY/CONSTANTS`,
        doc: `PLACES`
      })
    }
  }),
  withProps(props => ({
    authExists: isLoaded(props.auth) && !isEmpty(props.auth),
    routerProps: props.router
  })),
  // Flatten profile so that avatarUrl and displayName are available
  flattenProp('profile')
  // withStyles(styles)
)
